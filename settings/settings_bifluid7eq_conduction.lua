-- available settings read by canoP are written to terminal
-- at the beginning of the simulation, apart from the block
-- specific to the initial condition. Use a callback called
-- e.g. "list" to get available callback options in canoP. 

simulation = {

   -- name of the model.
   model = "bifluid7eq",

   -- the max time of the simulation which will run from [0, tmax]
   tmax = 0.1,

   -- the Courant number used to compute the time step
   cfl = 0.5,

   -- scheme space order (1 or 2 = MUSCL)
   space_order = 2,

   -- time order (1 or 2 = Hancock)
   time_order = 2

}

amr = {

   -- periodicity (override boundary condition callback)
   periodic_x = 1,
   periodic_y = 1,
   periodic_z = 1,

   -- number of p4est tree per direction
   ntree_x = 1,
   ntree_y = 1,
   ntree_z = 1,	

   -- physical size of a p4est tree
   ltree = 1,

   -- minimum number of starting quadrants per process
   min_quadrants = 16,
  
   -- the minimum level of refinement of the mesh per process
   min_refine = 3,

   -- the maximum level of refinement of the mesh per process
   max_refine = 6,

   -- the threshold for refining
   epsilon_refine = 0.005,

   -- the threshold for coarsening
   epsilon_coarsen = 0.01

}

io = {

   -- the name of the ouputs
   output_prefix = "bifluid7eq_conduction",

   -- number of output files (-1 means, every time step)
   save_count = 50,

   -- gather different statistics. see StatManager.h.
   statistics_level = 0,

   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,

   -- select which variables to write (conservative or primitive)
   write_variables = "mg, ml, Vg, Vl, Pg, Pl, ag, Tg, Tl",

   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0

}


callback = {

   -- initial condition.
   initial_condition = "conduction",

   -- boundary conditions (overriden by amr if periodic)
   boundary_condition = "neuman",

   -- type of refine condition.
   refine_condition = "mE_gradient",
 
   -- type of Riemann solver for hydrodynamics
   hydrodynamics_flux = "none",

   -- activate conductivity flux
   conductivity_flux = "conductivity_enabled",

   -- type of flux limiter
   scalar_limiter = "minmod"

}

conduction = {

   -- bump radius (in real units)
   radius = 0.1,
   
   -- center of temperature bump (in real units)
   x_c = 0.5,
   y_c = 0.5,
   z_c = 0.5,
   
   -- density of fluid : at centre and outside
   m_c  = 1.0,
   m_out = 1.0,

   -- pressure of fluid : at centre and outside
   P_c  = 100.0,
   P_out =  1.0

}

param = {

   -- fluid conductivity
   eos_g = {
   kappa = 1.
   },

   eos_l = {
   kappa = 1.
   },

}