-- available settings read by canoP are written to terminal
-- at the beginning of the simulation, apart from the block
-- specific to the initial condition. Use a callback called
-- e.g. "list" to get available callback options in canoP. 

simulation = {

   -- name of the model.
   model = "bifluid7eq",

   -- the max time of the simulation which will run from [0, tmax]
   tmax = 500.,

   -- the Courant number used to compute the time step
   cfl = 0.5,

   -- scheme space order (1 or 2 = MUSCL)
   space_order = 2,

   -- time order (1 or 2 = Hancock)
   time_order = 2

}

amr = {

   -- periodicity (override boundary condition callback)
   periodic_x = 0,
   periodic_y = 0,
   periodic_z = 0,

   -- number of p4est tree per direction
   ntree_x = 1,
   ntree_y = 1,
   ntree_z = 1,	

   -- physical size of a p4est tree
   ltree = 1,

   -- minimum number of starting quadrants per process
   min_quadrants = 16,
  
   -- the minimum level of refinement of the mesh per process
   min_refine = 3,

   -- the maximum level of refinement of the mesh per process
   max_refine = 5,

   -- the threshold for refining
   epsilon_refine = 0.005,

   -- the threshold for coarsening
   epsilon_coarsen = 0.01

}

io = {

   -- the name of the ouputs
   output_prefix = "bifluid7eq_poiseuille",

   -- number of output files (-1 means, every time step)
   save_count = 50,

   -- gather different statistics. see StatManager.h.
   statistics_level = 0,

   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,

   -- select which variables to write (conservative or primitive)
   write_variables = "mg, ml, Vg, Vl, Pg, Pl, ag",

   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0

}


callback = {

   -- initial condition.
   initial_condition = "poiseuille",

   -- boundary conditions (overriden by amr if periodic)
   boundary_condition = "poiseuille",

   -- type of refine condition.
   refine_condition = "any_gradient",
 
   -- type of Riemann solver for hydrodynamics
   hydrodynamics_flux = "hll_enabled",

   -- activate viscosity flux
   viscosity_flux = "viscosity_enabled",

   -- type of flux limiter
   scalar_limiter = "minmod"

}

poiseuille = {

   m0  = 1.0,
   P0    = 0.0240096,
   gradP = -8E-6

}

param = {

   -- viscosity of the fluids

   eos_g = {
   mu = 1E-3,
   eta = 0.
   },

   eos_l = {
   mu = 1E-3,
   eta = 0.
   }

}