-- available settings read by canoP are written to terminal
-- at the beginning of the simulation, apart from the block
-- specific to the initial condition. Use a callback called
-- e.g. "list" to get available callback options in canoP. 

simulation = {

   -- name of the model.
   model = "bifluid7eq",

   -- the max time of the simulation which will run from [0, tmax]
   tmax = 1E-3,

   -- the Courant number used to compute the time step
   cfl = 0.5,

   -- scheme space order (1 or 2 = MUSCL)
   space_order = 2,

   -- time order (1 or 2 = Hancock)
   time_order = 2

}

amr = {

   -- periodicity (override boundary condition callback)
   periodic_x = 0,
   periodic_y = 0,
   periodic_z = 0,

   -- number of p4est tree per direction
   ntree_x = 1,
   ntree_y = 1,
   ntree_z = 1,	

   -- physical size of a p4est tree
   ltree = 1,

   -- minimum number of starting quadrants per process
   min_quadrants = 16,
  
   -- the minimum level of refinement of the mesh per process
   min_refine = 3,

   -- the maximum level of refinement of the mesh per process
   max_refine = 7,

   -- the threshold for refining
   epsilon_refine = 0.005,

   -- the threshold for coarsening
   epsilon_coarsen = 0.01

}

io = {

   -- the name of the ouputs
   output_prefix = "bifluid7eq_underwater_explosion",

   -- number of output files (-1 means, every time step)
   save_count = 50,

   -- gather different statistics. see StatManager.h.
   statistics_level = 0,

   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,

   -- select which variables to write (conservative or primitive)
   write_variables = "mg, ml, Vg, Vl, Pg, Pl, ag",

   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0

}


callback = {

   -- initial condition.
   initial_condition = "underwater_explosion",

   -- boundary conditions (overriden by amr if periodic)
   boundary_condition = "neuman",

   -- type of refine condition.
   refine_condition = "m_gradient",
 
   -- type of Riemann solver for hydrodynamics
   hydrodynamics_flux = "hll_enabled",

   -- pessure relaxation
   pressure_relaxation_term = "pressure_relaxation_enabled",

   -- velocity relaxation
   velocity_relaxation_term = "velocity_relaxation_enabled",

   -- type of flux limiter
   scalar_limiter = "minmod"

}

underwater_explosion = {

   rho_up    = 1.27,
   P_up      = 1.0E5,
   alpha1_up = 0.995,
   Y1_up     = 0.995,

   rho_down    = 1000.,
   P_down      = 1.0E5,
   alpha1_down = 0.005,
   Y1_down     = 0.005,

   rho_in    = 1270.,
   P_in      = 8.29E8,
   alpha1_in = 0.995,
   Y1_in     = 0.995,

   radius = 0.05

}

param = {

      eos_g = {

      gamma = 2.0,
      pinf  = 0.

      },

      eos_l = {

      gamma = 7.15,
      pinf  = 3.0E8

      },

      interface_mode = 1

}

