cmake_minimum_required(VERSION 3.0.0)
project(canoP CXX C)

# allow cmake to use custom modules (e.g. FindHDF5)
# (cmake 3.4 has a nicer FindHDF5 macro, but we used here
# our own version of FindHDF5 instead of requiring users to
# upgrade their cmake version to version >= 3.4)
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake ${CMAKE_MODULE_PATH})

option(USE_2D "enable 2D" ON)
option(USE_3D "enable 3D" ON)
option(USE_MONOFLUID "enable monofluid solver" ON)
option(USE_BIFLUID5EQ "enable bifluid5eq solver" ON)
option(USE_BIFLUID7EQ "enable bifluid7eq solver" ON)

if (USE_MONOFLUID)
  add_definitions("-DUSE_MONOFLUID")
endif(USE_MONOFLUID)

if (USE_BIFLUID5EQ)
  add_definitions("-DUSE_BIFLUID5EQ")
endif(USE_BIFLUID5EQ)

if (USE_BIFLUID7EQ)
  add_definitions("-DUSE_BIFLUID7EQ")
endif(USE_BIFLUID7EQ)

set(BUILD_TESTING OFF)
set(enable_p6est OFF)
set(enable_p8est ON)
set(mpi ON)
set(openmp OFF)
set(zlib OFF)

set (CMAKE_CXX_STANDARD 20)

# set some flags
if(NOT CMAKE_BUILD_TYPE)
  message(STATUS "Setting build type to 'Release' as none was specified.")
  set(CMAKE_BUILD_TYPE "Release")
endif()

# find required packages
find_package(MPI REQUIRED)
set( HDF5_PREFER_PARALLEL true )
find_package(HDF5)

#
# Make sure p4est is available (as a git submodule)
#
if(NOT EXISTS "${PROJECT_SOURCE_DIR}/external/p4est/Makefile.am")
  execute_process(
    COMMAND git submodule update --init --recursive
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})
endif()


# build lua
add_subdirectory(external/lua)

# build sc
add_subdirectory(external/p4est/sc)

# build p4est
add_subdirectory(external/p4est)



# always include this directory
foreach(PREFIX_PATH ${CMAKE_PREFIX_PATH})
    include_directories(${PREFIX_PATH}/include)
    link_directories(${PREFIX_PATH}/lib)
endforeach()

add_subdirectory(src)

