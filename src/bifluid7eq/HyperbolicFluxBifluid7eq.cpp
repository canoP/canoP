//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataBifluid7eq.h"

using cons_t = bifluid7eq::cons_t;
using prim_t = bifluid7eq::prim_t;
using param_t = bifluid7eq::param_t;
using qdata_t = bifluid7eq::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// HLL Non-conservative flux
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void hyperbolic_flux_hll(prim_t &qleft, prim_t &qright, cons_t &ucleft,
                         cons_t &ucright, cons_t *fluxleft, cons_t *fluxright,
                         QdataManager<cons_t> *qdata_mgr, double *extra) {

  UNUSED(extra);

  param_t *param = qdata_mgr->get_param();

  // 1D HLL Riemann solver

  double agl = qleft.ag;
  double agr = qright.ag;

  double mgl = qleft.mg;
  double pgl = qleft.Pg;
  double ugl = qleft.Vg[IX];

  double mgr = qright.mg;
  double pgr = qright.Pg;
  double ugr = qright.Vg[IX];

  double rgl = qleft.mg / (agl);
  double rgr = qright.mg / (agr);

  double cgl = get_soundspeed(rgl, pgl, param->eos_g);
  double cgr = get_soundspeed(rgr, pgr, param->eos_g);

  double mll = qleft.ml;
  double pll = qleft.Pl;
  double ull = qleft.Vl[IX];

  double mlr = qright.ml;
  double plr = qright.Pl;
  double ulr = qright.Vl[IX];

  double rll = qleft.ml / (1.0 - agl);
  double rlr = qright.ml / (1.0 - agr);

  double cll = get_soundspeed(rll, pll, param->eos_l);
  double clr = get_soundspeed(rlr, plr, param->eos_l);

  double egl = get_eint(rgl, pgl, param->eos_g);
  double egr = get_eint(rgr, pgr, param->eos_g);

  double ell = get_eint(rll, pll, param->eos_l);
  double elr = get_eint(rlr, plr, param->eos_l);

  // Compute HLL wave speed
  double SgL = fmin(ugl - cgl, ugr - cgr);
  double SgR = fmax(ugl + cgl, ugr + cgr);
  double SlL = fmin(ull - cll, ulr - clr);
  double SlR = fmax(ull + cll, ulr + clr);
  double SL = fmin(SgL, SlL);
  double SR = fmax(SgR, SlR);

  // Compute conservative variables
  cons_t uleft, uright;

  uleft.ag = qleft.ag;
  uright.ag = qright.ag;

  uleft.mg = mgl;
  uright.mg = mgr;
  uleft.mgEg = (egl + 0.5 * ugl * ugl) * mgl;
  uright.mgEg = (egr + 0.5 * ugr * ugr) * mgr;
  uleft.mgEg += 0.5 * mgl * qleft.Vg[IY] * qleft.Vg[IY];
  uright.mgEg += 0.5 * mgr * qright.Vg[IY] * qright.Vg[IY];

  uleft.ml = mll;
  uright.ml = mlr;
  uleft.mlEl = (ell + 0.5 * ull * ull) * mll;
  uright.mlEl = (elr + 0.5 * ulr * ulr) * mlr;
  uleft.mlEl += 0.5 * mll * qleft.Vl[IY] * qleft.Vl[IY];
  uright.mlEl += 0.5 * mlr * qright.Vl[IY] * qright.Vl[IY];
#ifdef USE_3D
  uleft.mgEg += 0.5 * mgl * qleft.Vg[IZ] * qleft.Vg[IZ];
  uright.mgEg += 0.5 * mgr * qright.Vg[IZ] * qright.Vg[IZ];

  uleft.mlEl += 0.5 * mll * qleft.Vl[IZ] * qleft.Vl[IZ];
  uright.mlEl += 0.5 * mlr * qright.Vl[IZ] * qright.Vl[IZ];
#endif
  uleft.mgVg[IX] = mgl * ugl;
  uright.mgVg[IX] = mgr * ugr;

  uleft.mlVl[IX] = mll * ull;
  uright.mlVl[IX] = mlr * ulr;

  // Other advected quantities
  uleft.mgVg[IY] = mgl * qleft.Vg[IY];
  uright.mgVg[IY] = mgr * qright.Vg[IY];
  uleft.mlVl[IY] = mll * qleft.Vl[IY];
  uright.mlVl[IY] = mlr * qright.Vl[IY];
#ifdef USE_3D
  uleft.mgVg[IZ] = mgl * qleft.Vg[IZ];
  uright.mgVg[IZ] = mgr * qright.Vg[IZ];
  uleft.mlVl[IZ] = mll * qleft.Vl[IZ];
  uright.mlVl[IZ] = mlr * qright.Vl[IZ];
#endif
  // Compute left and right fluxes
  cons_t fleft, fright, flux;
  fleft.ag = 0.;
  fright.ag = 0.;

  fleft.mg = uleft.mgVg[IX];
  fright.mg = uright.mgVg[IX];
  fleft.mgEg = ugl * (uleft.mgEg + (agl)*pgl);
  fright.mgEg = ugr * (uright.mgEg + (agr)*pgr);
  fleft.mgVg[IX] = uleft.mgVg[IX] * ugl + pgl * (agl);
  fright.mgVg[IX] = uright.mgVg[IX] * ugr + pgr * (agr);

  fleft.ml = uleft.mlVl[IX];
  fright.ml = uright.mlVl[IX];
  fleft.mlEl = ull * (uleft.mlEl + (1.0 - agl) * pll);
  fright.mlEl = ulr * (uright.mlEl + (1.0 - agr) * plr);
  fleft.mlVl[IX] = uleft.mlVl[IX] * ull + pll * (1.0 - agl);
  fright.mlVl[IX] = uright.mlVl[IX] * ulr + plr * (1.0 - agr);

  // Other advected quantities
  fleft.mgVg[IY] = fleft.mg * qleft.Vg[IY];
  fright.mgVg[IY] = fright.mg * qright.Vg[IY];
  fleft.mlVl[IY] = fleft.ml * qleft.Vl[IY];
  fright.mlVl[IY] = fright.ml * qright.Vl[IY];
#ifdef USE_3D
  fleft.mgVg[IZ] = fleft.mg * qleft.Vg[IZ];
  fright.mgVg[IZ] = fright.mg * qright.Vg[IZ];
  fleft.mlVl[IZ] = fleft.ml * qleft.Vl[IZ];
  fright.mlVl[IZ] = fright.ml * qright.Vl[IZ];
#endif

  if (SL > 0.0) {
    flux.ag = 0.0;
    flux.mg = fleft.mg;
    flux.mgEg = fleft.mgEg;
    flux.mgVg[IX] = fleft.mgVg[IX];
    flux.ml = fleft.ml;
    flux.mlEl = fleft.mlEl;
    flux.mlVl[IX] = fleft.mlVl[IX];
    // phase g
    if (flux.mg > 0.0) {
      flux.mgVg[IY] = flux.mg * qleft.Vg[IY];
    } else {
      flux.mgVg[IY] = flux.mg * qright.Vg[IY];
    }
#ifdef USE_3D
    if (flux.mg > 0.0) {
      flux.mgVg[IZ] = flux.mg * qleft.Vg[IZ];
    } else {
      flux.mgVg[IZ] = flux.mg * qright.Vg[IZ];
    }
#endif
    // phase l
    if (flux.ml > 0.0) {
      flux.mlVl[IY] = flux.ml * qleft.Vl[IY];
    } else {
      flux.mlVl[IY] = flux.ml * qright.Vl[IY];
    }
#ifdef USE_3D
    if (flux.ml > 0.0) {
      flux.mlVl[IZ] = flux.ml * qleft.Vl[IZ];
    } else {
      flux.mlVl[IZ] = flux.ml * qright.Vl[IZ];
    }
#endif
  } else if (SR < 0.0) {
    flux.ag = 0.0;
    flux.mg = fright.mg;
    flux.mgEg = fright.mgEg;
    flux.mgVg[IX] = fright.mgVg[IX];
    flux.ml = fright.ml;
    flux.mlEl = fright.mlEl;
    flux.mlVl[IX] = fright.mlVl[IX];
    // phase g
    if (flux.mg > 0.0) {
      flux.mgVg[IY] = flux.mg * qleft.Vg[IY];
    } else {
      flux.mgVg[IY] = flux.mg * qright.Vg[IY];
    }
#ifdef USE_3D
    if (flux.mg > 0.0) {
      flux.mgVg[IZ] = flux.mg * qleft.Vg[IZ];
    } else {
      flux.mgVg[IZ] = flux.mg * qright.Vg[IZ];
    }
#endif
    // phase l
    if (flux.ml > 0.0) {
      flux.mlVl[IY] = flux.ml * qleft.Vl[IY];
    } else {
      flux.mlVl[IY] = flux.ml * qright.Vl[IY];
    }
#ifdef USE_3D
    if (flux.ml > 0.0) {
      flux.mlVl[IZ] = flux.ml * qleft.Vl[IZ];
    } else {
      flux.mlVl[IZ] = flux.ml * qright.Vl[IZ];
    }
#endif
  } else {
    // Compute HLL fluxes
    flux.ag =
        (SR * fleft.ag - SL * fright.ag + SR * SL * (uright.ag - uleft.ag)) /
        (SR - SL);
    // phase g
    flux.mg =
        (SR * fleft.mg - SL * fright.mg + SR * SL * (uright.mg - uleft.mg)) /
        (SR - SL);
    flux.mgEg = (SR * fleft.mgEg - SL * fright.mgEg +
                 SR * SL * (uright.mgEg - uleft.mgEg)) /
                (SR - SL);
    flux.mgVg[IX] = (SR * fleft.mgVg[IX] - SL * fright.mgVg[IX] +
                     SR * SL * (uright.mgVg[IX] - uleft.mgVg[IX])) /
                    (SR - SL);
    flux.mgVg[IY] = (SR * fleft.mgVg[IY] - SL * fright.mgVg[IY] +
                     SR * SL * (uright.mgVg[IY] - uleft.mgVg[IY])) /
                    (SR - SL);
    // phase l
    flux.ml =
        (SR * fleft.ml - SL * fright.ml + SR * SL * (uright.ml - uleft.ml)) /
        (SR - SL);
    flux.mlEl = (SR * fleft.mlEl - SL * fright.mlEl +
                 SR * SL * (uright.mlEl - uleft.mlEl)) /
                (SR - SL);
    flux.mlVl[IX] = (SR * fleft.mlVl[IX] - SL * fright.mlVl[IX] +
                     SR * SL * (uright.mlVl[IX] - uleft.mlVl[IX])) /
                    (SR - SL);
    flux.mlVl[IY] = (SR * fleft.mlVl[IY] - SL * fright.mlVl[IY] +
                     SR * SL * (uright.mlVl[IY] - uleft.mlVl[IY])) /
                    (SR - SL);
#ifdef USE_3D
    flux.mgVg[IZ] = (SR * fleft.mgVg[IZ] - SL * fright.mgVg[IZ] +
                     SR * SL * (uright.mgVg[IZ] - uleft.mgVg[IZ])) /
                    (SR - SL);
    flux.mlVl[IZ] = (SR * fleft.mlVl[IZ] - SL * fright.mlVl[IZ] +
                     SR * SL * (uright.mlVl[IZ] - uleft.mlVl[IZ])) /
                    (SR - SL);
#endif
  }

  // Nonconservative interface terms
  double Ui_left = get_interface_velocity(ucleft, param, IX);
  double Pi_left = get_interface_pressure(ucleft, param, IX);

  double Ui_right = get_interface_velocity(ucright, param, IX);
  double Pi_right = get_interface_pressure(ucright, param, IX);

  double alpha_face = (SR * qleft.ag - SL * qright.ag) / (SR - SL);

  fluxleft->ag += flux.ag + alpha_face * Ui_left;
  fluxleft->mg += flux.mg;
  fluxleft->mgEg += flux.mgEg - alpha_face * Pi_left * Ui_left;
  fluxleft->mgVg[IX] += flux.mgVg[IX] - alpha_face * Pi_left;
  fluxleft->mgVg[IY] += flux.mgVg[IY];
  fluxleft->ml += flux.ml;
  fluxleft->mlEl += flux.mlEl + alpha_face * Pi_left * Ui_left;
  ;
  fluxleft->mlVl[IX] += flux.mlVl[IX] + alpha_face * Pi_left;
  fluxleft->mlVl[IY] += flux.mlVl[IY];
#ifdef USE_3D
  fluxleft->mgVg[IZ] += flux.mgVg[IZ];
  fluxleft->mlVl[IZ] += flux.mlVl[IZ];
#endif

  fluxright->ag += flux.ag + alpha_face * Ui_right;
  fluxright->mg += flux.mg;
  fluxright->mgEg += flux.mgEg - alpha_face * Pi_right * Ui_right;
  fluxright->mgVg[IX] += flux.mgVg[IX] - alpha_face * Pi_right;
  fluxright->mgVg[IY] += flux.mgVg[IY];
  fluxright->ml += flux.ml;
  fluxright->mlEl += flux.mlEl + alpha_face * Pi_right * Ui_right;
  ;
  fluxright->mlVl[IX] += flux.mlVl[IX] + alpha_face * Pi_right;
  fluxright->mlVl[IY] += flux.mlVl[IY];
#ifdef USE_3D
  fluxright->mgVg[IZ] += flux.mgVg[IZ];
  fluxright->mlVl[IZ] += flux.mlVl[IZ];
#endif
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing hyperbolic fluxes for bifluid7eq
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_hf_callback_t<cons_t>>::Factory() {

  // register the above callback's
  register_factory("none", nullptr);
  register_factory("hll_enabled", hyperbolic_flux_hll);
}
