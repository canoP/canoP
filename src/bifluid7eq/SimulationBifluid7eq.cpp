//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Iterator.h"
#include "QdataBifluid7eq.h"
#include "Simulation.h"

using cons_t = bifluid7eq::cons_t;

//////////////////////////////////////////////////////////////////////////////
/// set_iterator_volume specialization for bifluid7eq
//////////////////////////////////////////////////////////////////////////////
template <> void Simulation<cons_t>::set_iterator_volume() {

  // register the above callback's
  iv_factory.register_factory("copy", iterator_copy<cons_t>);
  iv_factory.register_factory("mark_adapt", iterator_mark_adapt<cons_t>);
  iv_factory.register_factory("time_step", iterator_time_step<cons_t>);
  iv_factory.register_factory("compute_wm_wp", iterator_compute_wm_wp<cons_t>);
  iv_factory.register_factory("update_source", iterator_update_source<cons_t>);
  iv_factory.register_factory("init", iterator_init<cons_t>);
  iv_factory.register_factory("get_stat", iterator_get_stat<cons_t>);
}

//////////////////////////////////////////////////////////////////////////////
/// set_iterator_face specialization for bifluid7eq
//////////////////////////////////////////////////////////////////////////////
template <> void Simulation<cons_t>::set_iterator_face() {

  // register the above callback's
  if_factory.register_factory("update_flux", iterator_update_flux<cons_t>);
  if_factory.register_factory("compute_delta", iterator_compute_delta<cons_t>);
}
