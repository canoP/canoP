//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataBifluid7eq.h"

using cons_t = bifluid7eq::cons_t;
using prim_t = bifluid7eq::prim_t;
using param_t = bifluid7eq::param_t;
using qdata_t = bifluid7eq::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// Gravity source term
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void source_term_gravity(prim_t &q, prim_t &qnext, cons_t *wnext, double dt,
                         QdataManager<cons_t> *qdata_mgr) {
  UNUSED(qnext);

  param_t *param = qdata_mgr->get_param();

  wnext->mgVg[IX] += param->gravity_x * q.mg * dt;
  wnext->mgVg[IY] += param->gravity_y * q.mg * dt;
  wnext->mlVl[IX] += param->gravity_x * q.ml * dt;
  wnext->mlVl[IY] += param->gravity_y * q.ml * dt;
#ifdef USE_3D
  wnext->mgVg[IZ] += param->gravity_z * q.mg * dt;
  wnext->mlVl[IZ] += param->gravity_z * q.ml * dt;
#endif /* USE_3D */

  wnext->mgEg += param->gravity_x * q.mg * q.Vg[IX] * dt;
  wnext->mgEg += param->gravity_y * q.mg * q.Vg[IY] * dt;
  wnext->mlEl += param->gravity_x * q.ml * q.Vl[IX] * dt;
  wnext->mlEl += param->gravity_y * q.ml * q.Vl[IY] * dt;
#ifdef USE_3D
  wnext->mgEg += param->gravity_z * q.mg * q.Vg[IZ] * dt;
  wnext->mlEl += param->gravity_z * q.ml * q.Vl[IZ] * dt;
#endif /* USE_3D */
}

//////////////////////////////////////////////////////////////////////////////
/// Velocity relaxation source term
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void source_term_velocity_relaxation(prim_t &q, prim_t &qnext, cons_t *wnext,
                                     double dt,
                                     QdataManager<cons_t> *qdata_mgr) {
  UNUSED(q);
  UNUSED(dt);

  param_t *param = qdata_mgr->get_param();

  array<double> Vg_relax;
  array<double> Vl_relax;
  double eking = 0, ekinl = 0;
  double cinetg = 0, cinetl = 0;
  double eintg = 0, eintl = 0;

  for (int dir = 0; dir < P4EST_DIM; ++dir) {
    Vg_relax[dir] = (qnext.mg * qnext.Vg[dir] + qnext.ml * qnext.Vl[dir]) /
                    (qnext.mg + qnext.ml);
    Vl_relax[dir] = Vg_relax[dir];

    eking += 0.5 * Vg_relax[dir] * Vg_relax[dir];
    ekinl += 0.5 * Vl_relax[dir] * Vl_relax[dir];

    cinetg +=
        0.5 * (Vg_relax[dir] - qnext.Vg[dir]) * (Vg_relax[dir] + qnext.Vg[dir]);
    cinetl +=
        0.5 * (Vl_relax[dir] - qnext.Vl[dir]) * (Vl_relax[dir] + qnext.Vl[dir]);
  } // end for dir

  eintg = get_eint(qnext.mg / qnext.ag, qnext.Pg, param->eos_g);
  eintl = get_eint(qnext.ml / (1.0 - qnext.ag), qnext.Pl, param->eos_l);

  wnext->mgVg[IX] = qnext.mg * Vg_relax[IX];
  wnext->mgVg[IY] = qnext.mg * Vg_relax[IY];
  wnext->mlVl[IX] = qnext.ml * Vl_relax[IX];
  wnext->mlVl[IY] = qnext.ml * Vl_relax[IY];

#ifdef USE_3D
  wnext->mgVg[IZ] = qnext.mg * Vg_relax[IZ];
  wnext->mlVl[IZ] = qnext.ml * Vl_relax[IZ];
#endif

  wnext->mgEg = qnext.mg * (eintg + cinetg + eking);
  wnext->mlEl = qnext.ml * (eintl + cinetl + ekinl);
}

//////////////////////////////////////////////////////////////////////////////
/// Pressure relaxation source term
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void source_term_pressure_relaxation(prim_t &q, prim_t &qnext, cons_t *wnext,
                                     double dt,
                                     QdataManager<cons_t> *qdata_mgr) {
  UNUSED(q);
  UNUSED(dt);

  param_t *param = qdata_mgr->get_param();

  double B;
  double Ag, Al;
  double eintg_relax, eintl_relax;
  double eking, ekinl;
  double ag, alphal, ag_relax;
  double rhog_relax;
  double rhol_relax;
  double Pg, Pg_relax;
  double Pl, Pl_relax;

  double gammag = param->eos_g.gamma;
  double gammal = param->eos_l.gamma;
  double pinfg = param->eos_g.pinf;
  double pinfl = param->eos_l.pinf;
  double bg = param->eos_g.b;
  double bl = param->eos_l.b;

  ag = qnext.ag;
  alphal = 1.0 - qnext.ag;
  Pg = qnext.Pg;
  Pl = qnext.Pl;

  B = (ag - qnext.mg * bg) / gammag + (alphal - qnext.ml * bl) / gammal;

  Ag = (Pg + pinfg) / gammag * (ag - qnext.mg * bg) / B;
  Al = (Pl + pinfl) / gammal * (alphal - qnext.ml * bl) / B;

  Pg_relax = 0.5 * (Ag + Al - (pinfg + pinfl)) +
             sqrt((0.25 * SC_SQR((Al - Ag - (pinfl - pinfg))) + Ag * Al));
  Pl_relax = Pg_relax;

  rhog_relax =
      1.0 / ((ag / qnext.mg +
              (bg - ag / qnext.mg) *
                  (1.0 - (qnext.Pg + pinfg) / (Pg_relax + pinfg)) / gammag));
  rhol_relax =
      1.0 / ((alphal / qnext.ml +
              (bl - alphal / qnext.ml) *
                  (1.0 - (qnext.Pl + pinfl) / (Pl_relax + pinfl)) / gammal));

  ag_relax = qnext.mg / rhog_relax;

  eintg_relax = get_eint(rhog_relax, Pg_relax, param->eos_g);
  eintl_relax = get_eint(rhol_relax, Pl_relax, param->eos_l);

  eking = 0.5 * (qnext.Vg[IX] * qnext.Vg[IX] + qnext.Vg[IY] * qnext.Vg[IY]);
  ekinl = 0.5 * (qnext.Vl[IX] * qnext.Vl[IX] + qnext.Vl[IY] * qnext.Vl[IY]);

#ifdef USE_3D
  eking += 0.5 * qnext.Vg[IZ] * qnext.Vg[IZ];
  ekinl += 0.5 * qnext.Vl[IZ] * qnext.Vl[IZ];
#endif

  wnext->ag = ag_relax;

  wnext->mgEg = qnext.mg * (eintg_relax + eking);
  wnext->mlEl = qnext.ml * (eintl_relax + ekinl);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing source terms for bifluid7eq
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_st_callback_t<cons_t>>::Factory() {

  // register the above callback's
  register_factory("none", nullptr);
  register_factory("gravity_enabled", source_term_gravity);
  register_factory("pressure_relaxation_enabled",
                   source_term_pressure_relaxation);
  register_factory("velocity_relaxation_enabled",
                   source_term_velocity_relaxation);
}
