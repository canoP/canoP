//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "QdataBifluid7eq.h"
#include "QdataManager.h"

using cons_t = bifluid7eq::cons_t;

/// @name specialization for bifluid7eq
/// @{
//////////////////////////////////////////////////////////////////////////////
template <> QdataManager<cons_t>::QdataManager(SettingManager *stg_mgr) {

  P4EST_GLOBAL_PRODUCTION("-- read param settings -- \n");

  // Initialize bifluid7eq gravity parameters.
  m_param.gravity_x = stg_mgr->readv_double("param.gravity_x", 0.0);
  m_param.gravity_y = stg_mgr->readv_double("param.gravity_y", 0.0);
  m_param.gravity_z = stg_mgr->readv_double("param.gravity_z", 0.0);

  m_param.interface_mode = stg_mgr->readv_int("param.interface_mode", 1);

  // Initialize eos parameters.
  m_param.eos_g.gamma = stg_mgr->readv_double("param.eos_g.gamma", 1.666);
  m_param.eos_g.Cv = stg_mgr->readv_double("param.eos_g.Cv", 1.0);
  m_param.eos_g.q = stg_mgr->readv_double("param.eos_g.q", 0.0);
  m_param.eos_g.b = stg_mgr->readv_double("param.eos_g.b", 0.0);
  m_param.eos_g.qp = stg_mgr->readv_double("param.eos_g.qp", 0.0);
  m_param.eos_g.pinf = stg_mgr->readv_double("param.eos_g.pinf", 0.0);
  m_param.eos_g.mu = stg_mgr->readv_double("param.eos_g.mu", 0.0);
  m_param.eos_g.eta = stg_mgr->readv_double("param.eos_g.eta", 0.0);
  m_param.eos_g.kappa = stg_mgr->readv_double("param.eos_g.kappa", 0.0);

  m_param.eos_l.gamma = stg_mgr->readv_double("param.eos_l.gamma", 1.666);
  m_param.eos_l.Cv = stg_mgr->readv_double("param.eos_l.Cv", 1.0);
  m_param.eos_l.q = stg_mgr->readv_double("param.eos_l.q", 0.0);
  m_param.eos_l.b = stg_mgr->readv_double("param.eos_l.b", 0.0);
  m_param.eos_l.qp = stg_mgr->readv_double("param.eos_l.qp", 0.0);
  m_param.eos_l.pinf = stg_mgr->readv_double("param.eos_l.pinf", 0.0);
  m_param.eos_l.mu = stg_mgr->readv_double("param.eos_l.mu", 0.0);
  m_param.eos_l.eta = stg_mgr->readv_double("param.eos_l.eta", 0.0);
  m_param.eos_l.kappa = stg_mgr->readv_double("param.eos_l.kappa", 0.0);

  // for all-regime scheme
  // m_param.lowmach_correction_enabled = stg_mgr->readv_int
  // ("settings.lowmach_correction_enabled", 0); m_param.K =
  // stg_mgr->readv_double ("settings.K", 1.1);

  P4EST_GLOBAL_PRODUCTION("\n");
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::cons_to_prim(const cons_t &cdata, prim_t &pdata) {

  // kinetic energy per mass unit
  double ekin_g = 0.0;
  double ekin_l = 0.0;

  pdata.mg = cdata.mg;
  pdata.ml = cdata.ml;
  pdata.ag = cdata.ag;
  for (int i = 0; i < P4EST_DIM; ++i) {
    pdata.Vg[i] = cdata.mgVg[i] / cdata.mg;
    pdata.Vl[i] = cdata.mlVl[i] / cdata.ml;
    ekin_g += pdata.Vg[i] * pdata.Vg[i];
    ekin_l += pdata.Vl[i] * pdata.Vl[i];
  }
  ekin_g *= 0.5;
  ekin_l *= 0.5;
  // internal energy = total energy - kinetic energy (per mass unit)
  double eint_g = cdata.mgEg / cdata.mg - ekin_g;
  double eint_l = cdata.mlEl / cdata.ml - ekin_l;

  double rho_g = pdata.mg / (pdata.ag);
  double rho_l = pdata.ml / (1.0 - pdata.ag);

  // use equation of state to compute pressure and local speed of sound
  pdata.Pg = get_pressure(rho_g, eint_g, m_param.eos_g);
  pdata.Pl = get_pressure(rho_l, eint_l, m_param.eos_l);

  pdata.Tg = get_temperature(rho_g, eint_g, m_param.eos_g);
  pdata.Tl = get_temperature(rho_l, eint_l, m_param.eos_l);
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::prim_to_cons(const prim_t &pdata, cons_t &cdata) {
  double ekin_g = 0.0;
  double ekin_l = 0.0;

  cdata.mg = pdata.mg;
  cdata.ml = pdata.ml;
  cdata.ag = pdata.ag;

  for (int i = 0; i < P4EST_DIM; ++i) {
    cdata.mgVg[i] = pdata.Vg[i] * pdata.mg;
    cdata.mlVl[i] = pdata.Vl[i] * pdata.ml;

    ekin_g += 0.5 * pdata.Vg[i] * pdata.Vg[i];
    ekin_l += 0.5 * pdata.Vl[i] * pdata.Vl[i];
  }

  double rho_g = pdata.mg / (pdata.ag);
  double rho_l = pdata.ml / (1.0 - pdata.ag);

  double eint_g = get_eint(rho_g, pdata.Pg, m_param.eos_g);
  double eint_l = get_eint(rho_l, pdata.Pl, m_param.eos_l);

  cdata.mgEg = pdata.mg * (eint_g + ekin_g);
  cdata.mlEl = pdata.ml * (eint_l + ekin_l);
}

//////////////////////////////////////////////////////////////////////////////
template <>
std::vector<double>
QdataManager<cons_t>::quad_get_field(const std::string &varName,
                                     p4est_quadrant_t *q) {

  qdata_t *qdata;
  cons_t w;

  if (q != nullptr) {

    qdata = quad_get_qdata(q);
    w = qdata->w;
  }

  std::vector<double> field;

  if (!varName.compare("mg")) {
    field.push_back(w.mg);
  } else if (!varName.compare("mgEg")) {
    field.push_back(w.mgEg);
  } else if (!varName.compare("ml")) {
    field.push_back(w.ml);
  } else if (!varName.compare("mlEl")) {
    field.push_back(w.mlEl);
  } else if (!varName.compare("ag")) {
    field.push_back(w.ag);
  } else if (!varName.compare("Pg")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    field.push_back(pdata.Pg);
  } else if (!varName.compare("Tg")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    field.push_back(pdata.Tg);
  } else if (!varName.compare("Pl")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    field.push_back(pdata.Pl);
  } else if (!varName.compare("Tl")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    field.push_back(pdata.Tl);
  } else if (!varName.compare("mgVg")) {
    for (int i = 0; i < P4EST_DIM; i++)
      field.push_back(w.mgVg[i]);
  } else if (!varName.compare("mlVl")) {
    for (int i = 0; i < P4EST_DIM; i++)
      field.push_back(w.mlVl[i]);
  } else if (!varName.compare("Vg")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    for (int i = 0; i < P4EST_DIM; i++)
      field.push_back(pdata.Vg[i]);
  } else if (!varName.compare("Vl")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    for (int i = 0; i < P4EST_DIM; i++)
      field.push_back(pdata.Vl[i]);
  } else {
    // print warning
    P4EST_GLOBAL_INFOF("Unrecognized field: \"%s\"\n", varName.c_str());
  }

  return field;
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::get_hydro_flux_predictor(int idir, const cons_t &cc,
                                                    const prim_t &pf,
                                                    cons_t &flux) {
  cons_t cf;

  prim_to_cons(pf, cf);

  // advection term
  flux.mg = cf.mg * pf.Vg[idir];
  flux.ml = cf.ml * pf.Vl[idir];

  for (int i = 0; i < P4EST_DIM; ++i) {
    flux.mgVg[i] = cf.mgVg[i] * pf.Vg[idir];
    flux.mlVl[i] = cf.mlVl[i] * pf.Vl[idir];
  }
  flux.mgEg = cf.mgEg * pf.Vg[idir];
  flux.mlEl = cf.mlEl * pf.Vl[idir];

  // pressure term
  flux.mgVg[idir] += pf.ag * pf.Pg;
  flux.mlVl[idir] += (1.0 - pf.ag) * pf.Pl;
  flux.mgEg += pf.ag * pf.Pg * pf.Vg[idir];
  flux.mlEl += (1.0 - pf.ag) * pf.Pl * pf.Vl[idir];

  // non conservative flux using central interface velocity and pressure
  double ui = get_interface_velocity(cc, &m_param, idir);
  double pi = get_interface_pressure(cc, &m_param, idir);

  flux.ag = ui * pf.ag;

  flux.mgVg[idir] -= pi * pf.ag;
  flux.mlVl[idir] += pi * pf.ag;

  flux.mgEg -= pi * ui * pf.ag;
  flux.mlEl += pi * ui * pf.ag;
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::get_grav_source_predictor(const prim_t &pdata,
                                                     cons_t &source) {

  source.mgVg[IX] = pdata.mg * m_param.gravity_x;
  source.mgVg[IY] = pdata.mg * m_param.gravity_y;

  source.mlVl[IX] = pdata.ml * m_param.gravity_x;
  source.mlVl[IY] = pdata.ml * m_param.gravity_y;

  source.mgEg = pdata.mg * pdata.Vg[IX] * m_param.gravity_x;
  source.mgEg += pdata.mg * pdata.Vg[IY] * m_param.gravity_y;

  source.mlEl = pdata.ml * pdata.Vl[IX] * m_param.gravity_x;
  source.mlEl += pdata.ml * pdata.Vl[IY] * m_param.gravity_y;

#if USE_3D
  source.mgVg[IZ] = pdata.mg * m_param.gravity_z;

  source.mlVl[IZ] = pdata.ml * m_param.gravity_z;

  source.mgEg += pdata.mg * pdata.Vg[IZ] * m_param.gravity_z;

  source.mlEl += pdata.ml * pdata.Vl[IZ] * m_param.gravity_z;
#endif
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::swap_dir(int idir, prim_t &pdata, cons_t &cdata,
                                    array<prim_t> &delta) {

  std::swap(pdata.Vg[IX], pdata.Vg[idir]);
  std::swap(pdata.Vl[IX], pdata.Vl[idir]);
  std::swap(cdata.mgVg[IX], cdata.mgVg[idir]);
  std::swap(cdata.mlVl[IX], cdata.mlVl[idir]);

  for (int d = 0; d < P4EST_DIM; ++d) {
    std::swap(delta[d].Vg[IX], delta[d].Vg[idir]);
    std::swap(delta[d].Vl[IX], delta[d].Vl[idir]);
  }

  prim_swap(delta[IX], delta[idir]);
}

//////////////////////////////////////////////////////////////////////////////
template <> void QdataManager<cons_t>::swap_dir(int idir, cons_t &cdata) {

  std::swap(cdata.mgVg[IX], cdata.mgVg[idir]);
  std::swap(cdata.mlVl[IX], cdata.mlVl[idir]);
}

//////////////////////////////////////////////////////////////////////////////
template <>
double QdataManager<cons_t>::get_hydro_invdt(const prim_t &pdata,
                                             const double dx) {
  double Vg = 0.0, Vl = 0.0;
  double cg = 0.0, cl = 0.0;

  double rho_g = pdata.mg / (pdata.ag);
  double rho_l = pdata.ml / (1.0 - pdata.ag);

  cg = get_soundspeed(rho_g, pdata.Pg, m_param.eos_g);

  cl = get_soundspeed(rho_l, pdata.Pl, m_param.eos_l);

  for (int d = 0; d < P4EST_DIM; ++d) {
    Vg += cg + fabs(pdata.Vg[d]);
    Vl += cl + fabs(pdata.Vl[d]);
  }

  return SC_MAX(Vg / dx, Vl / dx);
}

//////////////////////////////////////////////////////////////////////////////
template <>
double QdataManager<cons_t>::get_cond_invdt(const prim_t &pdata,
                                            const double dx) {
  double Cv_g = m_param.eos_g.Cv;
  double Cv_l = m_param.eos_l.Cv;

  double kappa_g = m_param.eos_g.kappa;
  double kappa_l = m_param.eos_l.kappa;

  double rho_g = pdata.mg / (pdata.ag);
  double rho_l = pdata.ml / (1.0 - pdata.ag);

  return SC_MAX(kappa_g / (rho_g * Cv_g * dx * dx),
                kappa_l / (rho_l * Cv_l * dx * dx));
}

//////////////////////////////////////////////////////////////////////////////
template <>
double QdataManager<cons_t>::get_visc_invdt(const prim_t &pdata,
                                            const double dx) {
  double mu_g = m_param.eos_g.mu;
  double mu_l = m_param.eos_l.mu;

  double rho_g = pdata.mg / (pdata.ag);
  double rho_l = pdata.ml / (1.0 - pdata.ag);

  return SC_MAX(mu_g / (rho_g * dx * dx), mu_l / (rho_l * dx * dx));
}
/// @}
