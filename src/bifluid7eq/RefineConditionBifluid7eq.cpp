//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataBifluid7eq.h"

using cons_t = bifluid7eq::cons_t;
using prim_t = bifluid7eq::prim_t;
using param_t = bifluid7eq::param_t;
using qdata_t = bifluid7eq::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// \brief m gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid7eq_m_gradient(qdata_t *cella, qdata_t *cellb) {

  double m[2] = {cella->w.mg + cella->w.ml, cellb->w.mg + cellb->w.ml};

  return scalar_gradient(m[0], m[1]);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief mg gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid7eq_mg_gradient(qdata_t *cella, qdata_t *cellb) {

  double mg[2] = {cella->w.mg, cellb->w.mg};

  return scalar_gradient(mg[0], mg[1]);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief ml gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid7eq_ml_gradient(qdata_t *cella, qdata_t *cellb) {

  double ml[2] = {cella->w.ml, cellb->w.ml};

  return scalar_gradient(ml[0], ml[1]);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief ag gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid7eq_ag_gradient(qdata_t *cella, qdata_t *cellb) {

  double ag[2] = {cella->w.ag, cellb->w.ag};

  return scalar_gradient(ag[0], ag[1]);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief rhoV gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid7eq_mV_gradient(qdata_t *cella, qdata_t *cellb) {

  double mVx[2] = {cella->w.mgVg[IX] + cella->w.mlVl[IX],
                   cellb->w.mgVg[IX] + cellb->w.mlVl[IX]};

  double mVx_grad = scalar_gradient(mVx[0], mVx[1]);

  double mVy[2] = {cella->w.mgVg[IY] + cella->w.mlVl[IY],
                   cellb->w.mgVg[IY] + cellb->w.mlVl[IY]};

  double mVy_grad = scalar_gradient(mVy[0], mVy[1]);

  double grad = fmax(mVx_grad, mVy_grad);

#ifdef USE_3D
  double mVz[2] = {cella->w.mgVg[IZ] + cella->w.mlVl[IZ],
                   cellb->w.mgVg[IZ] + cellb->w.mlVl[IZ]};

  double mVz_grad = scalar_gradient(mVz[0], mVz[1]);

  grad = fmax(grad, mVz_grad);
#endif

  return grad;
}

//////////////////////////////////////////////////////////////////////////////
/// \brief mE gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid7eq_mE_gradient(qdata_t *cella, qdata_t *cellb) {

  double mE[2] = {cella->w.mgEg + cella->w.mlEl, cellb->w.mgEg + cellb->w.mlEl};

  return scalar_gradient(mE[0], mE[1]);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief any gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid7eq_any_gradient(qdata_t *cella, qdata_t *cellb) {

  double m_grad = rc_bifluid7eq_m_gradient(cella, cellb);
  double mg_grad = rc_bifluid7eq_mg_gradient(cella, cellb);
  double ml_grad = rc_bifluid7eq_ml_gradient(cella, cellb);
  double mV_grad = rc_bifluid7eq_mV_gradient(cella, cellb);
  double mE_grad = rc_bifluid7eq_mE_gradient(cella, cellb);
  double ag_grad = rc_bifluid7eq_ag_gradient(cella, cellb);

  double grad = fmax(m_grad, mg_grad);
  grad = fmax(grad, ml_grad);
  grad = fmax(grad, mV_grad);
  grad = fmax(grad, mE_grad);
  grad = fmax(grad, ag_grad);

  return grad;
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing refine conditions for bifluid7eq
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_rc_callback_t<qdata_t>>::Factory() {

  // register the above callback's
  register_factory("ag_gradient", rc_bifluid7eq_ag_gradient);
  register_factory("m_gradient", rc_bifluid7eq_m_gradient);
  register_factory("mg_gradient", rc_bifluid7eq_mg_gradient);
  register_factory("ml_gradient", rc_bifluid7eq_ml_gradient);
  register_factory("mV_gradient", rc_bifluid7eq_mV_gradient);
  register_factory("mE_gradient", rc_bifluid7eq_mE_gradient);
  register_factory("any_gradient", rc_bifluid7eq_any_gradient);
}
