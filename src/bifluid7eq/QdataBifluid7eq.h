//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef QDATA_BIFLUID7EQ_H_
#define QDATA_BIFLUID7EQ_H_

#include "Factory.h"
#include "Qdata.h"
#include "eos.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief Type containing conservative variables for a bifluid7eq model
//////////////////////////////////////////////////////////////////////////////
struct bifluid7eq_cons_t {

  static constexpr unsigned nvar = 5 + 2 * P4EST_DIM;

  double mg;          //!< mass of gas per unit volume
  array<double> mgVg; //!< 2D/3D momentum  of the gas phase per unit volume
  double mgEg;        //!< total energy of the gas phase per unit volume
  double ml;          //!< mass of liquid per unit volume
  array<double> mlVl; //!< 2D/3D momentum of the liquid phase per unit volume
  double mlEl;        //!< total energy of of the liquid phase per unit volume
  double ag;          //!< gas volume fraction

  inline bifluid7eq_cons_t() : mg(0.0), mgEg(0.0), ml(0.0), mlEl(0.0), ag(0.0) {
    mgVg.fill(0.0);
    mlVl.fill(0.0);
  }
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Type containing primitive variables for a bifluid7eq model
//////////////////////////////////////////////////////////////////////////////
struct bifluid7eq_prim_t {

  static constexpr unsigned nvar = 7 + 2 * P4EST_DIM;

  double mg;        //!< mass of gas per unit volume
  array<double> Vg; //!< 2D/3D velocity of the gas phase
  double Pg;        //!< pressure of the gas phase
  double Tg;        //!< temperature of the gas phase
  double ml;        //!< mass of liquid per unit volume
  array<double> Vl; //!< 2D/3D velocity of liquid
  double Pl;        //!< pressure of liquid
  double Tl;        //!< temperature of liquid
  double ag;        //!< gas volume fraction

  inline bifluid7eq_prim_t()
      : mg(0.0), Pg(0.0), Tg(0.0), ml(0.0), Pl(0.0), Tl(0.0), ag(0.0) {
    Vg.fill(0.0);
    Vl.fill(0.0);
  }
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Bifluid7eq primitive type deduction from conservative type
//////////////////////////////////////////////////////////////////////////////
template <> struct to_prim<bifluid7eq_cons_t> {
  using type = bifluid7eq_prim_t;
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Type containing parameters for a bifluid7eq model
/// (read from the param group in the setting file)
//////////////////////////////////////////////////////////////////////////////
struct bifluid7eq_param_t {

  eos_t eos_g; //!< parameters of the stiffened-gas eos for the gas phase
               //!< (default gamma=1.666, Cv=1)
  eos_t eos_l; //!< parameters of the stiffened-gas eos for the liquid phase
               //!< (default gamma=1.666, Cv=1)

  double gravity_x; //!< gravitational force in the x direction (default 0.0)
  double gravity_y; //!< gravitational force in the y direction (default 0.0)
  double gravity_z; //!< gravitational force in the z direction (default 0.0)

  // for all-regime scheme, not yet implemented
  // int lowmach_correction_enabled;
  // double K;

  int interface_mode; //!< closure for the interface velocity or pressure
                      //!< (default 1, options 1 to 5, see overview)
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Bifluid7eq parameter type deduction from conservative type
//////////////////////////////////////////////////////////////////////////////
template <> struct to_param<bifluid7eq_cons_t> {
  using type = bifluid7eq_param_t;
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Namespace containing types for a bifluid7eq simulation
/// with additional traits
//////////////////////////////////////////////////////////////////////////////
namespace bifluid7eq {

using cons_t = generic_data_t<bifluid7eq_cons_t>;
using prim_t = to_prim_t<cons_t>;
using qdata_t = to_qdata_t<cons_t>;
using param_t = to_param_t<cons_t>;

}; // namespace bifluid7eq

#ifndef SKIPPED_BY_DOXYGEN

/// Factory storing initiation conditions for bifluid7eq
template <>
Factory<bifluid7eq::qdata_t, to_ic_callback_t<bifluid7eq::qdata_t>>::Factory();

/// Factory storing boundary conditions for bifluid7eq
template <>
Factory<bifluid7eq::qdata_t, to_bc_callback_t<bifluid7eq::qdata_t>>::Factory();

/// Factory storing refine conditions for bifluid7eq
template <>
Factory<bifluid7eq::qdata_t, to_rc_callback_t<bifluid7eq::qdata_t>>::Factory();

/// Factory storing hyberbolic fluxes for bifluid7eq
template <>
Factory<bifluid7eq::qdata_t, to_hf_callback_t<bifluid7eq::cons_t>>::Factory();

/// Factory storing source terms for bifluid7eq
template <>
Factory<bifluid7eq::qdata_t, to_st_callback_t<bifluid7eq::cons_t>>::Factory();

/// Factory storing diffusion fluxes for bifluid7eq
template <>
Factory<bifluid7eq::qdata_t, to_df_callback_t<bifluid7eq::cons_t>>::Factory();

//////////////////////////////////////////////////////////////////////////////
/// @name specialization for bifluid7eq
/// @{
//////////////////////////////////////////////////////////////////////////////
template <>
QdataManager<bifluid7eq::cons_t>::QdataManager(SettingManager *stg_mgr);
template <>
void QdataManager<bifluid7eq::cons_t>::cons_to_prim(const bifluid7eq::cons_t &w,
                                                    bifluid7eq::prim_t &r);
template <>
void QdataManager<bifluid7eq::cons_t>::prim_to_cons(
    const bifluid7eq::prim_t &pdata, bifluid7eq::cons_t &cdata);
template <>
void QdataManager<bifluid7eq::cons_t>::get_hydro_flux_predictor(
    int idir, const bifluid7eq::cons_t &cc, const bifluid7eq::prim_t &pf,
    bifluid7eq::cons_t &flux);
template <>
void QdataManager<bifluid7eq::cons_t>::get_grav_source_predictor(
    const bifluid7eq::prim_t &pdata, bifluid7eq::cons_t &source);
template <>
std::vector<double>
QdataManager<bifluid7eq::cons_t>::quad_get_field(const std::string &field_name,
                                                 p4est_quadrant_t *quad);
template <>
void QdataManager<bifluid7eq::cons_t>::swap_dir(
    int idir, bifluid7eq::prim_t &pdata, bifluid7eq::cons_t &cdata,
    array<bifluid7eq::prim_t> &delta);
template <>
double QdataManager<bifluid7eq::cons_t>::get_hydro_invdt(
    const bifluid7eq::prim_t &pdata, const double dx);
template <>
double QdataManager<bifluid7eq::cons_t>::get_cond_invdt(
    const bifluid7eq::prim_t &pdata, const double dx);
template <>
double QdataManager<bifluid7eq::cons_t>::get_visc_invdt(
    const bifluid7eq::prim_t &pdata, const double dx);
/// @}

#endif

inline double get_interface_pressure(bifluid7eq::cons_t u,
                                     bifluid7eq::param_t *param, int idir) {
  UNUSED(idir);

  double PI = 0;

  double eken1 = 0.0;
  double eken2 = 0.0;

  for (int i = 0; i < P4EST_DIM; ++i) {
    eken1 += u.mgVg[i] * u.mgVg[i] / (u.mg * u.mg);
    eken2 += u.mlVl[i] * u.mlVl[i] / (u.ml * u.ml);
  }
  eken1 *= 0.5;
  eken2 *= 0.5;

  // internal energy = total energy - kinetic energy (per mass unit)
  double eint1 = u.mgEg / u.mg - eken1;
  double eint2 = u.mlEl / u.ml - eken2;

  double r1 = u.mg / u.ag;
  double r2 = u.ml / (1.0 - u.ag);

  double p1 = get_pressure(r1, eint1, param->eos_g);
  double p2 = get_pressure(r2, eint2, param->eos_l);

  if (param->interface_mode == 1) {

    PI = p2; // exact Baer-Nunziato model

  } else if (param->interface_mode == 2) {

    PI = p1;

  } else if (param->interface_mode == 3) {

    PI = u.ag * p1 + (1 - u.ag) * p2;

  } else if (param->interface_mode == 4) {

    PI = u.ag * p1 + (1 - u.ag) * p2;

  } else {
    PI = (p1 + p2) / 2;
  }

  return PI;
};

inline double get_interface_velocity(bifluid7eq::cons_t u,
                                     bifluid7eq::param_t *param, int idir) {

  double UI = 0;

  double u1 = u.mgVg[idir] / u.mg;
  double u2 = u.mlVl[idir] / u.ml;

  if (param->interface_mode == 1) {

    UI = u1; // exact Baer-Nunziato model

  } else if (param->interface_mode == 2) {

    UI = u2;

  } else if (param->interface_mode == 3) {

    UI = (u.mg * u1 + u.ml * u2) / (u.mg + u.ml);

  } else if (param->interface_mode == 4) {

    UI = (u.ag * u1 + (1.0 - u.ag) * u2);

  } else {

    UI = (u1 + u2) / 2;

  } // end for choice

  return UI;
};

#endif
