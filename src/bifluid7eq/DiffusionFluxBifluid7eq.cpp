//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataBifluid7eq.h"

using cons_t = bifluid7eq::cons_t;
using prim_t = bifluid7eq::prim_t;
using param_t = bifluid7eq::param_t;
using qdata_t = bifluid7eq::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// Viscosity diffusion flux
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void diffusion_flux_viscosity(prim_t &ql, prim_t &qr, array<prim_t> &deltal,
                              array<prim_t> &deltar, cons_t *fluxl,
                              cons_t *fluxr, QdataManager<cons_t> *qdata_mgr) {

  param_t *param = qdata_mgr->get_param();

  double agface = 0.5 * (ql.ag + qr.ag);
  double alface = 0.5 * (1.0 - ql.ag + 1.0 - qr.ag);

  double mug = param->eos_g.mu;
  double etag = param->eos_g.eta;

  double mul = param->eos_l.mu;
  double etal = param->eos_l.eta;

  double ugface = 0.5 * (ql.Vg[IX] + qr.Vg[IX]);
  double vgface = 0.5 * (ql.Vg[IY] + qr.Vg[IY]);

  double ulface = 0.5 * (ql.Vl[IX] + qr.Vl[IX]);
  double vlface = 0.5 * (ql.Vl[IY] + qr.Vl[IY]);

  double dug_dx = 0.5 * (deltal[IX].Vg[IX] + deltar[IX].Vg[IX]);
  double dvg_dy = 0.5 * (deltal[IY].Vg[IY] + deltar[IY].Vg[IY]);
  double dug_dy = 0.5 * (deltal[IY].Vg[IX] + deltar[IY].Vg[IX]);
  double dvg_dx = 0.5 * (deltal[IX].Vg[IY] + deltar[IX].Vg[IY]);

  double dul_dx = 0.5 * (deltal[IX].Vl[IX] + deltar[IX].Vl[IX]);
  double dvl_dy = 0.5 * (deltal[IY].Vl[IY] + deltar[IY].Vl[IY]);
  double dul_dy = 0.5 * (deltal[IY].Vl[IX] + deltar[IY].Vl[IX]);
  double dvl_dx = 0.5 * (deltal[IX].Vl[IY] + deltar[IX].Vl[IY]);

#ifdef USE_3D
  double wgface = 0.5 * (ql.Vg[IZ] + qr.Vg[IZ]);

  double wlface = 0.5 * (ql.Vl[IZ] + qr.Vl[IZ]);

  double dug_dz = 0.5 * (deltal[IZ].Vg[IX] + deltar[IZ].Vg[IX]);
  double dwg_dx = 0.5 * (deltal[IX].Vg[IZ] + deltar[IX].Vg[IZ]);
  double dwg_dz = 0.5 * (deltal[IZ].Vg[IZ] + deltar[IZ].Vg[IZ]);

  double dul_dz = 0.5 * (deltal[IZ].Vl[IX] + deltar[IZ].Vl[IX]);
  double dwl_dx = 0.5 * (deltal[IX].Vl[IZ] + deltar[IX].Vl[IZ]);
  double dwl_dz = 0.5 * (deltal[IZ].Vl[IZ] + deltar[IZ].Vl[IZ]);
#endif

  double taug_xx = 2. * mug * dug_dx + etag * (dug_dx + dvg_dy);
  double taug_xy = mug * (dug_dy + dvg_dx);

  double taul_xx = 2. * mul * dul_dx + etal * (dul_dx + dvl_dy);
  double taul_xy = mul * (dul_dy + dvl_dx);

#ifdef USE_3D
  taug_xx += etag * dwg_dz;
  double taug_xz = mug * (dug_dz + dwg_dx);

  taul_xx += etal * dwl_dz;
  double taul_xz = mul * (dul_dz + dwl_dx);
#endif

  fluxl->mgVg[IX] -= agface * taug_xx;
  fluxl->mgVg[IY] -= agface * taug_xy;
  fluxl->mgEg -= agface * (taug_xx * ugface + taug_xy * vgface);

  fluxl->mlVl[IX] -= alface * taul_xx;
  fluxl->mlVl[IY] -= alface * taul_xy;
  fluxl->mlEl -= alface * (taul_xx * ulface + taul_xy * vlface);

#ifdef USE_3D
  fluxl->mgVg[IZ] -= agface * taug_xz;
  fluxl->mgEg -= agface * taug_xz * wgface;

  fluxl->mlVl[IZ] -= alface * taul_xz;
  fluxl->mlEl -= alface * taul_xz * wlface;
#endif

  fluxr->mgVg[IX] -= agface * taug_xx;
  fluxr->mgVg[IY] -= agface * taug_xy;
  fluxr->mgEg -= agface * (taug_xx * ugface + taug_xy * vgface);

  fluxr->mlVl[IX] -= alface * taul_xx;
  fluxr->mlVl[IY] -= alface * taul_xy;
  fluxr->mlEl -= alface * (taul_xx * ulface + taul_xy * vlface);

#ifdef USE_3D
  fluxr->mgVg[IZ] -= agface * taug_xz;
  fluxr->mgEg -= agface * taug_xz * wgface;

  fluxr->mlVl[IZ] -= alface * taul_xz;
  fluxr->mlEl -= alface * taul_xz * wlface;
#endif
}

//////////////////////////////////////////////////////////////////////////////
/// Conductivity diffusion flux
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void diffusion_flux_conductivity(prim_t &ql, prim_t &qr, array<prim_t> &deltal,
                                 array<prim_t> &deltar, cons_t *fluxl,
                                 cons_t *fluxr,
                                 QdataManager<cons_t> *qdata_mgr) {
  UNUSED(ql);
  UNUSED(qr);

  param_t *param = qdata_mgr->get_param();

  double agface = 0.5 * (ql.ag + qr.ag);
  double alface = 0.5 * (1.0 - ql.ag + 1.0 - qr.ag);

  double kappag = param->eos_g.kappa;
  double kappal = param->eos_l.kappa;

  double dTg_dx = 0.5 * (deltal[IX].Tg + deltar[IX].Tg);
  double dTl_dx = 0.5 * (deltal[IX].Tl + deltar[IX].Tl);

  fluxl->mgEg -= agface * kappag * dTg_dx;
  fluxl->mlEl -= alface * kappal * dTl_dx;

  fluxr->mgEg -= agface * kappag * dTg_dx;
  fluxr->mlEl -= alface * kappal * dTl_dx;
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing diffusion fluxes for bifluid7eq
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_df_callback_t<cons_t>>::Factory() {

  // register the above callback's
  register_factory("none", nullptr);
  register_factory("viscosity_enabled", diffusion_flux_viscosity);
  register_factory("conductivity_enabled", diffusion_flux_conductivity);
}
