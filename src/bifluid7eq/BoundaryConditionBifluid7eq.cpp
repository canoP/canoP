//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataBifluid7eq.h"
#include "Simulation.h"

using cons_t = bifluid7eq::cons_t;
using prim_t = bifluid7eq::prim_t;
using param_t = bifluid7eq::param_t;
using qdata_t = bifluid7eq::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// \brief dirichlet boundary condition
///
/// Imposed values
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid7eq_dirichlet(p4est_t *p4est, p4est_topidx_t which_tree,
                             p4est_quadrant_t *q, int face,
                             qdata_t *ghost_qdata) {
  UNUSED(which_tree), UNUSED(q);

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  SettingManager *stg_mgr = simu->m_stg_mgr;

  double mg = 0., Pg = 0., Vgx = 0., Vgy = 0., Vgz = 0., ag = 0.;
  double ml = 0., Pl = 0., Vlx = 0., Vly = 0., Vlz = 0.;

  if (face == 0) {
    ag = stg_mgr->read_double("dirichlet.face0.ag", 0.0);
    mg = stg_mgr->read_double("dirichlet.face0.mg", 1.0);
    ml = stg_mgr->read_double("dirichlet.face0.ml", 1.0);
    Pg = stg_mgr->read_double("dirichlet.face0.Pg", 1.0);
    Pl = stg_mgr->read_double("dirichlet.face0.Pl", 1.0);
    Vgx = stg_mgr->read_double("dirichlet.face0.Vgx", 0.0);
    Vgy = stg_mgr->read_double("dirichlet.face0.Vgy", 0.0);
    Vgz = stg_mgr->read_double("dirichlet.face0.Vgz", 0.0);
    Vlx = stg_mgr->read_double("dirichlet.face0.Vlx", 0.0);
    Vly = stg_mgr->read_double("dirichlet.face0.Vly", 0.0);
    Vlz = stg_mgr->read_double("dirichlet.face0.Vlz", 0.0);
  } else if (face == 1) {
    ag = stg_mgr->read_double("dirichlet.face1.ag", 0.0);
    mg = stg_mgr->read_double("dirichlet.face1.mg", 1.0);
    ml = stg_mgr->read_double("dirichlet.face1.ml", 1.0);
    Pg = stg_mgr->read_double("dirichlet.face1.Pg", 1.0);
    Pl = stg_mgr->read_double("dirichlet.face1.Pl", 1.0);
    Vgx = stg_mgr->read_double("dirichlet.face1.Vgx", 0.0);
    Vgy = stg_mgr->read_double("dirichlet.face1.Vgy", 0.0);
    Vgz = stg_mgr->read_double("dirichlet.face1.Vgz", 0.0);
    Vlx = stg_mgr->read_double("dirichlet.face1.Vlx", 0.0);
    Vly = stg_mgr->read_double("dirichlet.face1.Vly", 0.0);
    Vlz = stg_mgr->read_double("dirichlet.face1.Vlz", 0.0);
  } else if (face == 2) {
    ag = stg_mgr->read_double("dirichlet.face2.ag", 0.0);
    mg = stg_mgr->read_double("dirichlet.face2.mg", 1.0);
    ml = stg_mgr->read_double("dirichlet.face2.ml", 1.0);
    Pg = stg_mgr->read_double("dirichlet.face2.Pg", 1.0);
    Pl = stg_mgr->read_double("dirichlet.face2.Pl", 1.0);
    Vgx = stg_mgr->read_double("dirichlet.face2.Vgx", 0.0);
    Vgy = stg_mgr->read_double("dirichlet.face2.Vgy", 0.0);
    Vgz = stg_mgr->read_double("dirichlet.face2.Vgz", 0.0);
    Vlx = stg_mgr->read_double("dirichlet.face2.Vlx", 0.0);
    Vly = stg_mgr->read_double("dirichlet.face2.Vly", 0.0);
    Vlz = stg_mgr->read_double("dirichlet.face2.Vlz", 0.0);
  } else if (face == 3) {
    ag = stg_mgr->read_double("dirichlet.face3.ag", 0.0);
    mg = stg_mgr->read_double("dirichlet.face3.mg", 1.0);
    ml = stg_mgr->read_double("dirichlet.face3.ml", 1.0);
    Pg = stg_mgr->read_double("dirichlet.face3.Pg", 1.0);
    Pl = stg_mgr->read_double("dirichlet.face3.Pl", 1.0);
    Vgx = stg_mgr->read_double("dirichlet.face3.Vgx", 0.0);
    Vgy = stg_mgr->read_double("dirichlet.face3.Vgy", 0.0);
    Vgz = stg_mgr->read_double("dirichlet.face3.Vgz", 0.0);
    Vlx = stg_mgr->read_double("dirichlet.face3.Vlx", 0.0);
    Vly = stg_mgr->read_double("dirichlet.face3.Vly", 0.0);
    Vlz = stg_mgr->read_double("dirichlet.face3.Vlz", 0.0);
  } else if (face == 4) {
    ag = stg_mgr->read_double("dirichlet.face4.ag", 0.0);
    mg = stg_mgr->read_double("dirichlet.face4.mg", 1.0);
    ml = stg_mgr->read_double("dirichlet.face4.ml", 1.0);
    Pg = stg_mgr->read_double("dirichlet.face4.Pg", 1.0);
    Pl = stg_mgr->read_double("dirichlet.face4.Pl", 1.0);
    Vgx = stg_mgr->read_double("dirichlet.face4.Vgx", 0.0);
    Vgy = stg_mgr->read_double("dirichlet.face4.Vgy", 0.0);
    Vgz = stg_mgr->read_double("dirichlet.face4.Vgz", 0.0);
    Vlx = stg_mgr->read_double("dirichlet.face4.Vlx", 0.0);
    Vly = stg_mgr->read_double("dirichlet.face4.Vly", 0.0);
    Vlz = stg_mgr->read_double("dirichlet.face4.Vlz", 0.0);
  } else if (face == 5) {
    ag = stg_mgr->read_double("dirichlet.face5.ag", 0.0);
    mg = stg_mgr->read_double("dirichlet.face5.mg", 1.0);
    ml = stg_mgr->read_double("dirichlet.face5.ml", 1.0);
    Pg = stg_mgr->read_double("dirichlet.face5.Pg", 1.0);
    Pl = stg_mgr->read_double("dirichlet.face5.Pl", 1.0);
    Vgx = stg_mgr->read_double("dirichlet.face5.Vgx", 0.0);
    Vgy = stg_mgr->read_double("dirichlet.face5.Vgy", 0.0);
    Vgz = stg_mgr->read_double("dirichlet.face5.Vgz", 0.0);
    Vlx = stg_mgr->read_double("dirichlet.face5.Vlx", 0.0);
    Vly = stg_mgr->read_double("dirichlet.face5.Vly", 0.0);
    Vlz = stg_mgr->read_double("dirichlet.face5.Vlz", 0.0);
  }
  UNUSED(Vgz);
  UNUSED(Vlz);

  prim_t w_prim;

  w_prim.ag = ag;
  w_prim.mg = mg;
  w_prim.ml = ml;
  w_prim.Pg = Pg;
  w_prim.Pl = Pl;
  w_prim.Vg[IX] = Vgx;
  w_prim.Vg[IY] = Vgy;
  w_prim.Vl[IX] = Vlx;
  w_prim.Vl[IY] = Vly;
#ifdef USE_3D
  w_prim.Vg[IX] = Vgz;
  w_prim.Vl[IX] = Vlz;
#endif

  ghost_qdata->wm[IX] = w_prim;
  ghost_qdata->wp[IX] = w_prim;
  ghost_qdata->wm[IY] = w_prim;
  ghost_qdata->wp[IY] = w_prim;
#ifdef USE_3D
  ghost_qdata->wm[IZ] = w_prim;
  ghost_qdata->wp[IZ] = w_prim;
#endif

  /* set slopes to zero */
  qdata_mgr->prim_zero(ghost_qdata->delta[IX]);
  qdata_mgr->prim_zero(ghost_qdata->delta[IY]);
#ifdef USE_3D
  qdata_mgr->prim_zero(ghost_qdata->delta[IZ]);
#endif // USE_3D
}

//////////////////////////////////////////////////////////////////////////////
/// \brief neuman boundary condition
///
/// Imposed null gradient
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid7eq_neuman(p4est_t *p4est, p4est_topidx_t which_tree,
                          p4est_quadrant_t *q, int face, qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);
  UNUSED(face);

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /*
   * copy the entire qdata is necessary for bifluid7eq scheme
   * so that we have the slope and MUSCL-Hancock extrapoled states
   * and can perform the trace operation
   */
  qdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* wp / wm should also be initialized */
  {
    prim_t w_prim;

    /* get the primitive variables in the current quadrant */
    qdata_mgr->cons_to_prim(ghost_qdata->w, w_prim);

    ghost_qdata->wm[IX] = w_prim;
    ghost_qdata->wp[IX] = w_prim;
    ghost_qdata->wm[IY] = w_prim;
    ghost_qdata->wp[IY] = w_prim;
#ifdef USE_3D
    ghost_qdata->wm[IZ] = w_prim;
    ghost_qdata->wp[IZ] = w_prim;
#endif
  }

  /* set slopes to zero */
  qdata_mgr->prim_zero(ghost_qdata->delta[IX]);
  qdata_mgr->prim_zero(ghost_qdata->delta[IY]);
#ifdef USE_3D
  qdata_mgr->prim_zero(ghost_qdata->delta[IZ]);
#endif // USE_3D
}

//////////////////////////////////////////////////////////////////////////////
/// \brief reflective boundary condition
///
/// Copy values and switch the sign of normal velocity
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid7eq_reflective(p4est_t *p4est, p4est_topidx_t which_tree,
                              p4est_quadrant_t *q, int face,
                              qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);

  /* get face normal direction */
  int direction = face / 2;

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /* copy current into the ghost outside-boundary cell */
  qdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* reverse the normal V */
  ghost_qdata->w.mgVg[direction] = -ghost_qdata->w.mgVg[direction];
  ghost_qdata->wnext.mgVg[direction] = -ghost_qdata->wnext.mgVg[direction];

  ghost_qdata->w.mlVl[direction] = -ghost_qdata->w.mlVl[direction];
  ghost_qdata->wnext.mlVl[direction] = -ghost_qdata->wnext.mlVl[direction];

  // slopes in normal direction change sign
  qdata_mgr->prim_change_sign(ghost_qdata->delta[direction]);

  // swap wm and wp in normal direction:
  // so that qm on the right side is the same as qp on the left side
  qdata_mgr->prim_swap(ghost_qdata->wp[direction], ghost_qdata->wm[direction]);
  ghost_qdata->wp[direction].Vg[direction] =
      -ghost_qdata->wp[direction].Vg[direction];
  ghost_qdata->wm[direction].Vg[direction] =
      -ghost_qdata->wm[direction].Vg[direction];
  ghost_qdata->wp[direction].Vl[direction] =
      -ghost_qdata->wp[direction].Vl[direction];
  ghost_qdata->wm[direction].Vl[direction] =
      -ghost_qdata->wm[direction].Vl[direction];
}

//////////////////////////////////////////////////////////////////////////////
/// \brief no-slip boundary condition
///
/// change sign of velocity in transverse direction
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid7eq_noslip(p4est_t *p4est, p4est_topidx_t which_tree,
                          p4est_quadrant_t *q, int face, qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);

  /* get face normal direction */
  int direction = face / 2;

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /* copy current into the ghost outside-boundary cell */
  qdata_mgr->qdata_copy(ghost_qdata, qdata);

  double dx = amr_mgr->quad_dx(q);

  /* reverse V */
  for (int i = 0; i < P4EST_DIM; i++) {
    ghost_qdata->w.mgVg[i] = -ghost_qdata->w.mgVg[i];
    ghost_qdata->wnext.mgVg[i] = -ghost_qdata->wnext.mgVg[i];

    ghost_qdata->w.mlVl[i] = -ghost_qdata->w.mlVl[i];
    ghost_qdata->wnext.mlVl[i] = -ghost_qdata->wnext.mlVl[i];

    for (int j = 0; j < P4EST_DIM; j++) {
      ghost_qdata->wp[j].Vg[i] = -ghost_qdata->wp[j].Vg[i];
      ghost_qdata->wm[j].Vg[i] = -ghost_qdata->wm[j].Vg[i];

      ghost_qdata->wp[j].Vl[i] = -ghost_qdata->wp[j].Vl[i];
      ghost_qdata->wm[j].Vl[i] = -ghost_qdata->wm[j].Vl[i];
    }
  }

  // slope change sign
  qdata_mgr->prim_change_sign(ghost_qdata->delta[direction]);

  // swap wm and wp:
  // so that qm on the right side is the same as qp on the left side
  qdata_mgr->prim_swap(ghost_qdata->wp[direction], ghost_qdata->wm[direction]);

  // change gradients in transverse direction
  for (int i = 0; i < P4EST_DIM; i++) {
    for (int j = 0; j < P4EST_DIM; j++) {

      if (!(i == direction) & !(j == direction)) {
        ghost_qdata->delta[j].Vg[i] = -ghost_qdata->delta[j].Vg[i];
        ghost_qdata->delta[j].Vl[i] = -ghost_qdata->delta[j].Vl[i];
      }
    }

    if (!(i == direction)) {

      if (face % 2 == 1) {
        ghost_qdata->delta[direction].Vg[i] =
            2 * ghost_qdata->w.mgVg[i] / (ghost_qdata->w.mg * dx);
        ghost_qdata->delta[direction].Vl[i] =
            2 * ghost_qdata->w.mlVl[i] / (ghost_qdata->w.ml * dx);
      } else {
        ghost_qdata->delta[direction].Vg[i] =
            -2 * ghost_qdata->w.mgVg[i] / (ghost_qdata->w.mg * dx);
        ghost_qdata->delta[direction].Vl[i] =
            -2 * ghost_qdata->w.mlVl[i] / (ghost_qdata->w.ml * dx);
      }
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
/// \brief poiseuille boundary condition
///
/// Imposed pressure gradient in x direction, no-slip otherwise
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid7eq_poiseuille(p4est_t *p4est, p4est_topidx_t which_tree,
                              p4est_quadrant_t *q, int face,
                              qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);
  UNUSED(face);

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  param_t *param = qdata_mgr->get_param();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  double dx = amr_mgr->quad_dx(q);

  // get the coordinates of the center of the quad
  double XYZ[3] = {0, 0, 0};
  double x, y, z;
  UNUSED(y);
  UNUSED(z);

  // get the physical coordinates of the center of the quad
  amr_mgr->quad_centre(which_tree, q, XYZ);

  x = XYZ[IX];
  y = XYZ[IY];
  z = XYZ[IZ];

  double gradP = simu->m_stg_mgr->read_double("poiseuille.gradP", 0.0);
  double P0 = simu->m_stg_mgr->read_double("poiseuille.P0", 1.0);

  if (face == 2 || face == 3 || face == 4 || face == 5) {

    bc_bifluid7eq_noslip(p4est, which_tree, q, face, ghost_qdata);

  } else if (face == 0 || face == 1) {

    if (face == 0) {
      x -= dx;
    } else {
      x += dx;
    }

    /*
     * copy the entire qdata is necessary for bifluid7eq scheme
     * so that we have the slope and MUSCL-Hancock extrapoled states
     * and can perform the trace operation
     */
    qdata_mgr->qdata_copy(ghost_qdata, qdata);

    prim_t w_prim;

    /* get the primitive variables in the current quadrant */
    qdata_mgr->cons_to_prim(ghost_qdata->w, w_prim);

    w_prim.Pg = P0 + gradP * x;
    w_prim.Pl = P0 + gradP * x;

    double ekin1 =
        0.5 * (w_prim.Vg[IX] * w_prim.Vg[IX] + w_prim.Vg[IY] * w_prim.Vg[IY]) *
        w_prim.mg;

    double ekin2 =
        0.5 * (w_prim.Vl[IX] * w_prim.Vl[IX] + w_prim.Vl[IY] * w_prim.Vl[IY]) *
        w_prim.ml;
#ifdef USE_3D
    ekin1 += w_prim.Vg[IZ] * w_prim.Vg[IZ] * w_prim.mg;
    ekin2 += w_prim.Vl[IZ] * w_prim.Vl[IZ] * w_prim.ml;
#endif // USE_3D

    ghost_qdata->w.mgEg =
        w_prim.mg * get_eint(w_prim.mg / w_prim.ag, w_prim.Pg, param->eos_g) +
        ekin1;

    ghost_qdata->w.mlEl = w_prim.ml * get_eint(w_prim.ml / (1.0 - w_prim.ag),
                                               w_prim.Pl, param->eos_l) +
                          ekin2;
    ghost_qdata->wp[IX] = w_prim;
    ghost_qdata->wm[IX] = w_prim;
    ghost_qdata->delta[IX].Pg = gradP;
    ghost_qdata->delta[IX].Pl = gradP;
  }
}

//////////////////////////////////////////////////////////////////////////////
/// \brief jet boundary condition
///
/// Imposed values in a opening, reflective otherwise
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid7eq_jet(p4est_t *p4est, p4est_topidx_t which_tree,
                       p4est_quadrant_t *q, int face, qdata_t *ghost_qdata) {

  UNUSED(which_tree);
  UNUSED(q);
  UNUSED(face);

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  param_t *param = qdata_mgr->get_param();

  SettingManager *stg_mgr = simu->m_stg_mgr;

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /* one can specify the inflow face in the parameter file */
  int jetface = stg_mgr->read_int("jet.face", 0);
  double x_c = stg_mgr->read_double("jet.x_c", 0.0);
  double y_c = stg_mgr->read_double("jet.y_c", 0.5);
  double z_c = stg_mgr->read_double("jet.z_c", 0.5);
  double radius = stg_mgr->read_double("jet.radius", 0.05);
  double radius2 = radius * radius;

  UNUSED(z_c);

  if (face == jetface) { // inflow

    /* get face normal direction */
    int direction = jetface / 2;

    // reflective
    /* copy current cell data into the ghost outside-boundary cell */
    qdata_mgr->qdata_copy(ghost_qdata, qdata);

    /* reverse the normal V */
    ghost_qdata->w.mgVg[direction] = -ghost_qdata->w.mgVg[direction];
    ghost_qdata->wnext.mgVg[direction] = -ghost_qdata->wnext.mgVg[direction];

    ghost_qdata->w.mlVl[direction] = -ghost_qdata->w.mlVl[direction];
    ghost_qdata->wnext.mlVl[direction] = -ghost_qdata->wnext.mlVl[direction];

    // slopes in normal direction change sign
    qdata_mgr->prim_change_sign(ghost_qdata->delta[direction]);

    // swap wm and wp in normal direction:
    // so that qm on the right side is the same as qp on the left side
    qdata_mgr->prim_swap(ghost_qdata->wp[direction],
                         ghost_qdata->wm[direction]);
    ghost_qdata->wp[direction].Vg[direction] =
        -ghost_qdata->wp[direction].Vg[direction];
    ghost_qdata->wm[direction].Vg[direction] =
        -ghost_qdata->wm[direction].Vg[direction];
    ghost_qdata->wp[direction].Vl[direction] =
        -ghost_qdata->wp[direction].Vl[direction];
    ghost_qdata->wm[direction].Vl[direction] =
        -ghost_qdata->wm[direction].Vl[direction];

    // get the physical coordinates of the center of the quad
    double x, y, z;
    double dx;
    double XYZ[3] = {0, 0, 0};
    amr_mgr->quad_centre(which_tree, q, XYZ);
    dx = amr_mgr->quad_dx(q);

    x = XYZ[0];
    y = XYZ[1];
    z = XYZ[2];
    UNUSED(z);

    double d2 = SC_SQR(x - x_c) + SC_SQR(y - y_c);
#ifdef USE_3D
    d2 += SC_SQR(z - z_c);
#endif

    double rhol_in = stg_mgr->read_double("jet.rhol_in", 170.65);
    double rhog_in = stg_mgr->read_double("jet.rhog_in", 170.65);
    double ag_in = stg_mgr->read_double("jet.ag_in", 0.005);
    double Pl_in = stg_mgr->read_double("jet.Pl_in", 1.8E7);
    double Pg_in = stg_mgr->read_double("jet.Pg_in", 1.8E7);
    double Vx_in = stg_mgr->read_double("jet.Vx_in", 0.0);
    double Vy_in = stg_mgr->read_double("jet.Vy_in", 0.0);
    double Vz_in = stg_mgr->read_double("jet.Vz_in", 0.0);

    UNUSED(Vz_in);

    int centre_in_cell = 0;

#ifdef USE_3D
    if (((x_c >= x - 0.5 * dx) & (x_c <= x + 0.5 * dx)) &
        ((y_c >= y - 0.5 * dx) & (y_c <= y + 0.5 * dx)) &
        ((z_c >= z - 0.5 * dx) & (z_c <= z + 0.5 * dx))) {
      centre_in_cell = 1;
    }
#else
    if (((x_c >= x - 0.5 * dx) & (x_c <= x + 0.5 * dx)) &
        ((y_c >= y - 0.5 * dx) & (y_c <= y + 0.5 * dx))) {
      centre_in_cell = 1;
    }
#endif

    if (d2 < radius2 || centre_in_cell) {
      ghost_qdata->w.ag = ag_in;
      ghost_qdata->w.mg = rhog_in * ag_in;
      ghost_qdata->w.ml = rhol_in * (1.0 - ag_in);
      ghost_qdata->w.mgVg[IX] = ghost_qdata->w.mg * Vx_in;
      ghost_qdata->w.mgVg[IY] = ghost_qdata->w.mg * Vy_in;
      ghost_qdata->w.mlVl[IX] = ghost_qdata->w.ml * Vx_in;
      ghost_qdata->w.mlVl[IY] = ghost_qdata->w.ml * Vy_in;
#ifdef USE_3D
      ghost_qdata->w.mgVg[IZ] = ghost_qdata->w.mg * Vz_in;
      ghost_qdata->w.mlVl[IZ] = ghost_qdata->w.ml * Vz_in;

      ghost_qdata->w.mgEg =
          ghost_qdata->w.mg * get_eint(rhog_in, Pg_in, param->eos_g) +
          0.5 *
              (SC_SQR(ghost_qdata->w.mg * Vx_in) +
               SC_SQR(ghost_qdata->w.mg * Vy_in) +
               SC_SQR(ghost_qdata->w.mg * Vz_in)) /
              ghost_qdata->w.mg;

      ghost_qdata->w.mlEl =
          ghost_qdata->w.ml * get_eint(rhol_in, Pl_in, param->eos_l) +
          0.5 *
              (SC_SQR(ghost_qdata->w.ml * Vx_in) +
               SC_SQR(ghost_qdata->w.ml * Vy_in) +
               SC_SQR(ghost_qdata->w.ml * Vz_in)) /
              ghost_qdata->w.ml;
#else
      ghost_qdata->w.mgEg =
          ghost_qdata->w.mg * get_eint(rhog_in, Pg_in, param->eos_g) +
          0.5 *
              (SC_SQR(ghost_qdata->w.mg * Vx_in) +
               SC_SQR(ghost_qdata->w.mg * Vy_in)) /
              ghost_qdata->w.mg;

      ghost_qdata->w.mlEl =
          ghost_qdata->w.ml * get_eint(rhol_in, Pl_in, param->eos_l) +
          0.5 *
              (SC_SQR(ghost_qdata->w.ml * Vx_in) +
               SC_SQR(ghost_qdata->w.ml * Vy_in)) /
              ghost_qdata->w.ml;
#endif

      ghost_qdata->wnext = ghost_qdata->w;

      // reconstructed state (trace)
      prim_t wr;
      wr.mg = rhog_in * ag_in;
      wr.ml = rhol_in * (1.0 - ag_in);
      wr.ag = ag_in;
      wr.Vg[IX] = Vx_in;
      wr.Vg[IY] = Vy_in;
      wr.Vl[IX] = Vx_in;
      wr.Vl[IY] = Vy_in;
#ifdef USE_3D
      wr.Vg[IZ] = Vz_in;
      wr.Vl[IZ] = Vz_in;
#endif
      wr.Pg = Pg_in;
      wr.Pl = Pl_in;

      qdata_mgr->prim_copy(ghost_qdata->wm[IX], wr);
      qdata_mgr->prim_copy(ghost_qdata->wm[IY], wr);
      qdata_mgr->prim_copy(ghost_qdata->wp[IX], wr);
      qdata_mgr->prim_copy(ghost_qdata->wp[IY], wr);
#ifdef USE_3D
      qdata_mgr->prim_copy(ghost_qdata->wm[IZ], wr);
      qdata_mgr->prim_copy(ghost_qdata->wp[IZ], wr);
#endif

      // set slopes to zero
      qdata_mgr->prim_zero(ghost_qdata->delta[IX]);
      qdata_mgr->prim_zero(ghost_qdata->delta[IY]);
#ifdef USE_3D
      qdata_mgr->prim_zero(ghost_qdata->delta[IZ]);
#endif // USE_3D
    }

  } else { // reflective

    bc_bifluid7eq_reflective(p4est, which_tree, q, face, ghost_qdata);
  }
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing boundary conditions for bifluid7eq
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_bc_callback_t<qdata_t>>::Factory() {

  // register the above callback's
  register_factory("dirichlet", bc_bifluid7eq_dirichlet);
  register_factory("neuman", bc_bifluid7eq_neuman);
  register_factory("reflective", bc_bifluid7eq_reflective);
  register_factory("noslip", bc_bifluid7eq_noslip);
  register_factory("poiseuille", bc_bifluid7eq_poiseuille);
  register_factory("jet", bc_bifluid7eq_jet);
}
