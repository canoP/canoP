//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataBifluid5eq.h"

using cons_t = bifluid5eq::cons_t;
using prim_t = bifluid5eq::prim_t;
using param_t = bifluid5eq::param_t;
using qdata_t = bifluid5eq::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// HLLC Non-conservative flux
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void hyperbolic_flux_hllc(prim_t &ql, prim_t &qr, cons_t &ucl, cons_t &ucr,
                          cons_t *fluxl, cons_t *fluxr,
                          QdataManager<cons_t> *qdata_mgr, double *extra) {

  param_t *param = qdata_mgr->get_param();

  // Left variables
  double rl = ql.mg + ql.ml;
  double pl = ql.P;
  double ul = ql.V[IX];
  double al = ql.ag;

  double ecinl = 0.5 * rl * ul * ul;
  ecinl += 0.5 * rl * ql.V[IY] * ql.V[IY];
#ifdef USE_3D
  ecinl += 0.5 * rl * ql.V[IZ] * ql.V[IZ];
#endif

  double etotl =
      rl * get_isobaric_eint(pl, al, ql.mg, ql.ml, param->eos_g, param->eos_l) +
      ecinl;
  double ptotl = pl;

  // Right variables
  double rr = qr.mg + qr.ml;
  double pr = qr.P;
  double ur = qr.V[IX];
  double ar = qr.ag;

  double ecinr = 0.5 * rr * ur * ur;
  ecinr += 0.5 * rr * qr.V[IY] * qr.V[IY];
#ifdef USE_3D
  ecinr += 0.5 * rr * qr.V[IZ] * qr.V[IZ];
#endif

  double etotr =
      rr * get_isobaric_eint(pr, ar, qr.mg, qr.ml, param->eos_g, param->eos_l) +
      ecinr;
  double ptotr = pr;

  // Find the largest eigenvalues in the normal direction to the interface
  double cfastl =
      get_isobaric_soundspeed(pl, al, ql.mg, ql.ml, param->eos_g, param->eos_l);
  double cfastr =
      get_isobaric_soundspeed(pr, ar, qr.mg, qr.ml, param->eos_g, param->eos_l);

  // Compute HLL wave speed
  double SL = fmin(ul, ur) - fmax(cfastl, cfastr);
  double SR = fmax(ul, ur) + fmax(cfastl, cfastr);

  // Compute lagrangian sound speed
  double rcl = rl * (ul - SL);
  double rcr = rr * (SR - ur);

  // Compute acoustic star state
  double ustar = (rcr * ur + rcl * ul + (ptotl - ptotr)) / (rcr + rcl);
  double ptotstar =
      (rcr * ptotl + rcl * ptotr + rcl * rcr * (ul - ur)) / (rcr + rcl);

  // L star region variables
  double mlstarl = ql.ml * (SL - ul) / (SL - ustar);
  double mgstarl = ql.mg * (SL - ul) / (SL - ustar);

  double etotstarl =
      ((SL - ul) * etotl - ptotl * ul + ptotstar * ustar) / (SL - ustar);

  // R star region variables
  double mlstarr = qr.ml * (SR - ur) / (SR - ustar);
  double mgstarr = qr.mg * (SR - ur) / (SR - ustar);

  double etotstarr =
      ((SR - ur) * etotr - ptotr * ur + ptotstar * ustar) / (SR - ustar);

  // Sample the solution at x/t=0
  double mgo, uo, ptoto, etoto, mlo, ago;
  if (SL > 0.0) {
    mgo = ql.mg;
    uo = ul;
    mlo = ql.ml;
    ago = al;
    ptoto = ptotl;
    etoto = etotl;
  } else if (ustar > 0.0) {
    mgo = mgstarl;
    uo = ustar;
    mlo = mlstarl;
    ago = al;
    ptoto = ptotstar;
    etoto = etotstarl;
  } else if (SR > 0.0) {
    mgo = mgstarr;
    uo = ustar;
    mlo = mlstarr;
    ago = ar;
    ptoto = ptotstar;
    etoto = etotstarr;
  } else {
    mgo = qr.mg;
    uo = ur;
    mlo = qr.ml;
    ago = ar;
    ptoto = ptotr;
    etoto = etotr;
  }

  // Compute the Godunov flux (in conservative variables)
  fluxl->mg += mgo * uo;
  fluxr->mg += mgo * uo;

  fluxl->ml += mlo * uo;
  fluxr->ml += mlo * uo;

  // non conservative flux for volume fraction advection
  fluxl->ag += ago * uo - ucl.ag * uo;
  fluxr->ag += ago * uo - ucr.ag * uo;

  // return the total flux
  fluxl->mV[IX] += (mgo + mlo) * uo * uo + ptoto;
  fluxr->mV[IX] += (mgo + mlo) * uo * uo + ptoto;

  fluxl->mE += (etoto + ptoto) * uo;
  fluxr->mE += (etoto + ptoto) * uo;

  if (uo > 0.0) {
    fluxl->mV[IY] += (mgo + mlo) * uo * ql.V[IY];
    fluxr->mV[IY] += (mgo + mlo) * uo * ql.V[IY];
  } else {
    fluxl->mV[IY] += (mgo + mlo) * uo * qr.V[IY];
    fluxr->mV[IY] += (mgo + mlo) * uo * qr.V[IY];
  }

#ifdef USE_3D
  if (uo > 0.0) {
    fluxl->mV[IZ] += (mgo + mlo) * uo * ql.V[IZ];
    fluxr->mV[IZ] += (mgo + mlo) * uo * ql.V[IZ];
  } else {
    fluxl->mV[IZ] += (mgo + mlo) * uo * qr.V[IZ];
    fluxr->mV[IZ] += (mgo + mlo) * uo * qr.V[IZ];
  }
#endif

  *extra = ustar;
}

//////////////////////////////////////////////////////////////////////////////
/// Allregime non-conservative flux
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void hyperbolic_flux_allregime(prim_t &ql, prim_t &qr, cons_t &ucl, cons_t &ucr,
                               cons_t *fluxl, cons_t *fluxr,
                               QdataManager<cons_t> *qdata_mgr, double *extra) {

  param_t *param = qdata_mgr->get_param();

  // Left variables
  double rl = ql.mg + ql.ml;
  double pl = ql.P;
  double ul = ql.V[IX];
  double al = ql.ag;
  double acl = rl * get_isobaric_soundspeed(pl, al, ql.mg, ql.ml, param->eos_g,
                                            param->eos_l);

  // Right variables
  double rr = qr.mg + qr.ml;
  double pr = qr.P;
  double ur = qr.V[IX];
  double ar = qr.ag;
  double acr = rr * get_isobaric_soundspeed(pr, ar, qr.mg, qr.ml, param->eos_g,
                                            param->eos_l);

  // compute the acoustic impedance at interface
  // double acface = sqrt(fmax(acl*acl/rl,acr*acr/rr)*fmin(rl,rr));

  // Compute ustar
  // double ustar = 0.5*(ul+ur)-0.5*(pr-pl)/acface;
  double ustar = (acl * ul + acr * ur - (pr - pl)) / (acl + acr);

  // compute the asymptotic correction

  double theta = 1.;
  if (param->lowmach_correction_enabled) {
    theta = fmin(abs(ustar) / fmax(acl / rl, acr / rr), 1.);
  }

  // compute pistar
  // double pstar = 0.5*(pl+pr)-0.5*acface*(ur-ul)*theta;
  double pstar =
      ((acr * pl + acl * pr) - acl * acr * (ur - ul) * theta) / (acl + acr);

  cons_t Uuw;
  // upwind conservative variables
  if (ustar > 0) {
    Uuw.mg = ql.mg;
    Uuw.ml = ql.ml;
    Uuw.ag = al;
    Uuw.mV[IX] = rl * ql.V[IX];
    Uuw.mV[IY] = rl * ql.V[IY];

    double ekinl = 0.5 * rl * ul * ul;
    ekinl += 0.5 * rl * ql.V[IY] * ql.V[IY];
#ifdef USE_3D
    Uuw.mV[IZ] = rl * ql.V[IZ];
    ekinl += 0.5 * rl * ql.V[IZ] * ql.V[IZ];
#endif

    Uuw.mE = rl * get_isobaric_eint(pl, al, ql.mg, ql.ml, param->eos_g,
                                    param->eos_l) +
             ekinl;
  } else {
    Uuw.mg = qr.mg;
    Uuw.ml = qr.ml;
    Uuw.ag = ar;
    Uuw.mV[IX] = rr * qr.V[IX];
    Uuw.mV[IY] = rr * qr.V[IY];

    double ekinr = 0.5 * rr * ur * ur;
    ekinr += 0.5 * rr * qr.V[IY] * qr.V[IY];
#ifdef USE_3D
    Uuw.mV[IZ] = rr * qr.V[IZ];
    ekinr += 0.5 * rr * qr.V[IZ] * qr.V[IZ];
#endif

    Uuw.mE = rr * get_isobaric_eint(pr, ar, qr.mg, qr.ml, param->eos_g,
                                    param->eos_l) +
             ekinr;
  }

  fluxl->mg += Uuw.mg * ustar;
  fluxr->mg += Uuw.mg * ustar;

  fluxl->ml += Uuw.ml * ustar;
  fluxr->ml += Uuw.ml * ustar;

  // non conservative flux for volume fraction advection
  fluxl->ag += Uuw.ag * ustar - ucl.ag * ustar;
  fluxr->ag += Uuw.ag * ustar - ucr.ag * ustar;

  fluxl->mV[IX] += Uuw.mV[IX] * ustar + pstar;
  fluxr->mV[IX] += Uuw.mV[IX] * ustar + pstar;

  fluxl->mV[IY] += Uuw.mV[IY] * ustar;
  fluxr->mV[IY] += Uuw.mV[IY] * ustar;
#ifdef USE_3D
  fluxl->mV[IZ] += Uuw.mV[IZ] * ustar;
  fluxr->mV[IZ] += Uuw.mV[IZ] * ustar;
#endif
  fluxl->mE += Uuw.mE * ustar + pstar * ustar;
  fluxr->mE += Uuw.mE * ustar + pstar * ustar;

  *extra = ustar;
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing hyperbolic fluxes for bifluid5eq
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_hf_callback_t<cons_t>>::Factory() {

  // register the above callback's
  register_factory("none", nullptr);
  register_factory("hllc_enabled", hyperbolic_flux_hllc);
  register_factory("allregime_enabled", hyperbolic_flux_allregime);
}
