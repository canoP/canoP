//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataBifluid5eq.h"

using cons_t = bifluid5eq::cons_t;
using prim_t = bifluid5eq::prim_t;
using param_t = bifluid5eq::param_t;
using qdata_t = bifluid5eq::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// \brief m gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid5eq_m_gradient(qdata_t *cella, qdata_t *cellb) {

  double mg[2] = {cella->w.mg, cellb->w.mg};

  double mg_grad = scalar_gradient(mg[0], mg[1]);

  double ml[2] = {cella->w.ml, cellb->w.ml};

  double ml_grad = scalar_gradient(ml[0], ml[1]);

  return fmax(mg_grad, ml_grad);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief mg gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid5eq_mg_gradient(qdata_t *cella, qdata_t *cellb) {

  double mg[2] = {cella->w.mg, cellb->w.mg};

  return scalar_gradient(mg[0], mg[1]);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief ml gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid5eq_ml_gradient(qdata_t *cella, qdata_t *cellb) {

  double ml[2] = {cella->w.ml, cellb->w.ml};

  return scalar_gradient(ml[0], ml[1]);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief ag gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid5eq_ag_gradient(qdata_t *cella, qdata_t *cellb) {

  double ag[2] = {cella->w.ag, cellb->w.ag};

  return scalar_gradient(ag[0], ag[1]);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief mV gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid5eq_mV_gradient(qdata_t *cella, qdata_t *cellb) {

  double mVx[2] = {cella->w.mV[IX], cellb->w.mV[IX]};

  double mVx_grad = scalar_gradient(mVx[0], mVx[1]);

  double mVy[2] = {cella->w.mV[IY], cellb->w.mV[IY]};

  double mVy_grad = scalar_gradient(mVy[0], mVy[1]);

  double grad = fmax(mVx_grad, mVy_grad);

#ifdef USE_3D
  double mVz[2] = {cella->w.mV[IZ], cellb->w.mV[IZ]};

  double mVz_grad = scalar_gradient(mVz[0], mVz[1]);

  grad = fmax(grad, mVz_grad);
#endif

  return grad;
}

//////////////////////////////////////////////////////////////////////////////
/// \brief mE gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid5eq_mE_gradient(qdata_t *cella, qdata_t *cellb) {

  double mE[2] = {cella->w.mE, cellb->w.mE};

  return scalar_gradient(mE[0], mE[1]);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief any gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_bifluid5eq_any_gradient(qdata_t *cella, qdata_t *cellb) {

  double mVx[2] = {cella->w.mV[IX], cellb->w.mV[IX]};

  double mVx_grad = scalar_gradient(mVx[0], mVx[1]);

  double mVy[2] = {cella->w.mV[IY], cellb->w.mV[IY]};

  double mVy_grad = scalar_gradient(mVy[0], mVy[1]);

  double grad = fmax(mVx_grad, mVy_grad);

#ifdef USE_3D
  double mVz[2] = {cella->w.mV[IZ], cellb->w.mV[IZ]};

  double mVz_grad = scalar_gradient(mVz[0], mVz[1]);

  grad = fmax(grad, mVz_grad);
#endif

  double mE[2] = {cella->w.mE, cellb->w.mE};

  double mE_grad = scalar_gradient(mE[0], mE[1]);

  grad = fmax(grad, mE_grad);

  double mg[2] = {cella->w.mg, cellb->w.mg};

  double mg_grad = scalar_gradient(mg[0], mg[1]);

  grad = fmax(grad, mg_grad);

  double ml[2] = {cella->w.ml, cellb->w.ml};

  double ml_grad = scalar_gradient(ml[0], ml[1]);

  grad = fmax(grad, ml_grad);

  double ag[2] = {cella->w.ag, cellb->w.ag};

  double ag_grad = scalar_gradient(ag[0], ag[1]);

  grad = fmax(grad, ag_grad);

  return grad;
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing refine conditions for bifluid5eq
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_rc_callback_t<qdata_t>>::Factory() {

  // register the above callback's
  register_factory("ag_gradient", rc_bifluid5eq_ag_gradient);
  register_factory("m_gradient", rc_bifluid5eq_m_gradient);
  register_factory("mg_gradient", rc_bifluid5eq_mg_gradient);
  register_factory("ml_gradient", rc_bifluid5eq_ml_gradient);
  register_factory("mV_gradient", rc_bifluid5eq_mV_gradient);
  register_factory("mE_gradient", rc_bifluid5eq_mE_gradient);
  register_factory("any_gradient", rc_bifluid5eq_any_gradient);
}
