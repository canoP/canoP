//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "QdataBifluid5eq.h"
#include "QdataManager.h"

using cons_t = bifluid5eq::cons_t;

/// @name specialization for bifluid5eq
/// @{
//////////////////////////////////////////////////////////////////////////////
template <> QdataManager<cons_t>::QdataManager(SettingManager *stg_mgr) {

  P4EST_GLOBAL_PRODUCTION("-- read param settings -- \n");

  // Initialize gravity parameters.
  m_param.gravity_x = stg_mgr->readv_double("param.gravity_x", 0.0);
  m_param.gravity_y = stg_mgr->readv_double("param.gravity_y", 0.0);
  m_param.gravity_z = stg_mgr->readv_double("param.gravity_z", 0.0);

  // Initialize eos parameters.
  m_param.eos_g.gamma = stg_mgr->readv_double("param.eos_g.gamma", 1.666);
  m_param.eos_g.Cv = stg_mgr->readv_double("param.eos_g.Cv", 1.0);
  m_param.eos_g.q = stg_mgr->readv_double("param.eos_g.q", 0.0);
  m_param.eos_g.b = stg_mgr->readv_double("param.eos_g.b", 0.0);
  m_param.eos_g.qp = stg_mgr->readv_double("param.eos_g.qp", 0.0);
  m_param.eos_g.pinf = stg_mgr->readv_double("param.eos_g.pinf", 0.0);
  m_param.eos_g.mu = stg_mgr->readv_double("param.eos_g.mu", 0.0);
  m_param.eos_g.eta = stg_mgr->readv_double("param.eosFluid.eta", 0.0);
  m_param.eos_g.kappa = stg_mgr->readv_double("param.eos_g.kappa", 0.0);

  m_param.eos_l.gamma = stg_mgr->readv_double("param.eos_l.gamma", 1.666);
  m_param.eos_l.Cv = stg_mgr->readv_double("param.eos_l.Cv", 1.0);
  m_param.eos_l.q = stg_mgr->readv_double("param.eos_l.q", 0.0);
  m_param.eos_l.b = stg_mgr->readv_double("param.eos_l.b", 0.0);
  m_param.eos_l.qp = stg_mgr->readv_double("param.eos_l.qp", 0.0);
  m_param.eos_l.pinf = stg_mgr->readv_double("param.eos_l.pinf", 0.0);
  m_param.eos_l.mu = stg_mgr->readv_double("param.eos_l.mu", 0.0);
  m_param.eos_l.eta = stg_mgr->readv_double("param.eos_l.eta", 0.0);
  m_param.eos_l.kappa = stg_mgr->readv_double("param.eos_l.kappa", 0.0);

  // for all-regime scheme
  m_param.lowmach_correction_enabled =
      stg_mgr->readv_int("param.lowmach_correction_enabled", 0);
  m_param.K = stg_mgr->readv_double("param.K", 1.1);

  P4EST_GLOBAL_PRODUCTION("\n");
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::cons_to_prim(const cons_t &cdata, prim_t &pdata) {

  // kinetic energy per mass unit
  double ekin = 0.0;

  // total mass per unit volume
  double m = cdata.ml + cdata.mg;

  pdata.mg = cdata.mg;
  pdata.ml = cdata.ml;
  pdata.ag = cdata.ag;
  for (int i = 0; i < P4EST_DIM; ++i) {
    pdata.V[i] = cdata.mV[i] / m;
    ekin += pdata.V[i] * pdata.V[i];
  }
  ekin *= 0.5;
  // internal energy = total energy - kinetic energy (per mass unit)
  double eint = cdata.mE / m - ekin;

  // use equation of state to compute pressure and local speed of sound
  pdata.P = get_isobaric_pressure(eint, pdata.ag, pdata.mg, pdata.ml,
                                  m_param.eos_g, m_param.eos_l);

  pdata.T = get_isothermic_temperature(eint, pdata.ag, pdata.mg, pdata.ml,
                                       m_param.eos_g, m_param.eos_l);
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::prim_to_cons(const prim_t &pdata, cons_t &cdata) {
  double ekin = 0.0;

  cdata.mg = pdata.mg;
  cdata.ml = pdata.ml;
  cdata.ag = pdata.ag;

  for (int i = 0; i < P4EST_DIM; ++i) {
    cdata.mV[i] = pdata.V[i] * (pdata.mg + pdata.ml);
    ekin += 0.5 * pdata.V[i] * pdata.V[i];
  }

  double eint = get_isobaric_eint(pdata.P, pdata.ag, pdata.mg, pdata.ml,
                                  m_param.eos_g, m_param.eos_l);

  cdata.mE = (pdata.mg + pdata.ml) * (eint + ekin);
}

//////////////////////////////////////////////////////////////////////////////
template <>
std::vector<double>
QdataManager<cons_t>::quad_get_field(const std::string &varName,
                                     p4est_quadrant_t *q) {

  qdata_t *qdata;
  cons_t w;

  if (q != nullptr) {

    qdata = quad_get_qdata(q);
    w = qdata->w;
  }

  std::vector<double> field;

  if (!varName.compare("mg")) {
    field.push_back(w.mg);
  } else if (!varName.compare("mE")) {
    field.push_back(w.mE);
  } else if (!varName.compare("ml")) {
    field.push_back(w.ml);
  } else if (!varName.compare("m")) {
    field.push_back(w.ml + w.mg);
  } else if (!varName.compare("ag")) {
    field.push_back(w.ag);
  } else if (!varName.compare("yg")) {
    field.push_back(w.mg / (w.mg + w.ml));
  } else if (!varName.compare("P")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    field.push_back(pdata.P);
  } else if (!varName.compare("T")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    field.push_back(pdata.T);
  } else if (!varName.compare("mV")) {
    for (int i = 0; i < P4EST_DIM; i++)
      field.push_back(w.mV[i]);
  } else if (!varName.compare("V")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    for (int i = 0; i < P4EST_DIM; i++)
      field.push_back(pdata.V[i]);
  } else {
    // print warning
    P4EST_GLOBAL_INFOF("Unrecognized field: \"%s\"\n", varName.c_str());
  }

  return field;
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::get_hydro_flux_predictor(int idir, const cons_t &cc,
                                                    const prim_t &pf,
                                                    cons_t &flux) {
  cons_t cf;

  prim_to_cons(pf, cf);

  // advection term
  flux.mg = cf.mg * pf.V[idir];

  flux.ml = cf.ml * pf.V[idir];

  for (int i = 0; i < P4EST_DIM; ++i) {
    flux.mV[i] = cf.mV[i] * pf.V[idir];
  }
  flux.mE = cf.mE * pf.V[idir];

  // pressure term
  flux.mV[idir] += pf.P;
  flux.mE += pf.P * pf.V[idir];

  // non conservative flux using central velocity
  flux.ag = cc.mV[idir] / (cc.mg + cc.ml) * cf.ag;
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::get_grav_source_predictor(const prim_t &pdata,
                                                     cons_t &source) {

  source.mV[IX] = (pdata.mg + pdata.ml) * m_param.gravity_x;
  source.mV[IY] = (pdata.mg + pdata.ml) * m_param.gravity_y;

  source.mE = (pdata.mg + pdata.ml) * pdata.V[IX] * m_param.gravity_x;
  source.mE += (pdata.mg + pdata.ml) * pdata.V[IY] * m_param.gravity_y;

#if USE_3D
  source.mV[IZ] = (pdata.mg + pdata.ml) * m_param.gravity_z;

  source.mE += (pdata.mg + pdata.ml) * pdata.V[IZ] * m_param.gravity_z;
#endif
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::swap_dir(int idir, prim_t &pdata, cons_t &cdata,
                                    array<prim_t> &delta) {

  std::swap(pdata.V[IX], pdata.V[idir]);
  std::swap(cdata.mV[IX], cdata.mV[idir]);

  for (int d = 0; d < P4EST_DIM; ++d) {
    std::swap(delta[d].V[IX], delta[d].V[idir]);
  }

  prim_swap(delta[IX], delta[idir]);
}

//////////////////////////////////////////////////////////////////////////////
template <> void QdataManager<cons_t>::swap_dir(int idir, cons_t &cdata) {

  std::swap(cdata.mV[IX], cdata.mV[idir]);
}

//////////////////////////////////////////////////////////////////////////////
template <>
double QdataManager<cons_t>::get_hydro_invdt(const prim_t &pdata,
                                             const double dx) {
  double V = 0.0;
  double c = 0.0;

  c = get_isobaric_soundspeed(pdata.P, pdata.ag, pdata.mg, pdata.ml,
                              m_param.eos_g, m_param.eos_l);

  for (int d = 0; d < P4EST_DIM; ++d) {
    V += c + fabs(pdata.V[d]);
  }

  return V / dx;
}

//////////////////////////////////////////////////////////////////////////////
template <>
double QdataManager<cons_t>::get_cond_invdt(const prim_t &pdata,
                                            const double dx) {

  double kappa = (pdata.ag * m_param.eos_g.kappa) +
                 ((1.0 - pdata.ag) * m_param.eos_l.kappa);

  double yg = pdata.mg / (pdata.mg + pdata.ml);

  double Cv = (yg * m_param.eos_g.Cv) + ((1.0 - yg) * m_param.eos_l.Cv);

  return kappa / ((pdata.mg + pdata.ml) * Cv * dx * dx);
}

//////////////////////////////////////////////////////////////////////////////
template <>
double QdataManager<cons_t>::get_visc_invdt(const prim_t &pdata,
                                            const double dx) {

  double mu =
      (pdata.ag * m_param.eos_g.mu) + ((1.0 - pdata.ag) * m_param.eos_l.mu);

  return mu / ((pdata.mg + pdata.ml) * dx * dx);
}
/// @}
