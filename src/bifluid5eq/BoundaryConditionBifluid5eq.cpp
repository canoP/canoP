//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataBifluid5eq.h"
#include "Simulation.h"

using cons_t = bifluid5eq::cons_t;
using prim_t = bifluid5eq::prim_t;
using param_t = bifluid5eq::param_t;
using qdata_t = bifluid5eq::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// \brief dirichlet boundary condition
///
/// Imposed values
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid5eq_dirichlet(p4est_t *p4est, p4est_topidx_t which_tree,
                             p4est_quadrant_t *q, int face,
                             qdata_t *ghost_qdata) {
  UNUSED(q);
  UNUSED(which_tree);

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  SettingManager *stg_mgr = simu->m_stg_mgr;

  double mg = 0., P = 0., Vx = 0., Vy = 0., Vz = 0., ag = 0., ml = 0.;

  if (face == 0) {
    ag = stg_mgr->read_double("dirichlet.face0.ag", 0.0);
    ml = stg_mgr->read_double("dirichlet.face0.ml", 0.0);
    mg = stg_mgr->read_double("dirichlet.face0.mg", 1.0);
    P = stg_mgr->read_double("dirichlet.face0.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face0.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face0.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face0.Vz", 0.0);
  } else if (face == 1) {
    ag = stg_mgr->read_double("dirichlet.face1.ag", 0.0);
    ml = stg_mgr->read_double("dirichlet.face1.ml", 0.0);
    mg = stg_mgr->read_double("dirichlet.face1.mg", 1.0);
    P = stg_mgr->read_double("dirichlet.face1.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face1.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face1.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face1.Vz", 0.0);
  } else if (face == 2) {
    ag = stg_mgr->read_double("dirichlet.face2.ag", 0.0);
    ml = stg_mgr->read_double("dirichlet.face2.ml", 0.0);
    mg = stg_mgr->read_double("dirichlet.face2.mg", 1.0);
    P = stg_mgr->read_double("dirichlet.face2.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face2.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face2.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face2.Vz", 0.0);
  } else if (face == 3) {
    ag = stg_mgr->read_double("dirichlet.face3.ag", 0.0);
    ml = stg_mgr->read_double("dirichlet.face3.ml", 0.0);
    mg = stg_mgr->read_double("dirichlet.face3.mg", 1.0);
    P = stg_mgr->read_double("dirichlet.face3.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face3.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face3.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face3.Vz", 0.0);
  } else if (face == 4) {
    ag = stg_mgr->read_double("dirichlet.face4.ag", 0.0);
    ml = stg_mgr->read_double("dirichlet.face4.ml", 0.0);
    mg = stg_mgr->read_double("dirichlet.face4.mg", 1.0);
    P = stg_mgr->read_double("dirichlet.face4.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face4.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face4.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face4.Vz", 0.0);
  } else if (face == 5) {
    ag = stg_mgr->read_double("dirichlet.face5.ag", 0.0);
    ml = stg_mgr->read_double("dirichlet.face5.ml", 0.0);
    mg = stg_mgr->read_double("dirichlet.face5.mg", 1.0);
    P = stg_mgr->read_double("dirichlet.face5.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face5.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face5.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face5.Vz", 0.0);
  }
  UNUSED(Vz);

  prim_t w_prim;

  w_prim.ag = ag;
  w_prim.ml = ml;
  w_prim.mg = mg;
  w_prim.P = P;
  w_prim.V[IX] = Vx;
  w_prim.V[IY] = Vy;
#ifdef USE_3D
  w_prim.V[IX] = Vz;
#endif

  ghost_qdata->wm[IX] = w_prim;
  ghost_qdata->wp[IX] = w_prim;
  ghost_qdata->wm[IY] = w_prim;
  ghost_qdata->wp[IY] = w_prim;
#ifdef USE_3D
  ghost_qdata->wm[IZ] = w_prim;
  ghost_qdata->wp[IZ] = w_prim;
#endif

  /* set slopes to zero */
  qdata_mgr->prim_zero(ghost_qdata->delta[IX]);
  qdata_mgr->prim_zero(ghost_qdata->delta[IY]);
#ifdef USE_3D
  qdata_mgr->prim_zero(ghost_qdata->delta[IZ]);
#endif // USE_3D
}

//////////////////////////////////////////////////////////////////////////////
/// \brief neuman boundary condition
///
/// Imposed null gradient
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid5eq_neuman(p4est_t *p4est, p4est_topidx_t which_tree,
                          p4est_quadrant_t *q, int face, qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);
  UNUSED(face);

  // retrieve our Simu, QdataManager and finaly our quadrant data
  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /*
   * copy the entire qdata is necessary for bifluid5eq scheme
   * so that we have the slope and MUSCL-Hancock extrapoled states
   * and can perform the trace operation
   */
  qdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* wp / wm should also be initialized */
  {
    prim_t w_prim;

    /* get the primitive variables in the current quadrant */
    qdata_mgr->cons_to_prim(ghost_qdata->w, w_prim);

    ghost_qdata->wm[IX] = w_prim;
    ghost_qdata->wp[IX] = w_prim;
    ghost_qdata->wm[IY] = w_prim;
    ghost_qdata->wp[IY] = w_prim;
#ifdef USE_3D
    ghost_qdata->wm[IZ] = w_prim;
    ghost_qdata->wp[IZ] = w_prim;
#endif
  }

  /* set slopes to zero */
  qdata_mgr->prim_zero(ghost_qdata->delta[IX]);
  qdata_mgr->prim_zero(ghost_qdata->delta[IY]);
#ifdef USE_3D
  qdata_mgr->prim_zero(ghost_qdata->delta[IZ]);
#endif // USE_3D
}

//////////////////////////////////////////////////////////////////////////////
/// \brief reflective boundary condition
///
/// Copy values and switch the sign of normal velocity
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid5eq_reflective(p4est_t *p4est, p4est_topidx_t which_tree,
                              p4est_quadrant_t *q, int face,
                              qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);

  /* get face normal direction */
  int direction = face / 2;

  /* get current cell data */
  // retrieve our Simu, QdataManager and finaly our quadrant data
  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /* copy current into the ghost outside-boundary cell */
  qdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* reverse the normal V */
  ghost_qdata->w.mV[direction] = -ghost_qdata->w.mV[direction];
  ghost_qdata->wnext.mV[direction] = -ghost_qdata->wnext.mV[direction];

  // slopes in normal direction change sign
  qdata_mgr->prim_change_sign(ghost_qdata->delta[direction]);

  // swap wm and wp in normal direction:
  // so that qm on the right side is the same as qp on the left side
  qdata_mgr->prim_swap(ghost_qdata->wp[direction], ghost_qdata->wm[direction]);
  ghost_qdata->wp[direction].V[direction] =
      -ghost_qdata->wp[direction].V[direction];
  ghost_qdata->wm[direction].V[direction] =
      -ghost_qdata->wm[direction].V[direction];
}

//////////////////////////////////////////////////////////////////////////////
/// \brief no-slip boundary condition
///
/// change sign of velocity in transverse direction
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid5eq_noslip(p4est_t *p4est, p4est_topidx_t which_tree,
                          p4est_quadrant_t *q, int face, qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);

  /* get face normal direction */
  int direction = face / 2;

  /* get current cell data */
  // retrieve our Simu, QdataManager and finaly our quadrant data
  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /* copy current into the ghost outside-boundary cell */
  qdata_mgr->qdata_copy(ghost_qdata, qdata);

  double dx = amr_mgr->quad_dx(q);

  /* reverse V */
  for (int i = 0; i < P4EST_DIM; i++) {
    ghost_qdata->w.mV[i] = -ghost_qdata->w.mV[i];
    ghost_qdata->wnext.mV[i] = -ghost_qdata->wnext.mV[i];

    for (int j = 0; j < P4EST_DIM; j++) {
      ghost_qdata->wp[j].V[i] = -ghost_qdata->wp[j].V[i];
      ghost_qdata->wm[j].V[i] = -ghost_qdata->wm[j].V[i];
    }
  }

  // slope change sign
  qdata_mgr->prim_change_sign(ghost_qdata->delta[direction]);

  // swap wm and wp:
  // so that qm on the right side is the same as qp on the left side
  qdata_mgr->prim_swap(ghost_qdata->wp[direction], ghost_qdata->wm[direction]);

  // change gradients in transverse direction
  for (int i = 0; i < P4EST_DIM; i++) {
    for (int j = 0; j < P4EST_DIM; j++) {

      if (!(i == direction) & !(j == direction)) {
        ghost_qdata->delta[j].V[i] = -ghost_qdata->delta[j].V[i];
      }
    }

    if (!(i == direction)) {

      double m = ghost_qdata->w.ml + ghost_qdata->w.mg;

      if (face % 2 == 1) {
        ghost_qdata->delta[direction].V[i] =
            2 * ghost_qdata->w.mV[i] / (m * dx);
      } else {
        ghost_qdata->delta[direction].V[i] =
            -2 * ghost_qdata->w.mV[i] / (m * dx);
      }
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
/// \brief poiseuille boundary condition
///
/// Imposed pressure gradient in x direction, no-slip otherwise
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid5eq_poiseuille(p4est_t *p4est, p4est_topidx_t which_tree,
                              p4est_quadrant_t *q, int face,
                              qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);
  UNUSED(face);

  // retrieve our Simu, QdataManager and finaly our quadrant data
  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  /* setting parameter handle */
  param_t *param = qdata_mgr->get_param();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  double dx = amr_mgr->quad_dx(q);

  // get the coordinates of the center of the quad
  double XYZ[3] = {0, 0, 0};
  double x, y, z;
  UNUSED(y);
  UNUSED(z);

  // get the physical coordinates of the center of the quad
  amr_mgr->quad_centre(which_tree, q, XYZ);

  x = XYZ[IX];
  y = XYZ[IY];
  z = XYZ[IZ];

  double gradP = simu->m_stg_mgr->read_double("poiseuille.gradP", 0.0);
  double P0 = simu->m_stg_mgr->read_double("poiseuille.P0", 1.0);

  if (face == 2 || face == 3 || face == 4 || face == 5) {

    bc_bifluid5eq_noslip(p4est, which_tree, q, face, ghost_qdata);

  } else if (face == 0 || face == 1) {

    if (face == 0) {
      x -= dx;
    } else {
      x += dx;
    }

    /*
     * copy the entire qdata is necessary for bifluid5eq scheme
     * so that we have the slope and MUSCL-Hancock extrapoled states
     * and can perform the trace operation
     */
    qdata_mgr->qdata_copy(ghost_qdata, qdata);

    prim_t w_prim;

    /* get the primitive variables in the current quadrant */
    qdata_mgr->cons_to_prim(ghost_qdata->w, w_prim);

    w_prim.P = P0 + gradP * x;

    double ekin = 0.5 *
                  (w_prim.V[IX] * w_prim.V[IX] + w_prim.V[IY] * w_prim.V[IY]) *
                  (w_prim.ml + w_prim.mg);
#ifdef USE_3D
    ekin += w_prim.V[IZ] * w_prim.V[IZ] * (w_prim.ml + w_prim.mg);
#endif // USE_3D

    ghost_qdata->w.mE =
        (w_prim.ml + w_prim.mg) *
            get_isobaric_eint(w_prim.P, w_prim.ag, w_prim.mg, w_prim.ml,
                              param->eos_g, param->eos_l) +
        ekin;
    ghost_qdata->wp[IX] = w_prim;
    ghost_qdata->wm[IX] = w_prim;
    ghost_qdata->delta[IX].P = gradP;
  }
}

//////////////////////////////////////////////////////////////////////////////
/// \brief jet boundary condition
///
/// Imposed values in a opening, reflective otherwise
//////////////////////////////////////////////////////////////////////////////
void bc_bifluid5eq_jet(p4est_t *p4est, p4est_topidx_t which_tree,
                       p4est_quadrant_t *q, int face, qdata_t *ghost_qdata) {

  UNUSED(which_tree);
  UNUSED(q);
  UNUSED(face);

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  param_t *param = qdata_mgr->get_param();

  SettingManager *stg_mgr = simu->m_stg_mgr;

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /* one can specify the inflow face in the parameter file */
  int jetface = stg_mgr->read_int("jet.face", 0);
  double x_c = stg_mgr->read_double("jet.x_c", 0.0);
  double y_c = stg_mgr->read_double("jet.y_c", 0.5);
  double z_c = stg_mgr->read_double("jet.z_c", 0.5);
  double radius = stg_mgr->read_double("jet.radius", 0.02);
  double radius2 = radius * radius;

  UNUSED(z_c);

  if (face == jetface) { // inflow

    /* get face normal direction */
    int direction = jetface / 2;

    // reflective
    /* copy current cell data into the ghost outside-boundary cell */
    qdata_mgr->qdata_copy(ghost_qdata, qdata);

    /* reverse the normal V */
    ghost_qdata->w.mV[direction] = -ghost_qdata->w.mV[direction];
    ghost_qdata->wnext.mV[direction] = -ghost_qdata->wnext.mV[direction];

    // slopes in normal direction change sign
    qdata_mgr->prim_change_sign(ghost_qdata->delta[direction]);

    // swap wm and wp in normal direction:
    // so that qm on the right side is the same as qp on the left side
    qdata_mgr->prim_swap(ghost_qdata->wp[direction],
                         ghost_qdata->wm[direction]);
    ghost_qdata->wp[direction].V[direction] =
        -ghost_qdata->wp[direction].V[direction];
    ghost_qdata->wm[direction].V[direction] =
        -ghost_qdata->wm[direction].V[direction];

    // get the physical coordinates of the center of the quad
    double x, y, z;
    double dx;
    double XYZ[3] = {0, 0, 0};

    amr_mgr->quad_centre(which_tree, q, XYZ);
    dx = amr_mgr->quad_dx(q);

    x = XYZ[0];
    y = XYZ[1];
    z = XYZ[2];
    UNUSED(z);

    double d2 = SC_SQR(x - x_c) + SC_SQR(y - y_c);
#ifdef USE_3D
    d2 += SC_SQR(z - z_c);
#endif

    double ml_in = stg_mgr->read_double("jet.ml_in", 170.65);
    double mg_in = stg_mgr->read_double("jet.mg_in", 0.0);
    double ag_in = stg_mgr->read_double("jet.ag_in", 0.0);
    double P_in = stg_mgr->read_double("jet.P_in", 1.8E7);

    int centre_in_cell = 0;

#ifdef USE_3D
    if (((x_c >= x - 0.5 * dx) & (x_c <= x + 0.5 * dx)) &
        ((y_c >= y - 0.5 * dx) & (y_c <= y + 0.5 * dx)) &
        ((z_c >= z - 0.5 * dx) & (z_c <= z + 0.5 * dx))) {
      centre_in_cell = 1;
    }
#else
    if (((x_c >= x - 0.5 * dx) & (x_c <= x + 0.5 * dx)) &
        ((y_c >= y - 0.5 * dx) & (y_c <= y + 0.5 * dx))) {
      centre_in_cell = 1;
    }
#endif

    if (d2 < radius2 || centre_in_cell) {

      // reconstructed state (trace)
      prim_t wr;
      wr.ml = ml_in;
      wr.mg = mg_in;
      wr.ag = ag_in;
      wr.P = P_in;

      qdata_mgr->prim_to_cons(wr, ghost_qdata->w);
      qdata_mgr->prim_to_cons(wr, ghost_qdata->wnext);

      qdata_mgr->prim_copy(ghost_qdata->wm[IX], wr);
      qdata_mgr->prim_copy(ghost_qdata->wm[IY], wr);
      qdata_mgr->prim_copy(ghost_qdata->wp[IX], wr);
      qdata_mgr->prim_copy(ghost_qdata->wp[IY], wr);
#ifdef USE_3D
      qdata_mgr->prim_copy(ghost_qdata->wm[IZ], wr);
      qdata_mgr->prim_copy(ghost_qdata->wp[IZ], wr);
#endif

      // set slopes to zero
      qdata_mgr->prim_zero(ghost_qdata->delta[IX]);
      qdata_mgr->prim_zero(ghost_qdata->delta[IY]);
#ifdef USE_3D
      qdata_mgr->prim_zero(ghost_qdata->delta[IZ]);
#endif // USE_3D
    }

  } else { // reflective

    bc_bifluid5eq_reflective(p4est, which_tree, q, face, ghost_qdata);
  }
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing boundary conditions for bifluid5eq
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_bc_callback_t<qdata_t>>::Factory() {

  // register the above callback's
  register_factory("dirichlet", bc_bifluid5eq_dirichlet);
  register_factory("neuman", bc_bifluid5eq_neuman);
  register_factory("reflective", bc_bifluid5eq_reflective);
  register_factory("noslip", bc_bifluid5eq_noslip);
  register_factory("poiseuille", bc_bifluid5eq_poiseuille);
  register_factory("jet", bc_bifluid5eq_jet);
}
