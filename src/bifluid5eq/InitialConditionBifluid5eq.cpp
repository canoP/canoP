//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataBifluid5eq.h"
#include "Simulation.h"

using cons_t = bifluid5eq::cons_t;
using prim_t = bifluid5eq::prim_t;
using param_t = bifluid5eq::param_t;
using qdata_t = bifluid5eq::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// \brief riemann_problem
///
/// The initial condition is a two-state riemann problem
/// See Einfeldt et al. 1999 JCP92(2):pp 273-295 for rarefaction
//////////////////////////////////////////////////////////////////////////////
void ic_bifluid5eq_riemann_problem(p4est_t *p4est, p4est_topidx_t which_tree,
                                   p4est_quadrant_t *quad) {

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  /* get setting reader */
  SettingManager *stg_mgr = simu->m_stg_mgr;

  param_t *param = qdata_mgr->get_param();

  /* now start genuine initialization */
  cons_t w;
  memset(&w, 0, sizeof(cons_t));

  prim_t qleft, qright;
  memset(&qleft, 0, sizeof(prim_t));
  memset(&qright, 0, sizeof(prim_t));

  // get the coordinates of the center of the quad
  double XYZ[3] = {0, 0, 0};
  double x;

  // get the physical coordinates of the center of the quad
  amr_mgr->quad_centre(which_tree, quad, XYZ);

  x = XYZ[IX];

  // position of the interface
  double x0 = stg_mgr->read_double("riemann_problem.x0", 0.5);

  // fluid left
  qleft.mg = stg_mgr->read_double("riemann_problem.mg_left", 1.0);
  qleft.ml = stg_mgr->read_double("riemann_problem.ml_left", 1.0);
  qleft.ag = stg_mgr->read_double("riemann_problem.ag_left", 0.5);
  qleft.P = stg_mgr->read_double("riemann_problem.P_left", 1.0);
  qleft.V[IX] = stg_mgr->read_double("riemann_problem.Vx_left", 0.0);
  qleft.V[IY] = stg_mgr->read_double("riemann_problem.Vy_left", 0.0);
#ifdef USE_3D
  qleft.V[IZ] = stg_mgr->read_double("riemann_problem.Vz_left", 0.0);
#endif

  // fluid right
  qright.mg = stg_mgr->read_double("riemann_problem.mg_right", 1.0);
  qright.ml = stg_mgr->read_double("riemann_problem.ml_right", 1.0);
  qright.ag = stg_mgr->read_double("riemann_problem.ag_right", 0.5);
  qright.P = stg_mgr->read_double("riemann_problem.P_right", 1.0);
  qright.V[IX] = stg_mgr->read_double("riemann_problem.Vx_right", 0.0);
  qright.V[IY] = stg_mgr->read_double("riemann_problem.Vy_right", 0.0);
#ifdef USE_3D
  qright.V[IZ] = stg_mgr->read_double("riemann_problem.Vz_right", 0.0);
#endif

  if (x < x0) {

    qdata_mgr->prim_to_cons(qleft, w);

  } else {

    qdata_mgr->prim_to_cons(qright, w);
  }

  // finally set quadrant's qdata
  qdata_mgr->quad_set_cons(quad, &w);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief blast
///
/// The initial solution for rho is a disk which center is (xc,yc,zc) and
/// radius R. Initial V is zero.
/// See http://www.astro.princeton.edu/~jstone/Athena/tests/blast/blast.html
//////////////////////////////////////////////////////////////////////////////
void ic_bifluid5eq_blast(p4est_t *p4est, p4est_topidx_t which_tree,
                         p4est_quadrant_t *quad) {

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  /* get setting reader */
  SettingManager *stg_mgr = simu->m_stg_mgr;

  param_t *param = qdata_mgr->get_param();

  /* now start genuine initialization */
  cons_t w;
  prim_t qin, qout;

  memset(&w, 0, sizeof(cons_t));
  memset(&qin, 0, sizeof(prim_t));
  memset(&qout, 0, sizeof(prim_t));

  // get the coordinates of the center of the quad
  double XYZ[3] = {0, 0, 0};
  double x, y, z;
  UNUSED(z);

  // get the physical coordinates of the center of the quad
  amr_mgr->quad_centre(which_tree, quad, XYZ);

  x = XYZ[IX];
  y = XYZ[IY];
  z = XYZ[IZ];

  // disk center, radius
  double x_c;
  double y_c;
  double z_c;
  double radius;

  x_c = stg_mgr->read_double("blast.x_c", 0.5);
  y_c = stg_mgr->read_double("blast.y_c", 0.5);
#ifdef USE_3D
  z_c = stg_mgr->read_double("blast.z_c", 0.5);
#endif
  UNUSED(z_c);

  radius = stg_mgr->read_double("blast.radius", 0.25);
  double radius2 = radius * radius;

  // fluid in
  qin.ml = 0.5 * stg_mgr->read_double("blast.m_in", 1.0);
  qin.mg = 0.5 * stg_mgr->read_double("blast.m_in", 1.0);
  qin.ag = 0.5;
  qin.P = stg_mgr->read_double("blast.P_in", 10.0);

  // fluid out
  qout.ml = 0.5 * stg_mgr->read_double("blast.m_out", 1.2);
  qout.mg = 0.5 * stg_mgr->read_double("blast.m_out", 1.2);
  qout.ag = 0.5;
  qout.P = stg_mgr->read_double("blast.P_out", 1.0);

  double d2 = SC_SQR(x - x_c) + SC_SQR(y - y_c);
#ifdef USE_3D
  d2 += SC_SQR(z - z_c);
#endif

  if (d2 < radius2) {

    qdata_mgr->prim_to_cons(qin, w);

  } else {

    qdata_mgr->prim_to_cons(qout, w);
  }

  // finally set quadrant's qdata
  qdata_mgr->quad_set_cons(quad, &w);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief gresho
///
/// The initial condition is a stationary vortex
///
/// Gresho & Chan 1990 Int. J. Numer. Meth. Fluids, 11(5):pp. 621–659
//////////////////////////////////////////////////////////////////////////////
void ic_bifluid5eq_gresho(p4est_t *p4est, p4est_topidx_t which_tree,
                          p4est_quadrant_t *quad) {

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  /* get setting reader */
  SettingManager *stg_mgr = simu->m_stg_mgr;

  param_t *param = qdata_mgr->get_param();

  /* now start genuine initialization */
  cons_t w;
  memset(&w, 0, sizeof(cons_t));

  prim_t q;
  memset(&q, 0, sizeof(prim_t));

  // get the coordinates of the center of the quad
  double XYZ[3] = {0, 0, 0};
  double x, y;

  // get the physical coordinates of the center of the quad
  amr_mgr->quad_centre(which_tree, quad, XYZ);

  x = XYZ[IX];
  y = XYZ[IY];

  // disk center, radius
  double x_c;
  double y_c;
  double m, mach;

  x_c = stg_mgr->read_double("gresho.x_c", 0.5);
  y_c = stg_mgr->read_double("gresho.y_c", 0.5);
  m = stg_mgr->read_double("gresho.m", 1.0);
  mach = stg_mgr->read_double("gresho.mach", 0.01);

  double d2 = SC_SQR(x - x_c) + SC_SQR(y - y_c);
  double r = sqrt(d2);
  double theta = atan2(y - y_c, x - x_c);
  double P0 = m / (mach * mach);

  double pressure, utheta;
  if (r < 0.2) {

    utheta = 5. * r;
    pressure = P0 + 5. * 5. * r * r / 2.;

  } else if (r < 0.4) {

    utheta = 2. - 5. * r;
    pressure = P0 + 5. * 5. * r * r / 2. + 4. * (1. - 5. * r + log(5. * r));

  } else {

    utheta = 0.;
    pressure = P0 - 2. + log(4. * 4.);
  }

  q.ml = 0.5 * m;
  q.mg = 0.5 * m;
  q.ag = 0.5;
  q.P = pressure;
  q.V[IX] = -utheta * sin(theta);
  q.V[IY] = +utheta * cos(theta);
#ifdef USE_3D
  q.V[IZ] = 0.;
#endif

  qdata_mgr->prim_to_cons(q, w);

  // finally set quadrant's qdata
  qdata_mgr->quad_set_cons(quad, &w);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Rayleigh_Taylor instability.
///
/// See http://www.astro.princeton.edu/~jstone/Athena/tests/rt/rt.html
/// for a description of such initial conditions
//////////////////////////////////////////////////////////////////////////////
void ic_bifluid5eq_rayleigh_taylor(p4est_t *p4est, p4est_topidx_t which_tree,
                                   p4est_quadrant_t *quad) {

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  /* get setting reader */
  SettingManager *stg_mgr = simu->m_stg_mgr;

  param_t *param = qdata_mgr->get_param();

  /* now start genuine initialization */
  cons_t w;
  memset(&w, 0, sizeof(cons_t));

  prim_t q;
  memset(&q, 0, sizeof(prim_t));

  // get the coordinates of the center of the quad
  double XYZ[3] = {0, 0, 0};
  double x, y, z;
  UNUSED(z);

  // get the physical coordinates of the center of the quad
  amr_mgr->quad_centre(which_tree, quad, XYZ);

  x = XYZ[IX];
  y = XYZ[IY];
  z = XYZ[IZ];

  // density
  double m_down = stg_mgr->read_double("rayleigh_taylor.m_down", 1.0);
  double m_up = stg_mgr->read_double("rayleigh_taylor.m_up", 2.0);

  // static gravity field
  double grav_x = stg_mgr->read_double("param.gravity_x", -0.1);

  // interface location and pressure
  double x0 = stg_mgr->read_double("rayleigh_taylor.x0", 1.5);
  double P0 = stg_mgr->read_double("rayleigh_taylor.P0", 1.0);

  // type of perturbation
  int randomEnabled = stg_mgr->read_int("rayleigh_taylor.randomEnabled", 0.0);
  double amplitude = stg_mgr->read_double("rayleigh_taylor.amplitude", 0.01);
  double mode = stg_mgr->read_int("rayleigh_taylor.perturb_mode", 1.0);
  double width = stg_mgr->read_double("rayleigh_taylor.perturb_width", 0.05);

  // random number generator (only used if randomEnabled is different of 0)
  int seed = stg_mgr->read_int("rayleigh_taylor.rand_seed", 12);
  srand(seed * (p4est->mpirank + 1));

  double Lx = amr_mgr->m_ntree_x * amr_mgr->m_ltree;
  double Ly = amr_mgr->m_ntree_y * amr_mgr->m_ltree;
  double Lz = amr_mgr->m_ntree_z * amr_mgr->m_ltree;
  UNUSED(Lx);
  UNUSED(Ly);
  UNUSED(Lz);

  if (x < x0) { // light fluid
    q.ml = 0.5 * m_down;
    q.mg = 0.5 * m_down;
  } else { // heavy fluid
    q.ml = 0.5 * m_up;
    q.mg = 0.5 * m_up;
  }

  if (randomEnabled) {
    q.V[IX] = amplitude * (1.0 * rand() / RAND_MAX - 0.5);
  } else {
    q.V[IX] = amplitude * cos(mode * 2 * M_PI * y / Ly) *
              cos(mode * 2 * M_PI * z / Lz) *
              exp(-(x - x0) * (x - x0) / 2 / width / width);
  }
  q.V[IY] = 0.0;
#ifdef USE_3D
  q.V[IZ] = 0.0;
#endif

  q.ag = 0.5;
  q.P = P0 + (q.mg + q.ml) * grav_x * (x - x0);

  qdata_mgr->prim_to_cons(q, w);

  // finally set quadrant's qdata
  qdata_mgr->quad_set_cons(quad, &w);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief poiseuille
///
/// The initial condition is a poiseuille flow generated by boundary
/// conditions
//////////////////////////////////////////////////////////////////////////////
void ic_bifluid5eq_poiseuille(p4est_t *p4est, p4est_topidx_t which_tree,
                              p4est_quadrant_t *quad) {

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  /* get setting reader */
  SettingManager *stg_mgr = simu->m_stg_mgr;

  param_t *param = qdata_mgr->get_param();

  /* now start genuine initialization */
  cons_t w;
  memset(&w, 0, sizeof(cons_t));

  prim_t q;
  memset(&q, 0, sizeof(prim_t));

  // get the coordinates of the center of the quad
  double XYZ[3] = {0, 0, 0};
  double x, y, z;
  UNUSED(y);
  UNUSED(z);

  // get the physical coordinates of the center of the quad
  amr_mgr->quad_centre(which_tree, quad, XYZ);

  x = XYZ[IX];
  y = XYZ[IY];
  z = XYZ[IZ];

  // fluid
  double m0 = stg_mgr->read_double("poiseuille.m0", 1.0);
  double P0 = stg_mgr->read_double("poiseuille.P0", 0.0240096);
  double gradP = stg_mgr->read_double("poiseuille.gradP", -8E-6);

  double P = P0 + gradP * x;

  q.ml = 0.5 * m0;
  q.mg = 0.5 * m0;
  q.ag = 0.5;
  q.P = P0 + gradP * x;

  qdata_mgr->prim_to_cons(q, w);

  // finally set quadrant's qdata
  qdata_mgr->quad_set_cons(quad, &w);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief conduction
///
/// The initial condition for temperature is a disk which center is (xc,yc,zc)
/// and radius R.
//////////////////////////////////////////////////////////////////////////////
void ic_bifluid5eq_conduction(p4est_t *p4est, p4est_topidx_t which_tree,
                              p4est_quadrant_t *quad) {

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  /* get setting reader */
  SettingManager *stg_mgr = simu->m_stg_mgr;

  param_t *param = qdata_mgr->get_param();

  /* now start genuine initialization */
  cons_t w;
  memset(&w, 0, sizeof(cons_t));

  prim_t q;
  memset(&q, 0, sizeof(prim_t));

  // get the coordinates of the center of the quad
  double XYZ[3] = {0, 0, 0};
  double x, y, z;
  UNUSED(z);

  // get the physical coordinates of the center of the quad
  amr_mgr->quad_centre(which_tree, quad, XYZ);

  x = XYZ[IX];
  y = XYZ[IY];
  z = XYZ[IZ];

  // disk center, radius
  double x_c;
  double y_c;
  double z_c;
  double radius;

  x_c = stg_mgr->read_double("conduction.x_c", 0.5);
  y_c = stg_mgr->read_double("conduction.y_c", 0.5);
#ifdef USE_3D
  z_c = stg_mgr->read_double("conduction.z_c", 0.5);
#endif
  UNUSED(z_c);

  radius = stg_mgr->read_double("conduction.radius", 0.1);
  double radius2 = radius * radius;

  // fluid in
  double m_c = stg_mgr->read_double("conduction.m_c", 1.0);
  double P_c = stg_mgr->read_double("conduction.P_c", 100.0);

  // fluid out
  double m_out = stg_mgr->read_double("conduction.m_out", 1.0);
  double P_out = stg_mgr->read_double("conduction.P_out", 1.0);

  double d2 = SC_SQR(x - x_c) + SC_SQR(y - y_c);
#ifdef USE_3D
  d2 += SC_SQR(z - z_c);
#endif

  q.ml = 0.5 * (m_out + m_c * exp(-d2 / radius2));
  q.mg = 0.5 * (m_out + m_c * exp(-d2 / radius2));
  q.ag = 0.5;
  q.P = P_out + P_c * exp(-d2 / radius2);

  qdata_mgr->prim_to_cons(q, w);

  // finally set quadrant's qdata
  qdata_mgr->quad_set_cons(quad, &w);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Underwater explosion
///
/// Lochon et al. JCP Volume 326, 1 December 2016, Pages 733-762
//////////////////////////////////////////////////////////////////////////////
void ic_bifluid5eq_underwater_explosion(p4est_t *p4est,
                                        p4est_topidx_t which_tree,
                                        p4est_quadrant_t *quad) {
  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  /* get setting reader */
  SettingManager *stg_mgr = simu->m_stg_mgr;

  param_t *param = qdata_mgr->get_param();

  /* now start genuine initialization */
  cons_t w;
  memset(&w, 0, sizeof(cons_t));

  prim_t q;
  memset(&q, 0, sizeof(prim_t));

  // get the coordinates of the center of the quad
  double XYZ[3] = {0, 0, 0};
  double x, y, z;
  UNUSED(z);

  // get the physical coordinates of the center of the quad
  amr_mgr->quad_centre(which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  // disk center, radius
  double x_c;
  double y_c;
  double z_c;
  double radius;

  x_c = stg_mgr->read_double("underwater-explosion.x_c", 0.5);
  y_c = stg_mgr->read_double("underwater-explosion.y_c", 0.375);
#ifdef USE_3D
  z_c = stg_mgr->read_double("underwater-explosion.z_c", 0.5);
#endif
  UNUSED(z_c);

  radius = stg_mgr->read_double("underwater-explosion.radius", 0.05);
  double radius2 = radius * radius;

  double d2 = SC_SQR(x - x_c) + SC_SQR(y - y_c);
#ifdef USE_3D
  d2 += SC_SQR(z - z_c);
#endif

  // interface up/down
  double y0 = stg_mgr->read_double("underwater_explosion.y0", 0.5);

  double mg_up = stg_mgr->read_double("underwater_explosion.mg_up", 1.27);
  double ml_up = stg_mgr->read_double("underwater_explosion.ml_up", 0.0);
  double P_up = stg_mgr->read_double("underwater_explosion.P_up", 1.0E5);
  double ag_up = stg_mgr->read_double("underwater_explosion.ag_up", 1.0);

  double ml_down = stg_mgr->read_double("underwater_explosion.m_down", 1.0E3);
  double mg_down = stg_mgr->read_double("underwater_explosion.mg_down", 0.0);
  double P_down = stg_mgr->read_double("underwater_explosion.P_down", 1.0E5);
  double ag_down = stg_mgr->read_double("underwater_explosion.ag_down", 0.0);

  double mg_in = stg_mgr->read_double("underwater_explosion.mg_in", 1.27E3);
  double ml_in = stg_mgr->read_double("underwater_explosion.ml_in", 0.0);
  double P_in = stg_mgr->read_double("underwater_explosion.P_in", 8.29E8);
  double ag_in = stg_mgr->read_double("underwater_explosion.ag_in", 1.0);

  if (y < y0) {

    if (d2 < radius2) {

      q.mg = mg_in;
      q.ml = ml_in;
      q.ag = ag_in;
      q.P = P_in;

    } else {

      q.mg = mg_down;
      q.ml = ml_down;
      q.ag = ag_down;
      q.P = P_down;
    }

  } else {

    q.mg = mg_up;
    q.ml = ml_up;
    q.ag = ag_up;
    q.P = P_up;
  }

  qdata_mgr->prim_to_cons(q, w);

  // finally set quadrant's qdata
  qdata_mgr->quad_set_cons(quad, &w);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief jet, ref: TECNA R4G
//////////////////////////////////////////////////////////////////////////////
void ic_bifluid5eq_jet(p4est_t *p4est, p4est_topidx_t which_tree,
                       p4est_quadrant_t *quad) {

  UNUSED(which_tree);

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  SettingManager *stg_mgr = simu->m_stg_mgr;

  param_t *param = qdata_mgr->get_param();

  /* now start genuine initialization */
  cons_t w;
  memset(&w, 0, sizeof(cons_t));

  prim_t q;
  memset(&q, 0, sizeof(prim_t));

  q.mg = stg_mgr->read_double("jet.mg_out", 856.2);
  q.ml = stg_mgr->read_double("jet.ml_out", 0.0);
  q.ag = stg_mgr->read_double("jet.ag_out", 1.0);
  q.P = stg_mgr->read_double("jet.P_out", 1.0E5);

  qdata_mgr->prim_to_cons(q, w);

  // finally set quadrant's qdata
  qdata_mgr->quad_set_cons(quad, &w);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing initial conditions for bifluid5eq
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_ic_callback_t<qdata_t>>::Factory() {

  // register the above callback's
  register_factory("riemann_problem", ic_bifluid5eq_riemann_problem);
  register_factory("blast", ic_bifluid5eq_blast);
  register_factory("gresho", ic_bifluid5eq_gresho);
  register_factory("rayleigh_taylor", ic_bifluid5eq_rayleigh_taylor);
  register_factory("poiseuille", ic_bifluid5eq_poiseuille);
  register_factory("conduction", ic_bifluid5eq_conduction);
  register_factory("underwater_explosion", ic_bifluid5eq_underwater_explosion);
  register_factory("jet", ic_bifluid5eq_jet);
}
