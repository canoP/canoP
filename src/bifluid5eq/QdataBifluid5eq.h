//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef QDATA_BIFLUID5EQ_H_
#define QDATA_BIFLUID5EQ_H_

#include "Factory.h"
#include "Qdata.h"
#include "Simulation.h"
#include "eos.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief Type containing conservative variables for a bifluid5eq model
//////////////////////////////////////////////////////////////////////////////
struct bifluid5eq_cons_t {

  static constexpr unsigned nvar = 4 + P4EST_DIM;

  double mg;        //!< mass of gas per unit volume
  double ml;        //!< mass of liquid per unit volume
  array<double> mV; //!< 2D/3D momentum per unit volume
  double mE;        //!< total energy per unit volume
  double ag;        //!< gas volume fraction

  inline bifluid5eq_cons_t() : mg(0.0), ml(0.0), mE(0.0), ag(0.0) {
    mV.fill(0.0);
  }
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Type containing primitive variables for a bifluid5eq model
//////////////////////////////////////////////////////////////////////////////
struct bifluid5eq_prim_t {

  static constexpr unsigned nvar = 5 + P4EST_DIM;

  double mg;       //!< mass of gas per unit volume
  double ml;       //!< mass of liquid per unit volume
  double P;        //!< pressure
  double T;        //!< temperature
  double ag;       //!< gas volume fraction
  array<double> V; //!< 2D/3D velocity

  inline bifluid5eq_prim_t() : mg(0.0), P(0.0), T(0.0), ml(0.0), ag(0.0) {
    V.fill(0.0);
  }
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Bifluid5eq primitive type deduction from conservative type
//////////////////////////////////////////////////////////////////////////////
template <> struct to_prim<bifluid5eq_cons_t> {
  using type = bifluid5eq_prim_t;
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Type containing parameters for a bifluid5eq model
/// (read from the param group in the setting file)
//////////////////////////////////////////////////////////////////////////////
struct bifluid5eq_param_t {

  eos_t eos_g; //!< parameters of the stiffened-gas eos for the gas phase
               //!< (default gamma=1.666, Cv=1)
  eos_t eos_l; //!< parameters of the stiffened-gas eos for the liquid phase
               //!< (default gamma=1.666, Cv=1)

  double gravity_x; //!< gravitational force in the x direction (default 0.0)
  double gravity_y; //!< gravitational force in the y direction (default 0.0)
  double gravity_z; //!< gravitational force in the z direction (default 0.0)

  // for all-regime scheme
  int lowmach_correction_enabled; //!< low-mach correction for the all-regime
                                  //!< solver (default 0, options 0 or 1)
  double K; //!< diffusion coefficient for the all-regime solver (default 1.1)
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Bifluid5eq parameter type deduction from conservative type
//////////////////////////////////////////////////////////////////////////////
template <> struct to_param<bifluid5eq_cons_t> {
  using type = bifluid5eq_param_t;
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Namespace containing types for a bifluid5eq simulation
/// with additional traits
//////////////////////////////////////////////////////////////////////////////
namespace bifluid5eq {

using cons_t = generic_data_t<bifluid5eq_cons_t>;
using prim_t = to_prim_t<cons_t>;
using qdata_t = to_qdata_t<cons_t>;
using param_t = to_param_t<cons_t>;

} // namespace bifluid5eq

#ifndef SKIPPED_BY_DOXYGEN

/// Factory storing initiation conditions for bifluid5eq
template <>
Factory<bifluid5eq::qdata_t, to_ic_callback_t<bifluid5eq::qdata_t>>::Factory();

/// Factory storing boundary conditions for bifluid5eq
template <>
Factory<bifluid5eq::qdata_t, to_bc_callback_t<bifluid5eq::qdata_t>>::Factory();

/// Factory storing refine conditions for bifluid5eq
template <>
Factory<bifluid5eq::qdata_t, to_rc_callback_t<bifluid5eq::qdata_t>>::Factory();

/// Factory storing hyberbolic fluxes for bifluid5eq
template <>
Factory<bifluid5eq::qdata_t, to_hf_callback_t<bifluid5eq::cons_t>>::Factory();

/// Factory storing source terms for bifluid5eq
template <>
Factory<bifluid5eq::qdata_t, to_st_callback_t<bifluid5eq::cons_t>>::Factory();

/// Factory storing diffusion fluxes for bifluid5eq
template <>
Factory<bifluid5eq::qdata_t, to_df_callback_t<bifluid5eq::cons_t>>::Factory();

//////////////////////////////////////////////////////////////////////////////
/// @name specialization for bifluid5eq
/// @{
//////////////////////////////////////////////////////////////////////////////
template <>
QdataManager<bifluid5eq::cons_t>::QdataManager(SettingManager *stg_mgr);
template <>
void QdataManager<bifluid5eq::cons_t>::cons_to_prim(const bifluid5eq::cons_t &w,
                                                    bifluid5eq::prim_t &r);
template <>
void QdataManager<bifluid5eq::cons_t>::prim_to_cons(
    const bifluid5eq::prim_t &pdata, bifluid5eq::cons_t &cdata);
template <>
void QdataManager<bifluid5eq::cons_t>::get_hydro_flux_predictor(
    int idir, const bifluid5eq::cons_t &cc, const bifluid5eq::prim_t &pf,
    bifluid5eq::cons_t &flux);
template <>
void QdataManager<bifluid5eq::cons_t>::get_grav_source_predictor(
    const bifluid5eq::prim_t &pdata, bifluid5eq::cons_t &source);
template <>
std::vector<double>
QdataManager<bifluid5eq::cons_t>::quad_get_field(const std::string &field_name,
                                                 p4est_quadrant_t *quad);
template <>
void QdataManager<bifluid5eq::cons_t>::swap_dir(
    int idir, bifluid5eq::prim_t &pdata, bifluid5eq::cons_t &cdata,
    array<bifluid5eq::prim_t> &delta);
template <>
double QdataManager<bifluid5eq::cons_t>::get_hydro_invdt(
    const bifluid5eq::prim_t &pdata, const double dx);
template <>
double QdataManager<bifluid5eq::cons_t>::get_cond_invdt(
    const bifluid5eq::prim_t &pdata, const double dx);
template <>
double QdataManager<bifluid5eq::cons_t>::get_visc_invdt(
    const bifluid5eq::prim_t &pdata, const double dx);
/// @}

#endif

#endif
