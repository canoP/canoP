//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "utils.h"

//////////////////////////////////////////////////////////////////////////////
double limiter_minmod(double ul, double ur) {

  if ((ul * ur) < 0) {
    return 0;
  } else if (fabs(ul) < fabs(ur)) {
    return ul;
  } else {
    return ur;
  }
}

//////////////////////////////////////////////////////////////////////////////
double limiter_maxmod(double ul, double ur) {

  if ((ul * ur) < 0) {
    return 0;
  } else if (fabs(ul) > fabs(ur)) {
    return ul;
  } else {
    return ur;
  }
}

//////////////////////////////////////////////////////////////////////////////
double limiter_mc(double ul, double ur) {

  return limiter_minmod(2 * limiter_minmod(ul, ur), (ur + ul) / 2.);
}

//////////////////////////////////////////////////////////////////////////////
double limiter_smart(double ul, double ur) {

  if (ISFUZZYNULL(ur)) {
    return 0;
  }

  if (ISFUZZYNULL(ul)) {
    return 0;
  }

  double r = ul / ur;
  return fmax(0, fmin(fmin(2.0 * r, 0.25 + 0.75 * r), 4)) * ur;
}

//////////////////////////////////////////////////////////////////////////////
double limiter_superbee(double ul, double ur) {

  if (ISFUZZYNULL(ur)) {
    return 0;
  }

  if (ISFUZZYNULL(ul)) {
    return 0;
  }

  return limiter_maxmod(limiter_minmod(2 * ul, ur), limiter_minmod(2 * ur, ul));
}

//////////////////////////////////////////////////////////////////////////////
double limiter_sweby(double ul, double ur) {

  if (ISFUZZYNULL(ur)) {
    return 0;
  }

  if (ISFUZZYNULL(ul)) {
    return 0;
  }

  const double beta = 1.5;

  double r = ul / ur;
  return fmax(0, fmax(fmin(beta * r, 1), fmin(r, beta))) * ur;
}

//////////////////////////////////////////////////////////////////////////////
double limiter_charm(double ul, double ur) {

  if (ISFUZZYNULL(ur)) {
    return 0;
  }

  if (ISFUZZYNULL(ul)) {
    return 0;
  }

  double r = ul / ur;
  return r * (3.0 * r + 1) / ((r + 1) * (r + 1)) * (r > 0) * ur;
}

//////////////////////////////////////////////////////////////////////////////
double limiter_osher(double ul, double ur) {

  if (ISFUZZYNULL(ur)) {
    return 0;
  }

  if (ISFUZZYNULL(ul)) {
    return 0;
  }

  const double beta = 1.5;
  double r = ul / ur;
  return fmax(0, fmin(r, beta)) * ur;
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing scalar limiters
//////////////////////////////////////////////////////////////////////////////
template <> Factory<void, sl_callback_t>::Factory() {

  register_factory("minmod", limiter_minmod);
  register_factory("maxmod", limiter_maxmod);
  register_factory("mc", limiter_mc);
  register_factory("smart", limiter_smart);
  register_factory("superbee", limiter_superbee);
  register_factory("sweby", limiter_sweby);
  register_factory("charm", limiter_charm);
  register_factory("osher", limiter_osher);
}
