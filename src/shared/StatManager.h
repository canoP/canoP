//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef STAT_MANAGER_H_
#define STAT_MANAGER_H_

#include "SettingManager.h"
#include "utils.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief Stat manager to gather performance statistics
//////////////////////////////////////////////////////////////////////////////
class StatManager {

public:
  //////////////////////////////////////////////////////////////////////////////
  /// \brief Accumulation types.
  ///
  /// There are two accumulations: at every time step over MPI tasks, and over
  /// all time steps. The \c timer_accum::none value is only valid over MPI
  /// tasks, where it prevents communications.
  //////////////////////////////////////////////////////////////////////////////
  enum class timer_accum { sum, min, max, mean, none };

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Add a new timer.
  ///
  /// @param[in] name           A name for the timer
  /// @param[in] mpi_accum      The accumulation over MPI tasks
  /// @param[in] time_accum     The accumulation over time steps
  ///
  /// @returns true if the timer has been created, false if the name already
  /// exists
  //////////////////////////////////////////////////////////////////////////////
  static bool add_timer(const char *name, timer_accum mpi_accum,
                        timer_accum time_accum);

protected:
  //////////////////////////////////////////////////////////////////////////////
  /// Type containing information for a timer
  //////////////////////////////////////////////////////////////////////////////
  struct timer_info_t {
    const char *name;
    timer_accum mpi_accum;
    timer_accum time_accum;

    timer_info_t() : name(nullptr) {} // used by map::operator[]
    timer_info_t(const char *name_, timer_accum mpi_accum_,
                 timer_accum time_accum_)
        : name(name_), mpi_accum(mpi_accum_), time_accum(time_accum_) {}
  };

  using timer_info_map = std::map<std::string, timer_info_t>;

  static timer_info_map s_timer_info; //!< map containing the timers infos

public:
  //////////////////////////////////////////////////////////////////////////////
  /// All fields are initialized to TYPE_MAX and TYPE_MIN, as need be.
  ///
  /// @param[in] stg_mgr a SettingManager object (to parse input setting file)
  /// @param[in] mpicomm MPI communicator
  //////////////////////////////////////////////////////////////////////////////
  StatManager(SettingManager *stg_mgr, MPI_Comm mpicomm);
  ~StatManager();

  MPI_Comm m_mpicomm; //!< the global communicator
  int m_mpisize;      //!< number of processses
  int m_mpirank;      //!< the current process

  /// local min / max number of quadrants. Sum of the local_num_quadrants
  p4est_locidx_t *m_local_num_quadrants[2];

  /// global min / max number of quadrants.
  p4est_gloidx_t m_global_num_quadrants[2];

  std::time_t m_start; //!< run start timestamp
  std::time_t m_end;   //!< run end timestamp

  int m_iterations; //!< number of iterations
  double m_t;       //!< final time
  double m_dt[2];   //!< min / max time step
  int m_level[2];   //!< min / max level

  double m_norml1; //!< L1 norm
  double m_norml2; //!< L2 norm

  std::map<std::string, double> m_timers; //!< map containing the timers

  int m_verboseLevel; //!< verbose level for the timers

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Test if statistics should be collected.
  //////////////////////////////////////////////////////////////////////////////
  int get_stat();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Update the min / max time step from this new value.
  //////////////////////////////////////////////////////////////////////////////
  void statistics_dt(double dt);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Update the min / max levels from this new values.
  //////////////////////////////////////////////////////////////////////////////
  void statistics_level(int min, int max);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Update the min / max local number of quadrants from this new value.
  //////////////////////////////////////////////////////////////////////////////
  void statistics_num_quadrants(int lnq);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Update a timer by its name.
  //////////////////////////////////////////////////////////////////////////////
  void statistics_timing_byname(std::string name, double t);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Update the min / max time to advance by one iteration.
  //////////////////////////////////////////////////////////////////////////////
  void statistics_timing_scheme(double t);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Update the min / max time to write the solution this new value.
  //////////////////////////////////////////////////////////////////////////////
  void statistics_timing_io(double t);

  //////////////////////////////////////////////////////////////////////////////
  /// @name amr statistics
  /// \brief Update the min / max time to adapt the mesh from this new value.
  //////////////////////////////////////////////////////////////////////////////
  /// @{
  void statistics_timing_adapt(double t);
  void statistics_timing_adapt_mark(double t);
  void statistics_timing_adapt_balance(double t, double *stat_time);
  void statistics_timing_adapt_partition(double t);
  /// @}

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Write the data inside the statistics struct to a file.
  ///
  /// Only process 0 will write to the file. Before writing, a global reduce
  /// and gather is done on all the members to get the corresponding global
  /// values.
  ///
  /// @param[in] filename Output filename for statistics.
  //////////////////////////////////////////////////////////////////////////////
  void statistics_write(const std::string &filename);

private:
  //////////////////////////////////////////////////////////////////////////////
  /// \brief Get global values for all the members of the statistics struct.
  //////////////////////////////////////////////////////////////////////////////
  void statistics_reduce();
};

#endif
