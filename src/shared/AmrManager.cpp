//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "AmrManager.h"

//////////////////////////////////////////////////////////////////////////////
AmrManager::AmrManager(SettingManager *stg_mgr, MPI_Comm mpicomm,
                       int qdata_t_sizeof)
    : m_stg_mgr(stg_mgr) {

  p4est_t *p4est = NULL;
  p4est_connectivity_t *connectivity = NULL;

  P4EST_GLOBAL_PRODUCTION("-- read amr settings -- \n");

  m_min_quadrants = m_stg_mgr->readv_int("amr.min_quadrants", 16);
  m_min_refine = m_stg_mgr->readv_int("amr.min_refine", 5);
  m_max_refine = m_stg_mgr->readv_int("amr.max_refine", 7);
  m_epsilon_refine = m_stg_mgr->readv_double("amr.epsilon_refine", 0.1);
  m_epsilon_coarsen = m_stg_mgr->readv_double("amr.epsilon_coarsen", 0.1);
  m_ltree = m_stg_mgr->readv_double("amr.ltree", 1.0);

  m_restart_enabled = m_stg_mgr->read_int("io.restart_enabled", 0);
  m_restart_filename =
      m_stg_mgr->read_string("io.restart_filename", "data.p4est");

  if (m_restart_enabled) {

    // this is a restart run, p4est and connectivity are created from the
    // restart file data
    p4est = p4est_load_ext(m_restart_filename.c_str(), mpicomm,
                           qdata_t_sizeof, // size of data per quadrant
                           1,              // enable data loading
                           1,              // enable auto-partition
                           0, // enable header broadcasting is not implemented
                           NULL,         // the user pointer
                           &connectivity //connectivity is initialized from\
                                        the restart information
    );

  } else {

    // build connectivity
    connectivity = get_connectivity();

    P4EST_GLOBAL_PRODUCTION("\n");

    SC_CHECK_ABORT(connectivity != NULL, "connectivity is NULL");

    // buid p4est
    p4est = p4est_new_ext(mpicomm, connectivity, m_min_quadrants, m_min_refine,
                          true,           // fill_uniform
                          qdata_t_sizeof, // sizeof (qdata_t),
                          NULL,
                          NULL // user_pointer
    );
  }

  P4EST_GLOBAL_PRODUCTION("\n");

  // initialize p4est wrap structure
  m_wrap = p4est_wrap_new_p4est(p4est, false, P4EST_CONNECT_FACE, NULL, NULL);

  SC_CHECK_ABORT(p4est != NULL, "p4est is NULL");

  m_maxlevel = 0;
  m_minlevel = P4EST_QMAXLEVEL;
}

//////////////////////////////////////////////////////////////////////////////
AmrManager::~AmrManager() { p4est_wrap_destroy(m_wrap); }

//////////////////////////////////////////////////////////////////////////////
p4est_connectivity_t *AmrManager::get_connectivity() {

  // read tree structure and periodicity for brick connectivity
  m_ntree_x = m_stg_mgr->readv_int("amr.ntree_x", 1);
  m_ntree_y = m_stg_mgr->readv_int("amr.ntree_y", 1);
  m_ntree_z = m_stg_mgr->readv_int("amr.ntree_z", 1);

  m_periodic_x = m_stg_mgr->readv_int("amr.periodic_x", 1);
  m_periodic_y = m_stg_mgr->readv_int("amr.periodic_y", 1);
  m_periodic_z = m_stg_mgr->readv_int("amr.periodic_z", 1);

  // read if a connectivity file has been provided
  std::string string_name =
      m_stg_mgr->readv_string("amr.connectivity_file", "none");

  p4est_connectivity_t *c = NULL;

  if (!string_name.compare("none")) {

    // create brick connectivity
#ifndef P4_TO_P8
    return p4est_connectivity_new_brick(m_ntree_x, m_ntree_y, m_periodic_x,
                                        m_periodic_y);
#else
    return p4est_connectivity_new_brick(m_ntree_x, m_ntree_y, m_ntree_z,
                                        m_periodic_x, m_periodic_y,
                                        m_periodic_z);
#endif

  } else {

    // create inp connectivity from connectivity_file
    c = p4est_connectivity_read_inp(string_name.c_str());

    P4EST_GLOBAL_PRODUCTION("#### WARNING ####\n");
    P4EST_GLOBAL_PRODUCTIONF("Connectivity file %s is used\n",
                             string_name.data());
    P4EST_GLOBAL_PRODUCTION("Quadrants are assumed to be squares or cubes\n");
    P4EST_GLOBAL_PRODUCTION("#### WARNING ####\n");
    if (c == NULL) {

      P4EST_GLOBAL_LERROR("#### ERROR ####\n");
      P4EST_GLOBAL_LERRORF("Connectivity file %s was not found.\n",
                           string_name.data());
      P4EST_GLOBAL_LERROR("#### ERROR ####\n");
      SC_ABORT("Error in SimulationBase::get_connectivity()\n");

    } else {

      return c;
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
int AmrManager::adapt() { return p4est_wrap_adapt(m_wrap); }

//////////////////////////////////////////////////////////////////////////////
int AmrManager::partition() {

  return p4est_wrap_partition(m_wrap, 0, NULL, NULL, NULL);
}

//////////////////////////////////////////////////////////////////////////////
void AmrManager::complete() { p4est_wrap_complete(m_wrap); }

//////////////////////////////////////////////////////////////////////////////
double AmrManager::quad_dx(p4est_quadrant_t *quad) {

  return (double)P4EST_QUADRANT_LEN(quad->level) / P4EST_ROOT_LEN * m_ltree;
}

//////////////////////////////////////////////////////////////////////////////
double quad_dS(double dx) {

  double dS;

#ifdef USE_3D
  dS = dx * dx;
#else
  dS = dx;
#endif

  return dS;
}

//////////////////////////////////////////////////////////////////////////////
double quad_dV(double dx) {

  double dV;

#ifdef USE_3D
  dV = dx * dx * dx;
#else
  dV = dx * dx;
#endif

  return dV;
}

//////////////////////////////////////////////////////////////////////////////
void AmrManager::quad_centre(p4est_topidx_t tree, p4est_quadrant_t *quad,
                             double xyz[3]) {

  p4est_connectivity_t *connectivity = m_wrap->conn;

  p4est_qcoord_t half_length = P4EST_QUADRANT_LEN(quad->level) / 2;

  p4est_qcoord_to_vertex(connectivity, tree, quad->x + half_length,
                         quad->y + half_length,
#ifdef P4_TO_P8
                         quad->z + half_length,
#endif
                         xyz);

  xyz[0] *= m_ltree;
  xyz[1] *= m_ltree;
  xyz[2] *= m_ltree;
}
