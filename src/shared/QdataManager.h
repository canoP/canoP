//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef QDATA_MANAGER_H_
#define QDATA_MANAGER_H_

#include "AmrManager.h"
#include "Qdata.h"
#include "SettingManager.h"
#include "utils.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief Generic Qdata manager to handle operations on the data of
/// a p4est quadrant
///
/// @tparam cons_t A type containing a set of conservative variables
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t> class QdataManager {

public:
  using prim_t = to_prim_t<cons_t>;
  using qdata_t = to_qdata_t<cons_t>;
  using param_t = to_param_t<cons_t>;

public:
  //////////////////////////////////////////////////////////////////////////////
  /// Implementation of the constructor is done at specialization. It will set
  /// all the parameters needed for this simulation type.
  ///
  /// @param[in] stg_mgr a SettingManager object (to parse input setting file)
  //////////////////////////////////////////////////////////////////////////////
  QdataManager(SettingManager *stg_mgr);
  ~QdataManager() = default;

  param_t m_param; //!< parameters needed for this simulation type

  void *m_user_param; //!< parameters defined by the user

  /// getter for a pointer to m_param
  param_t *get_param() { return &m_param; };

  /// getter for a pointer to m_user_param
  void *get_user_param() { return m_user_param; };

  /// setter for a pointer to m_user_param
  void set_user_param(void *pointer) { m_user_param = pointer; };

  //////////////////////////////////////////////////////////////////////////////
  /// \brief get qdata from a given quad
  ///
  /// @param[in] quad A p4est quadrant
  //////////////////////////////////////////////////////////////////////////////
  static qdata_t *quad_get_qdata(p4est_quadrant_t *quad) {

    return (qdata_t *)quad->p.user_data;
  }

  //////////////////////////////////////////////////////////////////////////////
  /// \brief set conservative variables in a given quad
  ///
  /// @param[inout] quad_dst a p4est quadrant
  /// @param[in] cons_src a conservative set of variables
  //////////////////////////////////////////////////////////////////////////////
  static void quad_set_cons(p4est_quadrant_t *quad_dst, cons_t *cons_src) {

    qdata_t *qdata_dst = quad_get_qdata(quad_dst);

    memcpy(&(qdata_dst->w), cons_src, sizeof(cons_t));
    memcpy(&(qdata_dst->wnext), cons_src, sizeof(cons_t));
    memset(&(qdata_dst->delta), 0, P4EST_DIM * sizeof(prim_t));
    memset(&(qdata_dst->wm), 0, P4EST_DIM * sizeof(prim_t));
    memset(&(qdata_dst->wp), 0, P4EST_DIM * sizeof(prim_t));
  };

  //////////////////////////////////////////////////////////////////////////////
  /// \brief set conservative variables in a given quad from another quad
  ///
  /// @param[inout] quad_dst a p4est quadrant
  /// @param[in] quad_src a p4est quadrant
  //////////////////////////////////////////////////////////////////////////////
  static void quad_set_cons(p4est_quadrant_t *quad_dst,
                            p4est_quadrant_t *quad_src) {

    qdata_t *qdata_dst = quad_get_qdata(quad_dst);
    qdata_t *qdata_src = quad_get_qdata(quad_src);

    memcpy(&(qdata_dst->w), &(qdata_src->w), sizeof(cons_t));
    memcpy(&(qdata_dst->wnext), &(qdata_src->wnext), sizeof(cons_t));
    memset(&(qdata_dst->delta), 0, P4EST_DIM * sizeof(prim_t));
    memset(&(qdata_dst->wm), 0, P4EST_DIM * sizeof(prim_t));
    memset(&(qdata_dst->wp), 0, P4EST_DIM * sizeof(prim_t));
  };

  //////////////////////////////////////////////////////////////////////////////
  /// \brief set conservative variables in a given quad from the mean of a set
  /// of children quads
  ///
  /// @param[inout] quad_dst a p4est quadrant
  /// @param[] quad_src a list of p4est quadrants
  //////////////////////////////////////////////////////////////////////////////
  static void quad_mean_cons(p4est_quadrant_t *quad_dst,
                             p4est_quadrant_t *quad_src[]) {

    qdata_t *qdata_src;
    cons_t cdata_dst;

    memset(&cdata_dst, 0, sizeof(cons_t));

    double *dst_array = reinterpret_cast<double *>(&cdata_dst);

    for (int8_t i = 0; i < P4EST_CHILDREN; ++i) {
      qdata_src = quad_get_qdata(quad_src[i]);

      double *src_array = reinterpret_cast<double *>(&qdata_src->w);

      for (int ivar = 0; ivar < cons_t::nvar; ++ivar) {

        dst_array[ivar] += src_array[ivar] / P4EST_CHILDREN;
      }
    }

    // finally, copy all of that into quad_dst
    quad_set_cons(quad_dst, &cdata_dst);
  };

  //////////////////////////////////////////////////////////////////////////////
  /// \brief This replace callback can be used either when refining or
  /// coarsening.
  ///
  /// When refining, num_outgoing = 1 and num_incoming = P4EST_CHILDREN.
  /// num_outgoing == 1 is used to trigger the refining behavior and actually
  /// num_incoming is discarded
  ///
  /// When coarsening, num_outgoing = P4EST_CHILDREN and num_incoming = 1.
  /// num_incoming == 1 is used to trigger the coarsening behavior and actually
  /// num_outgoing is discarded
  ///
  /// @param[in] p4est          the forest
  /// @param[in] which_tree     the tree in the forest containing \a children
  /// @param[in] num_outgoing   the number of quadrants that are being replaced:
  ///                           either 1 if a quadrant is being refined, or
  ///                           P4EST_CHILDREN if a family of children are being
  ///                           coarsened.
  /// @param[inout] quadout     the outgoing quadrants
  /// @param[in] num_incoming   the number of quadrants that are being added:
  ///                           either P4EST_CHILDREN if a quadrant is being
  /// refined, or 1 if a family of children are being coarsened. \param [in,out]
  /// quadin     quadrants whose data are initialized.
  //////////////////////////////////////////////////////////////////////////////
  static void quad_replace_fn(p4est_t *p4est, p4est_topidx_t which_tree,
                              int num_outgoing, p4est_quadrant_t *quadout[],
                              int num_incoming, p4est_quadrant_t *quadin[]) {

    UNUSED(p4est);
    UNUSED(which_tree);
    UNUSED(num_incoming);

    if (num_outgoing == 1) { // we're refining

      // parent data are in quadout[0]
      for (int8_t i = 0; i < P4EST_CHILDREN; ++i) {
        quad_set_cons(quadin[i], quadout[0]);
      }

    } else { // we're coarsening

      quad_mean_cons(quadin[0], quadout);
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  /// \brief set qdata in one quadrant
  ///
  /// @param[inout] quad_dst a p4est quadrant
  /// @param[in] qdata_src source qdata
  //////////////////////////////////////////////////////////////////////////////
  void quad_set_qdata(p4est_quadrant_t *quad_dst, qdata_t *qdata_src) {

    qdata_t *qdata_dst = quad_get_qdata(quad_dst);
    memcpy(qdata_dst, qdata_src, sizeof(qdata_t));
  }

  //////////////////////////////////////////////////////////////////////////////
  /// \brief copy qdata from one quadrant to another
  ///
  /// @param[inout] quad_dst a p4est quadrant
  /// @param[in] quad_src a p4est quadrant
  //////////////////////////////////////////////////////////////////////////////
  void quad_copy_qdata(p4est_quadrant_t *quad_dst, p4est_quadrant_t *quad_src) {

    qdata_t *qdata_src = quad_get_qdata(quad_src);
    qdata_t *qdata_dst = quad_get_qdata(quad_dst);

    memcpy(qdata_dst, qdata_src, sizeof(qdata_t));
  }

  //////////////////////////////////////////////////////////////////////////////
  /// \brief print coordinates
  ///
  /// @param[in] log_priority
  /// @param[in] quad a p4est quadrant
  //////////////////////////////////////////////////////////////////////////////
  void quad_print(int log_priority, p4est_quadrant_t *quad) {

    p4est_qcoord_t x = (quad->x) >> (P4EST_MAXLEVEL - quad->level);
    p4est_qcoord_t y = (quad->y) >> (P4EST_MAXLEVEL - quad->level);
#ifdef P4_TO_P8
    p4est_qcoord_t z = (quad->z) >> (P4EST_MAXLEVEL - quad->level);
    P4EST_LOGF(log_priority, "x %d y %d z %d level %d\n", x, y, z, quad->level);
#else
    P4EST_LOGF(log_priority, "x %d y %d level %d\n", x, y, quad->level);
#endif
  }

  //////////////////////////////////////////////////////////////////////////////
  /// \brief copy w into wnext
  ///
  /// @param[in] quad a p4est quadrant
  //////////////////////////////////////////////////////////////////////////////
  void quad_copy_w_to_wnext(p4est_quadrant_t *quad) {
    qdata_t *qdata = quad_get_qdata(quad);
    memcpy(&(qdata->wnext), &(qdata->w), sizeof(cons_t));
  }

  //////////////////////////////////////////////////////////////////////////////
  /// \brief copy wnext into w
  ///
  /// @param[in] quad a p4est quadrant
  //////////////////////////////////////////////////////////////////////////////
  void quad_copy_wnext_to_w(p4est_quadrant_t *quad) {
    qdata_t *qdata = quad_get_qdata(quad);
    memcpy(&(qdata->w), &(qdata->wnext), sizeof(cons_t));
  }

  //////////////////////////////////////////////////////////////////////////////
  /// \brief set wm wp delta to zero
  ///
  /// @param[in] quad a p4est quadrant
  //////////////////////////////////////////////////////////////////////////////
  void quad_wm_wp_delta_zero(p4est_quadrant_t *quad) {
    qdata_t *qdata = quad_get_qdata(quad);

    memset(&(qdata->wp), 0, P4EST_DIM * sizeof(prim_t));
    memset(&(qdata->wm), 0, P4EST_DIM * sizeof(prim_t));
    memset(&(qdata->delta), 0, P4EST_DIM * sizeof(prim_t));
  }

  //////////////////////////////////////////////////////////////////////////////
  /// \brief set a primitive state to zero
  ///
  /// @param[in] pdata a primitive state
  //////////////////////////////////////////////////////////////////////////////
  void prim_zero(prim_t &pdata) {

    double *pdata_array = reinterpret_cast<double *>(&pdata);

    for (int ivar = 0; ivar < prim_t::nvar; ++ivar) {
      pdata_array[ivar] = 0.0;
    }
  };

  //////////////////////////////////////////////////////////////////////////////
  /// \brief copy a primitive state into another
  ///
  /// @param[inout] pdata1 a primitive state
  /// @param[in]    pdata2 a primitive state
  //////////////////////////////////////////////////////////////////////////////
  void prim_copy(prim_t &pdata1, const prim_t &pdata2) {

    memcpy(&pdata1, &pdata2, sizeof(prim_t));
  }

  /////////////////////////////////////////////////////////////////////////////
  /// \brief swap two primitive state
  ///
  /// @param[inout] pdata1 a primitive state
  /// @param[inout] pdata2 a primitive state
  //////////////////////////////////////////////////////////////////////////////
  void prim_swap(prim_t &pdata1, prim_t &pdata2) {

    prim_t tmp = pdata1;
    pdata1 = pdata2;
    pdata2 = tmp;
  }

  /////////////////////////////////////////////////////////////////////////////
  /// \brief change the sign of all fields in a primitive state
  ///
  /// @param[inout] pdata a primitive state
  //////////////////////////////////////////////////////////////////////////////
  void prim_change_sign(prim_t &pdata) {

    double *pdata_array = reinterpret_cast<double *>(&pdata);

    for (int ivar = 0; ivar < prim_t::nvar; ++ivar) {
      pdata_array[ivar] = -pdata_array[ivar];
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  /// \brief copy a qdata into another
  ///
  /// @param[inout] qdata_dst a qdata
  /// @param[in]    qdata_src a qdata
  //////////////////////////////////////////////////////////////////////////////
  void qdata_copy(qdata_t *qdata_dst, qdata_t *qdata_src) {

    memcpy(qdata_dst, qdata_src, sizeof(qdata_t));
  }

  //////////////////////////////////////////////////////////////////////////////
  /// \brief get a given field by name from a quadrant. Implementation is done
  /// at specialization
  ///
  /// @param[in] field_name The name of the field in cons_t or prim_t
  /// @param[in] quad a p4est quadrant
  //////////////////////////////////////////////////////////////////////////////
  std::vector<double> quad_get_field(const std::string &field_name,
                                     p4est_quadrant_t *quad);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Convert a conservative state into a primitive state.
  /// Implementation is done at specialization
  ///
  /// @param[in]    cdata a conservative state
  /// @param[inout] pdata a primitive state
  //////////////////////////////////////////////////////////////////////////////
  void cons_to_prim(const cons_t &cdata, prim_t &pdata);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Convert a primitive state into a conservative state.
  /// Implementation is done at specialization
  ///
  /// @param[in]    pdata a primitive state
  /// @param[inout] cdata a conservative state
  //////////////////////////////////////////////////////////////////////////////
  void prim_to_cons(const prim_t &pdata, cons_t &cdata);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Get the hydrodynamics flux for the Hancock prediction step.
  /// Implementation is done at specialization
  ///
  /// @param[in]    idir the direction of the face
  /// @param[in]    cdata a conservative state (at cell centre)
  /// @param[in]    pdata a primitive state (at celle face)
  /// @param[inout] flux a conservative state storing the flux
  //////////////////////////////////////////////////////////////////////////////
  void get_hydro_flux_predictor(int idir, const cons_t &cdata,
                                const prim_t &pdata, cons_t &flux);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Get the gravity source term for the Hancock prediction step.
  /// Implementation is done at specialization
  ///
  /// @param[in]    pdata a primitive state (at celle centre)
  /// @param[inout] source a conservative state storing the source term
  //////////////////////////////////////////////////////////////////////////////
  void get_grav_source_predictor(const prim_t &pdata, cons_t &source);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Get the inverse of the hydrodynamics time step. Implementation is
  /// done at specialization
  ///
  /// @param[in]    pdata a primitive state (at celle centre)
  /// @param[in]    dx the size of the quadrant
  //////////////////////////////////////////////////////////////////////////////
  double get_hydro_invdt(const prim_t &pdata, const double dx);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Get the inverse of the conductivity time step. Implementation is
  /// done at specialization
  ///
  /// @param[in]    pdata a primitive state (at celle centre)
  /// @param[in]    dx the size of the quadrant
  //////////////////////////////////////////////////////////////////////////////
  double get_cond_invdt(const prim_t &pdata, const double dx);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Get the inverse of the viscosity time step. Implementation is
  /// done at specialization
  ///
  /// @param[in]    pdata a primitive state (at celle centre)
  /// @param[in]    dx the size of the quadrant
  //////////////////////////////////////////////////////////////////////////////
  double get_visc_invdt(const prim_t &pdata, const double dx);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Get the inverse of the user time step. Return 0 unless
  /// Implementation is done at specialization
  ///
  /// @param[in]    pdata a primitive state (at celle centre)
  /// @param[in]    dx the size of the quadrant
  //////////////////////////////////////////////////////////////////////////////
  double get_user_invdt(const prim_t &pdata, const double dx) {
    UNUSED(pdata);
    UNUSED(dx);
    return 0.;
  };

  //////////////////////////////////////////////////////////////////////////////
  /// \brief swap direction (used before flux computation). Implementation is
  /// done at specialization
  ///
  /// @param[in]    idir the direction of the face
  /// @param[inout] pdata a primitive state (at cell face)
  /// @param[inout] cdata a conservative state (at cell centre)
  /// #param[inout] delta an array of gradients of the primitive state
  //////////////////////////////////////////////////////////////////////////////
  void swap_dir(int idir, prim_t &pdata, cons_t &cdata, array<prim_t> &delta);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief swap direction (used after flux computation). Implementation is
  /// done at specialization
  ///
  /// @param[in]    idir the direction of the face
  /// @param[inout] cdata a conservative state storing e.g. the flux
  //////////////////////////////////////////////////////////////////////////////
  void swap_dir(int idir, cons_t &cdata);
};

#endif
