//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef IO_MANAGER_H
#define IO_MANAGER_H

#include "Factory.h"
#include "QdataManager.h"
#include "SettingManager.h"
#include "utils.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief The four types of supported attribute types.
//////////////////////////////////////////////////////////////////////////////
enum io_attribute_type_t {
  IO_CELL_SCALAR, //!< A cell-centered scalar.
  IO_CELL_VECTOR, //!< A cell-centered vector.
  IO_NODE_SCALAR, //!< A node-centered scalar.
  IO_NODE_VECTOR  //!< A node-centered vector.
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Io manager to handle data input/output
///
/// The writer has no opened files by default except a main xmf file
/// that contains a collection of all the files that are to be written
/// next as a `Temporal` Collection.
//////////////////////////////////////////////////////////////////////////////
class IoManager {

public:
  //////////////////////////////////////////////////////////////////////////////
  /// @param[in] stg_mgr a SettingManager object (to parse input setting file)
  /// @param[in] mpicomm MPI communicator
  //////////////////////////////////////////////////////////////////////////////
  IoManager(SettingManager *stg_mgr, MPI_Comm mpicomm);
  ~IoManager();

  p4est_t *m_p4est;         //!< not owned, pointer to p4est struct
  p4est_geometry_t *m_geom; //!< nullptr canoP hadles only cartesian geometry
  p4est_nodes_t *m_nodes;   //!< nullptr or owned if nodes are needed

  std::string m_basename; //!< the base name of the two files
  hid_t m_hdff;           //!< HDF file descriptor
  FILE *m_xmff;           //!< XMF file descriptor
  FILE *m_main_xmff;      //!< XMF main file descriptor
  int m_times_saved;      //!< current ouput number
  double m_scale;         //!< scale of a quad (forced to 1)

  /// @defgroup io-settings
  /// read from the io group of the setting file
  /// @{
  std::string
      m_output_prefix; //!< the output prefix for the filenames (default output)
  std::string m_restart_filename; //!< filename of data from a previous run
                                  //!< (default data.p4est)
  int m_restart_enabled; //!< is this a restart run ? (default 0, options 0 or
                         //!< 1)
  int m_mesh_info; //!< write treeid/level/mpirank to file (default 0, options 0
                   //!< or 1)
  int m_single_precision; //!< write data in single precision (default 1,
                          //!< options 0 or 1)
  int m_save_count;       //!< requested number of outputs (default 1)
  std::vector<std::string>
      m_write_variables;  //!< names of variables to save (default NULL)
  int m_statistics_level; //!< verbose level for the timers (default 0)
  /// @}

  int m_num_write_variables; //!< Number of variables to save

  /// @name nodes info
  /// store information about nodes for writing node data
  /// @{
  p4est_gloidx_t m_global_num_nodes;
  p4est_locidx_t m_local_num_nodes;
  p4est_locidx_t m_start_nodes;
  /// @}

  //////////////////////////////////////////////////////////////////////////////
  /// \brief write h5 file
  ///
  /// qdata_mgr is needed to here to retrieve the quad_get_field method for
  /// outputs.
  ///
  /// @tparam    cons_t    A type containing a set of conservative variables
  /// @param[in] time      Current time of the simulation
  /// @param[in] qdata_mgr Pointer to the Qdata Manager
  //////////////////////////////////////////////////////////////////////////////
  template <typename cons_t>
  void write_h5_file(double time, QdataManager<cons_t> *qdata_mgr);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief write restart file using p4est native format
  //////////////////////////////////////////////////////////////////////////////
  void write_p4est_file();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Open the HDF5 and XMF files for writing.
  ///
  /// Also includes this file inside the main xmf file, if needed.
  //////////////////////////////////////////////////////////////////////////////
  void open(const std::string &basename);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Close the HDF5 and XMF files.
  ///
  /// Also closes the main xmf file, if it was opened.
  //////////////////////////////////////////////////////////////////////////////
  void close();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Free the writer struct.
  ///
  /// This also closes the main xmf file
  //////////////////////////////////////////////////////////////////////////////
  void destroy();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Write the header for the XMF and HDF5 files.
  ///
  /// The header includes the node information, connectivity information and
  /// the treeid, level or mpirank for each quadrant, if required.
  ///
  /// In the case of the XMF file, this will defined the topology and geometry
  /// of the mesh and point to the relevant fields in the HDF5 file.
  //////////////////////////////////////////////////////////////////////////////
  int write_header(double time);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Write a node-centered or cell-centered attribute.
  ///
  /// @param[in] w The writer.
  /// @param[in] name The name of the attribute.
  /// @param[in] data The data to be written.
  /// @param[in] dim In the case of a vector, this is the dimension of each
  /// element in the vector field.
  /// @param[in] ftype The type of the attribute. See supported types in
  ///  the io_attribute_type_t enum.
  /// @param[in] dtype The type of the data we are writing. This is given as a
  /// native HDF5 type. See the types defined in the H5Tpublic.h header.
  /// @param[in] wtype The type of the data written to the file. The
  /// conversion between the data type and the written data is handled
  /// by HDF5.
  //////////////////////////////////////////////////////////////////////////////
  int write_attribute(const std::string &name, void *data, size_t dim,
                      io_attribute_type_t ftype, hid_t dtype, hid_t wtype);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Write a lnode-centered attribute. Only works for degree 1 lnodes.
  ///
  /// @param[in] name The name of the attribute.
  /// @param[in] lnodes The \c p4est_lnodes_t pointer describing the lnodes
  /// @param[in] lnode_data The actual data, in lnode order
  /// @param[in] type_id The HDF5 type id of the data to be written.
  //////////////////////////////////////////////////////////////////////////////
  int write_lnode_attribute(const std::string &name, p4est_lnodes_t *lnodes,
                            double *lnode_data, hid_t type_id);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Write the XMF footer.
  ///
  /// Closes all the XML tags.
  //////////////////////////////////////////////////////////////////////////////
  int write_footer();

private:
  // XMDF utilities.

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Write the header of the main XMF file.
  //////////////////////////////////////////////////////////////////////////////
  void io_xmf_write_main_header();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Write the XMF header information: topology and geometry.
  //////////////////////////////////////////////////////////////////////////////
  void io_xmf_write_header(double time);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Write the include for the current file.
  //////////////////////////////////////////////////////////////////////////////
  void io_xmf_write_main_include(const std::string &name);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Write information about an attribute.
  ///
  /// @param[in] fd   file descriptor for xmf file
  /// @param[in] basename The basename.
  /// @param[in] name The name of the attribute.
  /// @param[in] tyep The type.
  /// @param[in] dims The dimensions of the attribute. If it is a scalar,
  /// dims[1] will be ignored.
  //////////////////////////////////////////////////////////////////////////////
  void io_xmf_write_attribute(const std::string &name,
                              const std::string &number_type,
                              io_attribute_type_t type, hsize_t dims[2]);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Close the remaining tags for the main file.
  //////////////////////////////////////////////////////////////////////////////
  void io_xmf_write_main_footer();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Close all remaining tags.
  ///
  /// \param[in] fd xmff file descriptor
  //////////////////////////////////////////////////////////////////////////////
  void io_xmf_write_footer();

  // HDF5 utilities.

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Write a given dataset into the HDF5 file.
  ///
  /// @param[in] fd An open file descriptor to a HDF5 file.
  /// @param[in] name The name of the dataset we are writing.
  /// @param[in] data The data to write.
  /// @param[in] dtype_id The native HDF5 type of the given data.
  /// @param[in] wtype_id The native HDF5 type of the written data.
  /// @param[in] rank The rank of the dataset. 1 if it is a vector, 2 for a
  /// matrix. \param [in] dims The global dimensions of the dataset. \param [in]
  /// count The local dimensions of the dataset. \param [in] start The offset of
  /// the local data with respect to the global positioning.
  ///
  /// \sa H5TPublic.h
  //////////////////////////////////////////////////////////////////////////////
  void io_hdf_writev(hid_t fd, const std::string &name, void *data,
                     hid_t dtype_id, hid_t wtype_id, hid_t rank, hsize_t dims[],
                     hsize_t count[], hsize_t start[]);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Compute and write the coordinates of all the mesh nodes.
  //////////////////////////////////////////////////////////////////////////////
  void io_hdf_write_coordinates(p4est_nodes_t *nodes);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Compute and write the connectivity information for each quadrant.
  //////////////////////////////////////////////////////////////////////////////
  void io_hdf_write_connectivity(p4est_nodes_t *nodes);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Compute and write the treeid for each quadrant.
  //////////////////////////////////////////////////////////////////////////////
  void io_hdf_write_tree();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Compute and write the level for each quadrant.
  //////////////////////////////////////////////////////////////////////////////
  void io_hdf_write_level();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Compute and write the MPI rank for each quadrant.
  ///
  /// The rank is wrapped with IO_MPIRANK_WRAP.
  //////////////////////////////////////////////////////////////////////////////
  void io_hdf_write_rank();
};

//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
void IoManager::write_h5_file(double time, QdataManager<cons_t> *qdata_mgr) {

  std::ostringstream ss;
  ss << m_output_prefix << "_";
  ss << std::setw(5) << std::setfill('0') << m_times_saved;
  std::string filename = ss.str();

  const p4est_locidx_t Ntotal = m_p4est->local_num_quadrants;

  p4est_locidx_t t = 0;
  int k = 0;
  p4est_tree_t *tree = NULL;
  sc_array_t *quadrants = NULL;
  p4est_quadrant_t *quad = NULL;

  double *data = NULL;

  hid_t output_type =
      (m_single_precision ? H5T_NATIVE_FLOAT : H5T_NATIVE_DOUBLE);

  size_t dim;
  io_attribute_type_t IO_DATA_TYPE;

  // open the new file and write our stuff
  open(filename);
  write_header(time);

  std::vector<double> v;

  // write the scalar variables
  for (int ivar = 0; ivar < m_num_write_variables; ++ivar) {

    dim = 1;
    IO_DATA_TYPE = IO_CELL_SCALAR;

    v = qdata_mgr->quad_get_field(m_write_variables[ivar], nullptr);

    if (v.size() > 1) {

      dim = v.size();
      IO_DATA_TYPE = IO_CELL_VECTOR;
    }

    // allocate the data for the scalars/vectors/
    data = P4EST_ALLOC(double, dim *Ntotal);
    k = 0;

    // get it
    for (t = m_p4est->first_local_tree; t <= m_p4est->last_local_tree; ++t) {
      tree = p4est_tree_array_index(m_p4est->trees, t);
      quadrants = &tree->quadrants;

      for (size_t i = 0; i < quadrants->elem_count; ++i) {
        quad = p4est_quadrant_array_index(quadrants, i);

        v = qdata_mgr->quad_get_field(m_write_variables[ivar], quad);
        for (size_t j = 0; j < dim; ++j) {
          data[k * dim + j] = v.at(j);
        }

        ++k;
      }
    }

    write_attribute(m_write_variables[ivar], data, dim, IO_DATA_TYPE,
                    H5T_NATIVE_DOUBLE, output_type);

    P4EST_FREE(data);
  }

  write_footer();
  close();
};

#endif
