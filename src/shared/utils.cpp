//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "utils.h"

//////////////////////////////////////////////////////////////////////////////
void format_date(std::string &s, std::time_t t) {

  // Format and print the time, "ddd yyyy-mm-dd hh:mm:ss zzz"
  std::tm *tm = std::localtime(&t);

  std::stringstream ss;
  ss << std::put_time(tm, "%Y-%m-%d %H:%M:%S %Z");

  s = ss.str();
}

//////////////////////////////////////////////////////////////////////////////
void print_current_date() {

  // get current time
  std::time_t now = std::time(nullptr);

  std::string tmp;
  format_date(tmp, now);

  const char *cstr = tmp.c_str();

  P4EST_GLOBAL_ESSENTIALF("-- %s\n", cstr);
}

//////////////////////////////////////////////////////////////////////////////
double scalar_gradient(double qi, double qj) {

  double max = fmax(fabs(qi), fabs(qj));

  if (max < 0.001) {
    return 0;
  }

  max = fabs(qi - qj) / max;
  return fmax(fmin(max, 1.0), 0.0);
}
