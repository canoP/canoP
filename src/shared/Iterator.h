//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef ITERATOR_H_
#define ITERATOR_H_

#include "Simulation.h"

//////////////////////////////////////////////////////////////////////////////
/// iterator on volumes to gather statistics
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
static void iterator_get_stat(p4est_iter_volume_info_t *info, void *user_data) {
  UNUSED(user_data);
  UNUSED(info);
}

//////////////////////////////////////////////////////////////////////////////
/// iterator on volumes to copy wnext into w
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
static void iterator_copy(p4est_iter_volume_info_t *info, void *user_data) {
  UNUSED(user_data);

  Simulation<cons_t> *simu = get_simu<cons_t>(info->p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  qdata_mgr->quad_copy_wnext_to_w(info->quad);
  qdata_mgr->quad_wm_wp_delta_zero(info->quad);
};

//////////////////////////////////////////////////////////////////////////////
/// iterator on volumes to apply initial condition
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
static void iterator_init(p4est_iter_volume_info_t *info, void *user_data) {
  UNUSED(user_data);

  Simulation<cons_t> *simu = get_simu<cons_t>(info->p4est);

  simu->m_ic_fn(info->p4est, info->treeid, info->quad);
};

//////////////////////////////////////////////////////////////////////////////
/// iterator on volumes to mark cells to be refined/coarsened
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
static void iterator_mark_adapt(p4est_iter_volume_info_t *info,
                                void *user_data) {

  using qdata_t = to_qdata_t<cons_t>;

  UNUSED(user_data);

  p4est_t *p4est = info->p4est;

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  p4est_wrap_t *wrap = simu->get_wrap();
  p4est_ghost_t *ghost = simu->get_ghost();
  p4est_mesh_t *mesh = simu->get_mesh();

  qdata_t *ghost_qdata = simu->get_ghost_qdata();
  qdata_t *qdata;

  double eps = 0.0;
  double eps_max = -1.0;

  p4est_quadrant_t *neighbor = nullptr;
  qdata_t *ndata = nullptr;

  int face = 0;
  int level = info->quad->level;

  // init face neighbor iterator
  p4est_mesh_face_neighbor_t mfn;
  p4est_mesh_face_neighbor_init2(&mfn, p4est, ghost, mesh, info->treeid,
                                 info->quadid);

  // loop over neighbors
  neighbor = p4est_mesh_face_neighbor_next(&mfn, NULL, NULL, &face, NULL);

  while (neighbor) {

    // boundary
    if (p4est_quadrant_is_equal(neighbor, info->quad)) {

      ndata = (qdata_t *)sc_mempool_alloc(info->p4est->user_data_pool);
      memset(ndata, 0, sizeof(qdata_t));
      simu->m_bc_fn(p4est, info->treeid, info->quad, face, ndata);

    } else {
      ndata = (qdata_t *)p4est_mesh_face_neighbor_data(&mfn, ghost_qdata);
    }

    // compute the indicator
    qdata = qdata_mgr->quad_get_qdata(info->quad);

    eps = simu->m_rc_fn(qdata, ndata);
    eps_max = SC_MAX(eps_max, eps);

    // if the neighbor is on the boundary, the data was allocated on the fly,
    // so we have to deallocate it.
    //
    if (p4est_quadrant_is_equal(neighbor, info->quad)) {
      sc_mempool_free(info->p4est->user_data_pool, ndata);
    }

    neighbor = p4est_mesh_face_neighbor_next(&mfn, NULL, NULL, &face, NULL);

  } // end while. all neighbors visited.

  // we suppose that epsilon_refine >= epsilon_coarsen
  if (level > amr_mgr->m_min_refine && eps_max < amr_mgr->m_epsilon_coarsen) {
    p4est_wrap_mark_coarsen(wrap, info->treeid, info->quadid);
  } else if (level < amr_mgr->m_max_refine &&
             eps_max > amr_mgr->m_epsilon_refine) {
    p4est_wrap_mark_refine(wrap, info->treeid, info->quadid);
  } else {
    return;
  }
};

//////////////////////////////////////////////////////////////////////////////
/// static function to compute gradients
///
/// @param[inout] data[2] pointers to the qdata at each side of the face
/// @param[in]    face[2] face number for each quadrant
/// @param[in]    dl distance between the centres of the quads
/// @param[in]    simu pointer used to retrieve scalar limiter
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
static void compute_delta(to_qdata_t<cons_t> *data[2], int face[2], double dl,
                          Simulation<cons_t> *simu) {

  using prim_t = to_prim_t<cons_t>;

  prim_t dq, ql, qr;

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  int dir = face[CELL_CURRENT] / 2;

  int CELL_LEFT, CELL_RIGHT;

  // get orientation of the face
  if (face[CELL_CURRENT] % 2 == 0) {
    CELL_RIGHT = CELL_CURRENT;
    CELL_LEFT = CELL_NEIGHBOR;
  } else {
    CELL_RIGHT = CELL_NEIGHBOR;
    CELL_LEFT = CELL_CURRENT;
  }

  // compute gradients if second order or diffusion fluxes are activated
  if (simu->m_space_order == 1 && !simu->m_visc_fn && !simu->m_cond_fn &&
      !simu->m_df_fn) {

    memset(&(data[CELL_LEFT]->delta), 0, P4EST_DIM * sizeof(prim_t));
    memset(&(data[CELL_RIGHT]->delta), 0, P4EST_DIM * sizeof(prim_t));

    return;
  }

  qdata_mgr->cons_to_prim(data[CELL_LEFT]->w, ql);
  qdata_mgr->cons_to_prim(data[CELL_RIGHT]->w, qr);

  double *ql_array = reinterpret_cast<double *>(&ql);
  double *qr_array = reinterpret_cast<double *>(&qr);
  double *dq_array = reinterpret_cast<double *>(&dq);

  double *wpr_array = reinterpret_cast<double *>(&(data[CELL_RIGHT]->wp[dir]));
  double *wmr_array = reinterpret_cast<double *>(&(data[CELL_RIGHT]->wm[dir]));
  double *wpl_array = reinterpret_cast<double *>(&(data[CELL_LEFT]->wp[dir]));
  double *wml_array = reinterpret_cast<double *>(&(data[CELL_LEFT]->wm[dir]));

  double *delta_array;

  // compute the gradients of the primitive variables
  for (int ivar = 0; ivar < prim_t::nvar; ++ivar) {
    dq_array[ivar] = (qr_array[ivar] - ql_array[ivar]) / dl;
  }

  // piggy way to test if the cell has already been visited
  if (wml_array[0] < 1 && wpl_array[0] < 1) {

    // the cell has not been visited, the gradient is stored in delta
    memcpy(&(data[CELL_LEFT]->delta[dir]), &dq, sizeof(prim_t));

    wml_array[0] = 2.;

  } else {

    delta_array = reinterpret_cast<double *>(&(data[CELL_LEFT]->delta[dir]));

    // the cell has been visited, we limit the gradients with the scalar limiter
    for (int ivar = 0; ivar < prim_t::nvar; ++ivar) {
      delta_array[ivar] = simu->m_sl_fn(delta_array[ivar], dq_array[ivar]);
    }
  }

  // piggy way to test if the cell has already been visited
  if (wpr_array[0] < 1 && wmr_array[0] < 1) {

    // the cell has not been visited, the gradient is stored in delta
    memcpy(&(data[CELL_RIGHT]->delta[dir]), &dq, sizeof(prim_t));

    wpr_array[0] = 2.;

  } else {

    delta_array = reinterpret_cast<double *>(&(data[CELL_RIGHT]->delta[dir]));

    // the cell has been visited, we limit the gradients with the scalar limiter
    for (int ivar = 0; ivar < prim_t::nvar; ++ivar) {
      delta_array[ivar] = simu->m_sl_fn(delta_array[ivar], dq_array[ivar]);
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
/// iterator on faces to compute delta stored in qdata
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
static void iterator_compute_delta(p4est_iter_face_info_t *info,
                                   void *user_data) {

  using qdata_t = to_qdata_t<cons_t>;

  UNUSED(user_data);

  p4est_t *p4est = info->p4est;

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  qdata_t *ghost_qdata = simu->get_ghost_qdata();

  p4est_iter_face_side_t *side[2];
  sc_array_t *sides = &(info->sides);

  // 2 states: one for each side of the face
  qdata_t *data[2] = {NULL, NULL};

  // the face Id in range 0..P4EST_FACES-1
  int qf[2] = {-1, -1};

  double dx[2], dl;

  int ihang, ifull;

  side[0] = p4est_iter_fside_array_index_int(sides, 0);
  qf[0] = side[0]->face;

  if (sides->elem_count == 2) {

    // both sides of the face exists

    side[1] = p4est_iter_fside_array_index_int(sides, 1);
    qf[1] = side[1]->face;

    if (!side[0]->is_hanging && !side[1]->is_hanging) {

      // both sides are not hanging

      // get qdata from the forest or ghost array
      if (side[0]->is.full.is_ghost) {
        data[0] = &ghost_qdata[side[0]->is.full.quadid];
      } else {
        data[0] = qdata_mgr->quad_get_qdata(side[0]->is.full.quad);
      }

      // get qdata from the forest or ghost array
      if (side[1]->is.full.is_ghost) {
        data[1] = &ghost_qdata[side[1]->is.full.quadid];
      } else {
        data[1] = qdata_mgr->quad_get_qdata(side[1]->is.full.quad);
      }

      // get the distance between cell centres
      dx[0] = amr_mgr->quad_dx(side[0]->is.full.quad);
      dx[1] = amr_mgr->quad_dx(side[1]->is.full.quad);

      dl = 0.5 * (dx[0] + dx[1]);

      // compute gradient
      compute_delta<cons_t>(data, qf, dl, simu);

    } else {

      // one side is hanging, get which one
      if (!side[0]->is_hanging) {
        ifull = 0;
        ihang = 1;
      } else {
        ifull = 1;
        ihang = 0;
      }

      for (int i = 0; i < P4EST_HALF; i++) {

        // get qdata from the forest or ghost array
        if (side[ifull]->is.full.is_ghost) {
          data[ifull] = &ghost_qdata[side[ifull]->is.full.quadid];
        } else {
          data[ifull] = qdata_mgr->quad_get_qdata(side[ifull]->is.full.quad);
        }

        // get qdata from the forest or ghost array
        if (side[ihang]->is.hanging.is_ghost[i]) {
          data[ihang] = &ghost_qdata[side[ihang]->is.hanging.quadid[i]];
        } else {
          data[ihang] =
              qdata_mgr->quad_get_qdata(side[ihang]->is.hanging.quad[i]);
        }

        // get the distance between cell centres
        dx[ifull] = amr_mgr->quad_dx(side[ifull]->is.full.quad);
        dx[ihang] = amr_mgr->quad_dx(side[ihang]->is.hanging.quad[i]);

        dl = 0.5 * (dx[ihang] + dx[ifull]);

        // compute gradient
        compute_delta<cons_t>(data, qf, dl, simu);
      }
    }

  } else {

    // one side does not exist -> apply boundary condition

    data[0] = qdata_mgr->quad_get_qdata(side[0]->is.full.quad);

    // allocate memory to store the boundary qdata
    data[1] = (qdata_t *)sc_mempool_alloc(info->p4est->user_data_pool);
    memset(data[1], 0, sizeof(qdata_t));

    // flip face side with a binary operation 0<->1 2<->3
    qf[1] = qf[0] ^ 1;

    // get qdata from the boundary condition
    simu->m_bc_fn(p4est, side[0]->treeid, side[0]->is.full.quad, qf[0],
                  data[1]);

    // get the distance between cell centres
    dx[0] = amr_mgr->quad_dx(side[0]->is.full.quad);
    dx[1] = dx[0];

    dl = 0.5 * (dx[0] + dx[1]);

    // compute gradient
    compute_delta<cons_t>(data, qf, dl, simu);

    // free the allocated memory
    sc_mempool_free(info->p4est->user_data_pool, data[1]);
  }
};

//////////////////////////////////////////////////////////////////////////////
/// iterator on volumes to update the solution with the source terms
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
static void iterator_update_source(p4est_iter_volume_info_t *info,
                                   void *user_data) {

  using qdata_t = to_qdata_t<cons_t>;
  using param_t = to_param_t<cons_t>;
  using prim_t = to_prim_t<cons_t>;

  UNUSED(user_data);

  // get simu
  Simulation<cons_t> *simu = get_simu<cons_t>(info->p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  double dt = simu->m_dt;

  qdata_t *data = qdata_mgr->quad_get_qdata(info->quad);

  prim_t q, qnext;

  qdata_mgr->cons_to_prim(data->w, q);
  qdata_mgr->cons_to_prim(data->wnext, qnext);

  // apply gravity source term
  if (simu->m_grav_fn) {
    simu->m_grav_fn(q, qnext, &data->wnext, dt, qdata_mgr);
    qdata_mgr->cons_to_prim(data->wnext, qnext);
  }

  // apply user defined source term
  if (simu->m_st_fn) {
    simu->m_st_fn(q, qnext, &data->wnext, dt, qdata_mgr);
    qdata_mgr->cons_to_prim(data->wnext, qnext);
  }

  // apply pressure relaxation source term
  if (simu->m_prelax_fn) {
    simu->m_prelax_fn(q, qnext, &data->wnext, dt, qdata_mgr);
    qdata_mgr->cons_to_prim(data->wnext, qnext);
  }

  // apply velocity relaxation source term
  if (simu->m_vrelax_fn)
    simu->m_vrelax_fn(q, qnext, &data->wnext, dt, qdata_mgr);
}

//////////////////////////////////////////////////////////////////////////////
/// iterator on volumes to compute the time step
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
static void iterator_time_step(p4est_iter_volume_info_t *info,
                               void *user_data) {

  using qdata_t = to_qdata_t<cons_t>;
  using prim_t = to_prim_t<cons_t>;

  Simulation<cons_t> *simu = get_simu<cons_t>(info->p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  double *max_v_over_dx = (double *)user_data;

  qdata_t *data = qdata_mgr->quad_get_qdata(info->quad);
  prim_t pdata;

  double dx;
  double dt_loc = 0.;

  qdata_mgr->cons_to_prim(data->w, pdata);

  dx = amr_mgr->quad_dx(info->quad);

  // get the hydrodynamics timestep
  if (simu->m_hydro_fn) {

    dt_loc = qdata_mgr->get_hydro_invdt(pdata, dx);
    *max_v_over_dx = SC_MAX(*max_v_over_dx, dt_loc);
  }

  // get the conductivity timestep
  if (simu->m_cond_fn) {

    dt_loc = qdata_mgr->get_cond_invdt(pdata, dx);
    *max_v_over_dx = SC_MAX(*max_v_over_dx, dt_loc);
  }

  // get the viscosity timestep
  if (simu->m_visc_fn) {

    dt_loc = qdata_mgr->get_visc_invdt(pdata, dx);
    *max_v_over_dx = SC_MAX(*max_v_over_dx, dt_loc);
  }

  // get the user defined timestep
  if (simu->m_hf_fn || simu->m_df_fn || simu->m_st_fn) {

    dt_loc = qdata_mgr->get_user_invdt(pdata, dx);
    *max_v_over_dx = SC_MAX(*max_v_over_dx, dt_loc);
  }

  // also compute the min and max levels in the mesh
  simu->m_amr_mgr->m_minlevel =
      SC_MIN(simu->m_amr_mgr->m_minlevel, info->quad->level);
  simu->m_amr_mgr->m_maxlevel =
      SC_MAX(simu->m_amr_mgr->m_maxlevel, info->quad->level);
}

//////////////////////////////////////////////////////////////////////////////
/// static function to perform flux update
///
/// @param[inout] data[2] pointers to the qdata at each side of the face
/// @param[in]    face[2] face number for each quadrant
/// @param[in]    dt the time step
/// @param[in]    dS the surface of the face
/// @param[in]    dV[2] the volume of each quadrants
/// @param[in]    simu pointer used to retrieve scalar limiter
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
static void update_flux(to_qdata_t<cons_t> *data[2], int face[2], double dt,
                        double dS, double dV[2], Simulation<cons_t> *simu) {

  using qdata_t = to_qdata_t<cons_t>;
  using param_t = to_param_t<cons_t>;
  using prim_t = to_prim_t<cons_t>;

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  prim_t qleft, qright;
  cons_t uleft, uright;
  array<prim_t> deltaleft, deltaright;
  cons_t fluxleft, fluxright;

  double extra = 0;

  int CELL_LEFT, CELL_RIGHT;
  int dir = face[CELL_CURRENT] / 2;

  double dtdx[2] = {dt * dS / dV[CELL_CURRENT], dt * dS / dV[CELL_NEIGHBOR]};

  double *U_array, *F_array;

  // get orientation of the face
  if (face[CELL_CURRENT] % 2 == 0) {
    CELL_RIGHT = CELL_CURRENT;
    CELL_LEFT = CELL_NEIGHBOR;
  } else {
    CELL_RIGHT = CELL_NEIGHBOR;
    CELL_LEFT = CELL_CURRENT;
  }

  // get corresponding data on the left side
  qleft = data[CELL_LEFT]->wm[dir];
  uleft = data[CELL_LEFT]->w;
  for (int i = 0; i < P4EST_DIM; i++) {
    deltaleft[i] = data[CELL_LEFT]->delta[i];
  }

  // get corresponding data on the right side
  qright = data[CELL_RIGHT]->wp[dir];
  uright = data[CELL_RIGHT]->w;
  for (int i = 0; i < P4EST_DIM; i++) {
    deltaright[i] = data[CELL_RIGHT]->delta[i];
  }

  // swap direction to always compute fluxes in the x direction
  qdata_mgr->swap_dir(dir, qleft, uleft, deltaleft);
  qdata_mgr->swap_dir(dir, qright, uright, deltaright);

  // compute all the fluxes
  if (simu->m_hydro_fn)
    simu->m_hydro_fn(qleft, qright, uleft, uright, &fluxleft, &fluxright,
                     qdata_mgr, &extra);

  if (simu->m_cond_fn)
    simu->m_cond_fn(qleft, qright, deltaleft, deltaright, &fluxleft, &fluxright,
                    qdata_mgr);

  if (simu->m_visc_fn)
    simu->m_visc_fn(qleft, qright, deltaleft, deltaright, &fluxleft, &fluxright,
                    qdata_mgr);

  if (simu->m_df_fn)
    simu->m_df_fn(qleft, qright, deltaleft, deltaright, &fluxleft, &fluxright,
                  qdata_mgr);

  if (simu->m_hf_fn)
    simu->m_hf_fn(qleft, qright, uleft, uright, &fluxleft, &fluxright,
                  qdata_mgr, &extra);

  // swap direction of the fluxes to get the correct orientation
  qdata_mgr->swap_dir(dir, fluxleft);
  qdata_mgr->swap_dir(dir, fluxright);

  U_array = reinterpret_cast<double *>(&(data[CELL_RIGHT]->wnext));
  F_array = reinterpret_cast<double *>(&fluxright);

  // perform update of the right qdata
  for (int ivar = 0; ivar < cons_t::nvar; ++ivar) {
    U_array[ivar] += F_array[ivar] * dtdx[CELL_RIGHT];
  }

  U_array = reinterpret_cast<double *>(&(data[CELL_LEFT]->wnext));
  F_array = reinterpret_cast<double *>(&fluxleft);

  // perform update of the left qdata
  for (int ivar = 0; ivar < cons_t::nvar; ++ivar) {
    U_array[ivar] -= F_array[ivar] * dtdx[CELL_LEFT];
  }
}

//////////////////////////////////////////////////////////////////////////////
/// iterator on faces to perform flux update
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
static void iterator_update_flux(p4est_iter_face_info_t *info,
                                 void *user_data) {

  using qdata_t = to_qdata_t<cons_t>;

  UNUSED(user_data);

  p4est_t *p4est = info->p4est;

  /* get simu */
  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  qdata_t *ghost_data = simu->get_ghost_qdata();

  p4est_iter_face_side_t *side[2];
  sc_array_t *sides = &(info->sides);

  /* 2 states: one for each side of the face */
  qdata_t *data[2] = {NULL, NULL};

  /* the face Id in range 0..P4EST_FACES-1 */
  int qf[2] = {-1, -1};

  double dx[2];
  double dS;
  double dV[2];

  int ihang, ifull;

  side[0] = p4est_iter_fside_array_index_int(sides, 0);
  qf[0] = side[0]->face;

  if (sides->elem_count == 2) {

    // both sides of the face exists

    side[1] = p4est_iter_fside_array_index_int(sides, 1);
    qf[1] = side[1]->face;

    if (!side[0]->is_hanging && !side[1]->is_hanging) {

      // both sides are not hanging

      // get qdata from the forest or ghost array
      if (side[0]->is.full.is_ghost) {
        data[0] = &ghost_data[side[0]->is.full.quadid];
      } else {
        data[0] = qdata_mgr->quad_get_qdata(side[0]->is.full.quad);
      }

      // get qdata from the forest or ghost array
      if (side[1]->is.full.is_ghost) {
        data[1] = &ghost_data[side[1]->is.full.quadid];
      } else {
        data[1] = qdata_mgr->quad_get_qdata(side[1]->is.full.quad);
      }

      // get lengths, surface, and volumes
      dx[0] = amr_mgr->quad_dx(side[0]->is.full.quad);
      dx[1] = amr_mgr->quad_dx(side[1]->is.full.quad);

      dV[0] = quad_dV(dx[0]);
      dV[1] = quad_dV(dx[1]);

      dS = quad_dS(dx[0]);

      // perform flux update
      update_flux<cons_t>(data, qf, simu->m_dt, dS, dV, simu);

    } else {

      // one side is hanging, get which one
      if (!side[0]->is_hanging) {
        ifull = 0;
        ihang = 1;
      } else {
        ifull = 1;
        ihang = 0;
      }

      for (int i = 0; i < P4EST_HALF; i++) {

        // get qdata from the forest or ghost array
        if (side[ifull]->is.full.is_ghost) {
          data[ifull] = &ghost_data[side[ifull]->is.full.quadid];
        } else {
          data[ifull] = qdata_mgr->quad_get_qdata(side[ifull]->is.full.quad);
        }

        // get qdata from the forest or ghost array
        if (side[ihang]->is.hanging.is_ghost[i]) {
          data[ihang] = &ghost_data[side[ihang]->is.hanging.quadid[i]];
        } else {
          data[ihang] =
              qdata_mgr->quad_get_qdata(side[ihang]->is.hanging.quad[i]);
        }

        // get lengths, surface, and volumes
        dx[ifull] = amr_mgr->quad_dx(side[ifull]->is.full.quad);
        dx[ihang] = amr_mgr->quad_dx(side[ihang]->is.hanging.quad[i]);

        dV[ifull] = quad_dV(dx[ifull]);
        dV[ihang] = quad_dV(dx[ihang]);

        dS = quad_dS(dx[ihang]);

        // perform flux update
        update_flux<cons_t>(data, qf, simu->m_dt, dS, dV, simu);
      }
    }

  } else {

    data[0] = qdata_mgr->quad_get_qdata(side[0]->is.full.quad);

    // allocate memory to store the boundary qdata
    data[1] = (qdata_t *)sc_mempool_alloc(info->p4est->user_data_pool);
    memset(data[1], 0, sizeof(qdata_t));

    // flip face side with a binary operation 0<->1 2<->3
    qf[1] = qf[0] ^ 1;

    // get qdata from the boundary condition
    simu->m_bc_fn(p4est, side[0]->treeid, side[0]->is.full.quad, qf[0],
                  data[1]);

    // get lengths, surface, and volumes
    dx[0] = amr_mgr->quad_dx(side[0]->is.full.quad);
    dx[1] = dx[0];

    dV[0] = quad_dV(dx[0]);
    dV[1] = quad_dV(dx[1]);

    dS = quad_dS(dx[0]);

    // perform flux update
    update_flux<cons_t>(data, qf, simu->m_dt, dS, dV, simu);

    // free the allocated memory
    sc_mempool_free(info->p4est->user_data_pool, data[1]);
  }
}

//////////////////////////////////////////////////////////////////////////////
/// iterator on volumes to compute face primitive states wm/wp
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
static void iterator_compute_wm_wp(p4est_iter_volume_info_t *info,
                                   void *user_data) {

  using prim_t = to_prim_t<cons_t>;
  using qdata_t = to_qdata_t<cons_t>;

  UNUSED(user_data);

  Simulation<cons_t> *simu = get_simu<cons_t>(info->p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  qdata_t *data = qdata_mgr->quad_get_qdata(info->quad);

  double dt = simu->m_dt;
  double dx = amr_mgr->quad_dx(info->quad);

  prim_t qc, qf;
  cons_t fm, fp, st, un;

  double *qc_array = reinterpret_cast<double *>(&qc);
  double *qf_array = reinterpret_cast<double *>(&qf);

  double *fm_array = reinterpret_cast<double *>(&fm);
  double *fp_array = reinterpret_cast<double *>(&fp);
  double *st_array = reinterpret_cast<double *>(&st);
  double *un_array = reinterpret_cast<double *>(&un);

  double *delta_array, *wm_array, *wp_array;

  memset(&(data->wm), 0, P4EST_DIM * sizeof(prim_t));
  memset(&(data->wp), 0, P4EST_DIM * sizeof(prim_t));

  // get the primitive variables in the current quadrant
  memcpy(&un, &(data->w), sizeof(cons_t));

  qdata_mgr->cons_to_prim(data->w, qc);

  // if first order in space used constant values
  if (simu->m_space_order == 1)
    dx = 0.;

  // compute predictor only for second order in space and time
  if (simu->m_time_order == 2 && simu->m_space_order == 2) {

    for (int idir = 0; idir < P4EST_DIM; ++idir) {

      // get the gravity source predictor
      if (simu->m_grav_fn)
        qdata_mgr->get_grav_source_predictor(qc, st);

      delta_array = reinterpret_cast<double *>(&(data->delta[idir]));

      // extrapolate on left face
      for (int ivar = 0; ivar < prim_t::nvar; ++ivar) {
        qf_array[ivar] = qc_array[ivar] - 0.5 * dx * delta_array[ivar];
      }

      // get the hydro flux predictor
      if (simu->m_hydro_fn)
        qdata_mgr->get_hydro_flux_predictor(idir, data->w, qf, fp);

      // extrapolate on right face
      for (int ivar = 0; ivar < prim_t::nvar; ++ivar) {
        qf_array[ivar] = qc_array[ivar] + 0.5 * dx * delta_array[ivar];
      }

      // get the hydro flux predictor
      if (simu->m_hydro_fn)
        qdata_mgr->get_hydro_flux_predictor(idir, data->w, qf, fm);

      // second order in time with the predictors at 0.5*dt
      for (int ivar = 0; ivar < cons_t::nvar; ++ivar) {
        un_array[ivar] += (fp_array[ivar] - fm_array[ivar]) * 0.5 * dt / dx;
      }
    }

    for (int ivar = 0; ivar < cons_t::nvar; ++ivar) {
      un_array[ivar] += st_array[ivar] * 0.5 * dt;
    }

    qdata_mgr->cons_to_prim(un, qc);
  }

  for (int idir = 0; idir < P4EST_DIM; ++idir) {

    wm_array = reinterpret_cast<double *>(&(data->wm[idir]));
    wp_array = reinterpret_cast<double *>(&(data->wp[idir]));

    delta_array = reinterpret_cast<double *>(&(data->delta[idir]));

    // update wm
    for (int ivar = 0; ivar < prim_t::nvar; ++ivar) {
      wm_array[ivar] = qc_array[ivar] + 0.5 * dx * delta_array[ivar];
    }

    // update wp
    for (int ivar = 0; ivar < prim_t::nvar; ++ivar) {
      wp_array[ivar] = qc_array[ivar] - 0.5 * dx * delta_array[ivar];
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
/// set_iterator_volume register volume iterators into iv_factory
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t> void Simulation<cons_t>::set_iterator_volume() {

  // register the above callback's
  iv_factory.register_factory("copy", iterator_copy<cons_t>);
  iv_factory.register_factory("mark_adapt", iterator_mark_adapt<cons_t>);
  iv_factory.register_factory("time_step", iterator_time_step<cons_t>);
  iv_factory.register_factory("compute_wm_wp", iterator_compute_wm_wp<cons_t>);
  iv_factory.register_factory("update_source", iterator_update_source<cons_t>);
  iv_factory.register_factory("init", iterator_init<cons_t>);
  iv_factory.register_factory("get_stat", iterator_get_stat<cons_t>);
}

//////////////////////////////////////////////////////////////////////////////
/// set_iterator_face register face iterators into if_factory
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t> void Simulation<cons_t>::set_iterator_face() {

  // register the above callback's
  if_factory.register_factory("update_flux", iterator_update_flux<cons_t>);
  if_factory.register_factory("compute_delta", iterator_compute_delta<cons_t>);
}

#endif
