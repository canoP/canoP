//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef AMR_MANAGER_H_
#define AMR_MANAGER_H_

#include "SettingManager.h"
#include "utils.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief Amr manager to handle amr cycle operations
///
/// A AmrManager object handles all the operations related to adaptive mesh
/// refinement through the p4est library. It relies mostly on p4est wrap (see
/// \a p4est_wrap.h). It also handles p4est connectivity limited to brick (see
/// \a p4est_connectivity.h) in canoP.
//////////////////////////////////////////////////////////////////////////////
class AmrManager {

public:
  //////////////////////////////////////////////////////////////////////////////
  /// We need to pass here qdata size to initialize p4est struct.
  ///
  /// @param[in] stg_mgr a SettingManager object (to parse input setting file)
  /// @param[in] mpicomm MPI communicator
  /// @param[in] qdata_t_sizeof size of qdata_t hold by each p4est quadrant
  //////////////////////////////////////////////////////////////////////////////
  AmrManager(SettingManager *stg_mgr, MPI_Comm mpicomm, int qdata_t_sizeof);
  ~AmrManager();

  SettingManager *m_stg_mgr; //!< pointer to the setting manager
  p4est_wrap_t *m_wrap;      //!< p4est structures wrapper.

  /// @defgroup amr-settings
  /// read from the amr group of the setting file
  /// @{
  double m_ltree; //!< physical size of a p4est tree (default 1.0)

  int m_min_quadrants;     //!< minimal number of quadrants per MPI processor
                           //!< (default 16)
  int m_min_refine;        //!< minimum level of refinement (default 5)
  int m_max_refine;        //!< maximum level of refinement (default 7)
  double m_epsilon_refine; //!< threshold value used in refine callback (default
                           //!< 0.1)
  double m_epsilon_coarsen; //!< threshold value used in coarsen callback
                            //!< (default 0.1)

  int m_ntree_x; //!< number of p4est trees in the x direction (default 1)
  int m_ntree_y; //!< number of p4est trees in the y direction (default 1)
  int m_ntree_z; //!< number of p4est trees in the z direction (default 1)

  int m_periodic_x; //!< periodicity in the x direction (default 0, options 0 or
                    //!< 1)
  int m_periodic_y; //!< periodicity in the y direction (default 0, options 0 or
                    //!< 1)
  int m_periodic_z; //!< periodicity in the z direction (default 0, options 0 or
                    //!< 1)
  /// @}

  int m_minlevel; //!< the min level over MPI in the forest
  int m_maxlevel; //!< the max level over MPI in the forest

  int m_restart_enabled; //!< is this a restart run ? (default 0, options 0 or
                         //!< 1)
  std::string m_restart_filename; //!< filename of data from a previous run
                                  //!< (default data.p4est)

  //////////////////////////////////////////////////////////////////////////////
  /// \brief get the p4est brick connectivity struct used in canoP
  ///
  /// Check \a p4est_connectivity.h for details.
  ///
  /// @returns a p4est connectivity structure
  //////////////////////////////////////////////////////////////////////////////
  p4est_connectivity_t *get_connectivity();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Perform p4est_wrap_adapt on p4est wrap stucture.
  ///
  /// Check \a p4est_wrap.h for details.
  ///
  /// @returns 1 if the forest changed 0 otherwise
  //////////////////////////////////////////////////////////////////////////////
  int adapt();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Perform p4est_wrap_partition on p4est wrap stucture.
  ///
  /// Check \a p4est_wrap.h for details.
  ///
  /// @returns 1 if the forest changed 0 otherwise
  //////////////////////////////////////////////////////////////////////////////
  int partition();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Perform p4est_wrap_complete on p4est wrap stucture.
  ///
  /// Check \a p4est_wrap.h for details.
  //////////////////////////////////////////////////////////////////////////////
  void complete();

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Compute the real length of a quadrant.
  ///
  /// This function assumes the tree in which the quadrant resides is
  /// a square/cube.
  ///
  /// @param [in] quad A p4est quadrant.
  /// @returns The length of the quadrant.
  //////////////////////////////////////////////////////////////////////////////
  double quad_dx(p4est_quadrant_t *quad);

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Compute the coordinates of the center of a quadrant.
  ///
  /// Uses the p4est_qcoord_to_vertex() function to compute the coordinates
  /// of the lower left corner and the quadrant_length() function to compute
  /// the length of the quadrant. The center is then:
  ///          x_c = x_0 + h / 2
  ///
  /// \param [in]  tree         The tree id of the tree quad belongs to.
  /// \param [in]  quad         A p4est quadrant.
  /// \param [out] xyz          The coordinates of the center.
  //////////////////////////////////////////////////////////////////////////////
  void quad_centre(p4est_topidx_t tree, p4est_quadrant_t *quad, double x[3]);
};

//////////////////////////////////////////////////////////////////////////////
/// \brief compute cell face area, handle 2D/3D
///
/// @param[in] dx The length of a quadrant
/// @returns The face area of a quadrant
//////////////////////////////////////////////////////////////////////////////
double quad_dS(double dx);

//////////////////////////////////////////////////////////////////////////////
/// \brief compute cell volume, handle 2D/3D
///
/// @param[in] dx The length of a quadrant
/// @returns The volume of a quadrant
//////////////////////////////////////////////////////////////////////////////
double quad_dV(double dx);

#endif
