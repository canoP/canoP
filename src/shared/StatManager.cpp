//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "StatManager.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief Compute the length of a quadrant from its level.
///
/// NOTE: Copied from p4est quadrant_ext.c
//////////////////////////////////////////////////////////////////////////////
static double quadrant_length_level(int level) {
  return (double)(P4EST_QUADRANT_LEN(level)) / (double)(P4EST_ROOT_LEN);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief In-place reduce on a variable.
//////////////////////////////////////////////////////////////////////////////
static void statistics_mpi_reduce(double *recv, MPI_Datatype type, MPI_Op op,
                                  MPI_Comm comm) {
  int mpiret = 0;
  double send = *recv;

  mpiret = MPI_Reduce(&send, recv, 1, type, op, 0, comm);
  SC_CHECK_MPI(mpiret);
}

//////////////////////////////////////////////////////////////////////////////
StatManager::timer_info_map StatManager::s_timer_info = {
    {"t_total",
     {"t_total", StatManager::timer_accum::none,
      StatManager::timer_accum::sum}},
    {"t_it_min",
     {"t_it_min", StatManager::timer_accum::min,
      StatManager::timer_accum::min}},
    {"t_it_max",
     {"t_it_max", StatManager::timer_accum::max,
      StatManager::timer_accum::max}},
    {"t_it_mean",
     {"t_it_mean", StatManager::timer_accum::mean,
      StatManager::timer_accum::sum}},
    {"t_io_min",
     {"t_io_min", StatManager::timer_accum::min,
      StatManager::timer_accum::min}},
    {"t_io_max",
     {"t_io_max", StatManager::timer_accum::max,
      StatManager::timer_accum::max}},
    {"t_io_mean",
     {"t_io_mean", StatManager::timer_accum::mean,
      StatManager::timer_accum::sum}},
    {"t_adapt_min",
     {"t_adapt_min", StatManager::timer_accum::min,
      StatManager::timer_accum::min}},
    {"t_adapt_max",
     {"t_adapt_max", StatManager::timer_accum::max,
      StatManager::timer_accum::max}},
    {"t_adapt_mean",
     {"t_adapt_mean", StatManager::timer_accum::mean,
      StatManager::timer_accum::sum}},
    {"t_mark",
     {"t_mark", StatManager::timer_accum::mean, StatManager::timer_accum::sum}},
    {"t_change",
     {"t_change", StatManager::timer_accum::mean,
      StatManager::timer_accum::sum}},
    {"t_partition",
     {"t_partition", StatManager::timer_accum::mean,
      StatManager::timer_accum::sum}},
    {"t_refine",
     {"t_refine", StatManager::timer_accum::mean,
      StatManager::timer_accum::sum}},
    {"t_coarsen",
     {"t_coarsen", StatManager::timer_accum::mean,
      StatManager::timer_accum::sum}},
    {"t_balance",
     {"t_balance", StatManager::timer_accum::mean,
      StatManager::timer_accum::sum}},
    {"t_ghost_new",
     {"t_ghost_new", StatManager::timer_accum::mean,
      StatManager::timer_accum::sum}},
    {"t_mesh_new",
     {"t_mesh_new", StatManager::timer_accum::mean,
      StatManager::timer_accum::sum}},
};

//////////////////////////////////////////////////////////////////////////////
bool StatManager::add_timer(const char *name, timer_accum mpi_accum,
                            timer_accum time_accum) {
  P4EST_ASSERT(time_accum != timer_accum::none);

  auto res =
      s_timer_info.emplace(name, timer_info_t(name, mpi_accum, time_accum));
  // res is a std::pair<iterator, bool>
  return res.second;
}

//////////////////////////////////////////////////////////////////////////////
StatManager::StatManager(SettingManager *stg_mgr, MPI_Comm mpicomm) {

  int mpiret;

  m_mpicomm = mpicomm;

  mpiret = MPI_Comm_size(mpicomm, &m_mpisize);
  SC_CHECK_MPI(mpiret);
  mpiret = MPI_Comm_rank(mpicomm, &m_mpirank);
  SC_CHECK_MPI(mpiret);

  m_verboseLevel = stg_mgr->read_int("io.statistics_level", 0);

  m_local_num_quadrants[0] = P4EST_ALLOC(p4est_locidx_t, m_mpisize);
  m_local_num_quadrants[1] = P4EST_ALLOC(p4est_locidx_t, m_mpisize);
  memset(m_local_num_quadrants[0], 0, m_mpisize * sizeof(int));
  memset(m_local_num_quadrants[1], 0, m_mpisize * sizeof(int));
  m_local_num_quadrants[0][m_mpirank] = INT_MAX;
  m_local_num_quadrants[1][m_mpirank] = INT_MIN;

  m_global_num_quadrants[0] = 0;
  m_global_num_quadrants[1] = 0;

  for (auto it = s_timer_info.begin(); it != s_timer_info.end(); ++it) {
    switch (it->second.time_accum) {
    case timer_accum::sum:
    case timer_accum::mean:
      m_timers[it->first] = 0;
      break;
    case timer_accum::min:
      m_timers[it->first] = DBL_MAX;
      break;
    case timer_accum::max:
      m_timers[it->first] = DBL_MIN;
      break;
    default:
      P4EST_ASSERT(0);
      break;
    }
  }

  m_t = 0;
  m_dt[0] = DBL_MAX;
  m_dt[1] = DBL_MIN;
  m_level[0] = INT_MAX;
  m_level[1] = INT_MIN;

  m_norml1 = 0;
  m_norml2 = 0;

  m_start = std::time(nullptr);
  ;
  m_timers["t_total"] = (double)clock();
}

//////////////////////////////////////////////////////////////////////////////
StatManager::~StatManager() {

  P4EST_FREE(m_local_num_quadrants[0]);
  P4EST_FREE(m_local_num_quadrants[1]);
}

int StatManager::get_stat() { return (m_verboseLevel > 0); }

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_dt(double dt) {

  m_dt[0] = SC_MIN(m_dt[0], dt);
  m_dt[1] = SC_MAX(m_dt[1], dt);
}

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_level(int min, int max) {

  m_level[0] = SC_MIN(m_level[0], min);
  m_level[1] = SC_MAX(m_level[1], max);
}

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_num_quadrants(int lnq) {

  int i = m_mpirank;

  m_local_num_quadrants[0][i] = SC_MIN(m_local_num_quadrants[0][i], lnq);
  m_local_num_quadrants[1][i] = SC_MAX(m_local_num_quadrants[1][i], lnq);
}

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_timing_byname(std::string name, double t) {

  switch (s_timer_info[name].time_accum) {
  case timer_accum::sum:
  case timer_accum::mean:
    m_timers[name] += t;
    break;
  case timer_accum::min:
    m_timers[name] = SC_MIN(m_timers[name], t);
    break;
  case timer_accum::max:
    m_timers[name] = SC_MAX(m_timers[name], t);
    break;
  default:
    P4EST_ASSERT(0);
    break;
  }
}

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_timing_scheme(double t) {

  statistics_timing_byname("t_it_min", t);
  statistics_timing_byname("t_it_max", t);
  statistics_timing_byname("t_it_mean", t);
}

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_timing_io(double t) {

  statistics_timing_byname("t_io_min", t);
  statistics_timing_byname("t_io_max", t);
  statistics_timing_byname("t_io_mean", t);
}

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_timing_adapt(double t) {

  statistics_timing_byname("t_adapt_min", t);
  statistics_timing_byname("t_adapt_max", t);
  statistics_timing_byname("t_adapt_mean", t);
}

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_timing_adapt_mark(double t) {

  statistics_timing_byname("t_mark", t);
}

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_timing_adapt_balance(double t, double *stat_time) {

  statistics_timing_byname("t_change", t);
  statistics_timing_byname("t_refine", stat_time[0]);
  statistics_timing_byname("t_coarsen", stat_time[1]);
  statistics_timing_byname("t_balance", stat_time[2]);
  statistics_timing_byname("t_ghost_new", stat_time[3]);
  statistics_timing_byname("t_mesh_new", stat_time[4]);
}

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_timing_adapt_partition(double t) {

  statistics_timing_byname("t_partition", t);
}

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_write(const std::string &filename) {

  // MPI gather/reduce the stats
  statistics_reduce();

  // only the root will write
  if (m_mpirank != 0) {
    return;
  }

  // print summary on stdout
  {
    P4EST_GLOBAL_ESSENTIAL(
        "#######################################################\n");
    printf("Numerical scheme is %10f seconds (%3.1f %%)\n",
           m_timers["t_it_mean"],
           m_timers["t_it_mean"] / m_timers["t_total"] * 100);
    printf("Input/Output     is %10f seconds (%3.1f %%)\n",
           m_timers["t_io_mean"],
           m_timers["t_io_mean"] / m_timers["t_total"] * 100);
    printf("AMR adapt        is %10f seconds (%3.1f %%)\n",
           m_timers["t_adapt_mean"],
           m_timers["t_adapt_mean"] / m_timers["t_total"] * 100);
    P4EST_GLOBAL_ESSENTIAL(
        "#######################################################\n");
  }

  FILE *fd = fopen(filename.c_str(), "a");
  SC_CHECK_ABORTF(fd != nullptr, "Cannot open file \"%s\" for appending.\n",
                  filename.c_str());

  if (m_verboseLevel == 1) {
    // write all the stuff
    fprintf(fd, "---\n");

    std::string sstart, send;
    format_date(sstart, m_start);
    format_date(send, m_end);
    fprintf(fd, "%s: %s\n", "start_date", sstart.c_str());
    fprintf(fd, "%s: %s\n", "end_date", send.c_str());

    fprintf(fd, "%s: %d\n", "mpisize", m_mpisize);
    fprintf(fd, "%s: %d\n", "its", m_iterations);
    fprintf(fd, "%s: %g\n", "t", m_t);

    fprintf(fd, "%s: %g\n", "dtmin", m_dt[0]);
    fprintf(fd, "%s: %g\n", "dtmax", m_dt[1]);
    fprintf(fd, "%s: %d\n", "lmin", m_level[0]);
    fprintf(fd, "%s: %d\n", "lmax", m_level[1]);

    double quad_len_min = quadrant_length_level(m_level[1]);
    double quad_len_max = quadrant_length_level(m_level[0]);
    fprintf(fd, "%s: %f\n", "dxmin", quad_len_min);
    fprintf(fd, "%s: %f\n", "dxmax", quad_len_max);

    fprintf(fd, "%s: %lld\n", "minq", m_global_num_quadrants[0]);
    fprintf(fd, "%s: %lld\n", "maxq", m_global_num_quadrants[1]);

    for (auto it = s_timer_info.begin(); it != s_timer_info.end(); ++it) {
      fprintf(fd, "%s: %g\n", it->second.name, m_timers[it->first]);
    }

    fprintf(fd, "%s: %g\n", "errorL1", m_norml1);
    fprintf(fd, "%s: %g\n", "errorL2", m_norml2);

    /* fprintf (fd, "local_quads:\n"); */
    /* for (int i = 0; i < m_mpisize; ++i) { */
    /*   fprintf (fd, "  - min: %d\n", m_local_num_quadrants[0][i]); */
    /*   fprintf (fd, "    max: %d\n", m_local_num_quadrants[1][i]); */
    /* } */
  } else if (m_verboseLevel == 2) {
    // write all the stuff
    fprintf(fd, "---\n");

    double quad_len_min = quadrant_length_level(m_level[1]);
    double quad_len_max = quadrant_length_level(m_level[0]);
    fprintf(fd, "%s: %f\n", "dxmin", quad_len_min);
    fprintf(fd, "%s: %f\n", "dxmax", quad_len_max);

    fprintf(fd, "%s: %g\n", "errorL1", m_norml1);
    fprintf(fd, "%s: %g\n", "errorL2", m_norml2);
  } else {
    fclose(fd);
    printf("Unknown verboseLevel for StatManager: %d", m_verboseLevel);
    exit(-1);
  }

  fclose(fd);
}

//////////////////////////////////////////////////////////////////////////////
void StatManager::statistics_reduce() {
  int rank = m_mpirank;
  MPI_Comm comm = m_mpicomm;

  // compute all the mins and maxs over all the processes
  for (auto it = s_timer_info.begin(); it != s_timer_info.end(); ++it) {
    switch (it->second.mpi_accum) {
    case timer_accum::sum:
      statistics_mpi_reduce(&(m_timers[it->first]), MPI_DOUBLE, MPI_SUM, comm);
      break;
    case timer_accum::mean:
      statistics_mpi_reduce(&(m_timers[it->first]), MPI_DOUBLE, MPI_SUM, comm);
      m_timers[it->first] /= m_mpisize;
      break;
    case timer_accum::min:
      statistics_mpi_reduce(&(m_timers[it->first]), MPI_DOUBLE, MPI_MIN, comm);
      break;
    case timer_accum::max:
      statistics_mpi_reduce(&(m_timers[it->first]), MPI_DOUBLE, MPI_MAX, comm);
      break;
    default:
      break;
    }
  }

  // compute the global l1 and l2 norms
  statistics_mpi_reduce(&(m_norml1), MPI_DOUBLE, MPI_SUM, comm);
  statistics_mpi_reduce(&(m_norml2), MPI_DOUBLE, MPI_SUM, comm);
  m_norml2 = sqrt(m_norml2);

  // gather all the min / max local number quadrants
  MPI_Gather(&(m_local_num_quadrants[0][rank]), 1, MPI_INT,
             m_local_num_quadrants[0], 1, MPI_INT, 0, comm);
  MPI_Gather(&(m_local_num_quadrants[1][rank]), 1, MPI_INT,
             m_local_num_quadrants[1], 1, MPI_INT, 0, comm);

  // compute the min / max global number of quadrants
  for (int i = 0; i < m_mpisize; ++i) {
    m_global_num_quadrants[0] += m_local_num_quadrants[0][i];
    m_global_num_quadrants[1] += m_local_num_quadrants[1][i];
  }
}
