//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef FACTORY_H_
#define FACTORY_H_

#include "Qdata.h"
#include "QdataManager.h"
#include "utils.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief A prototype class to define a factory of functions
///
/// The main purpose is to return an callback_t, i.e. a function pointer
/// that will be passed to p4est later. The idea here it to define a map
/// between a name and the actual function defined by the function pointer
/// type callback_t.  The callback_map_t is empty, and supposed to be filled
/// in the specialized template.
///
/// tparam qdata_t a type containing quadrant data (can be void for callbacks
/// independent of qdata_t)
/// tparam callback_t a type defining a function pointer
//////////////////////////////////////////////////////////////////////////////
template <typename qdata_t, typename callback_t> class Factory {

public:
  Factory(){};
  ~Factory() = default;

private:
  using callback_map_t = std::map<std::string, callback_t>;

  callback_map_t m_callback_map; //!<  map label/function pointer.

public:
  //////////////////////////////////////////////////////////////////////////////
  /// \brief Insert a function pointer into the map.
  ///
  /// Note that this register function can be used to serve
  /// at least two different purposes:
  /// - in the specialized factory: register existing callback
  /// - in some client code, register a callback from a plugin code, at runtime.
  //////////////////////////////////////////////////////////////////////////////
  void register_factory(const std::string &key, callback_t cb) {
    m_callback_map[key] = cb;
  };

  //////////////////////////////////////////////////////////////////////////////
  /// \brief Retrieve one of the possible function pointer by name.
  ///
  /// Allowed default names are defined in the concrete factory.
  //////////////////////////////////////////////////////////////////////////////
  callback_t callback_byname(const std::string &name) {
    // find the function pointer in the register map
    typename callback_map_t::iterator it = m_callback_map.find(name);

    // if found, just return it
    if (it != m_callback_map.end())
      return it->second;

    // if not found, call SC_ABORT
    P4EST_GLOBAL_LERROR("#### ERROR ####\n");
    P4EST_GLOBAL_LERRORF("%s: is not recognized as a valid key.\n",
                         name.data());
    P4EST_GLOBAL_LERROR("Valid options are:\n");
    for (auto it = m_callback_map.begin(); it != m_callback_map.end(); ++it)
      P4EST_GLOBAL_LERRORF("%s\n", it->first.data());
    P4EST_GLOBAL_LERROR("#### ERROR ####\n");
    SC_ABORT("Error in Factory.h\n");
  };
};

/// callback type for scalar limiters
using sl_callback_t = double (*)(double ul, double ur);

/// callback type for volume iterator callbacks
using iv_callback_t = void (*)(p4est_iter_volume_info_t *info, void *user_data);

/// callback type for volume iterator callbacks
using if_callback_t = void (*)(p4est_iter_face_info_t *info, void *user_data);

/// prototype callback type for refinement criteria
template <typename qdata_t>
using to_rc_callback_t = double (*)(qdata_t *cella, qdata_t *cellb);

/// prototype callback type for boundary conditions
template <typename qdata_t>
using to_bc_callback_t = void (*)(p4est_t *p4est, p4est_topidx_t which_tree,
                                  p4est_quadrant_t *q, int face,
                                  qdata_t *ghost_qdata);

/// prototype callback type for initial conditions
template <typename qdata_t> using to_ic_callback_t = p4est_init_t;

/// prototype callback type for hyperbolic fluxes (non-conservative allowed)
template <typename cons_t>
using to_hf_callback_t = void (*)(to_prim_t<cons_t> &qleft,
                                  to_prim_t<cons_t> &qright, cons_t &uleft,
                                  cons_t &uright, cons_t *fluxleft,
                                  cons_t *fluxright,
                                  QdataManager<cons_t> *qdata_mgr,
                                  double *extra);

/// prototype callback type for diffusion fluxes
template <typename cons_t>
using to_df_callback_t = void (*)(to_prim_t<cons_t> &qleft,
                                  to_prim_t<cons_t> &qright,
                                  array<to_prim_t<cons_t>> &deltaleft,
                                  array<to_prim_t<cons_t>> &deltaright,
                                  cons_t *fluxleft, cons_t *fluxright,
                                  QdataManager<cons_t> *qdata_mgr);

/// prototype callback type for source terms
template <typename cons_t>
using to_st_callback_t = void (*)(to_prim_t<cons_t> &q,
                                  to_prim_t<cons_t> &qnext, cons_t *wnext,
                                  double dt, QdataManager<cons_t> *qdata_mgr);

#ifndef SKIPPED_BY_DOXYGEN

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing scalar limiters
//////////////////////////////////////////////////////////////////////////////
template <> Factory<void, sl_callback_t>::Factory();

#endif

#endif
