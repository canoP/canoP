//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Simulation.h"

//////////////////////////////////////////////////////////////////////////////
SimulationBase::SimulationBase(SettingManager *stg_mgr, MPI_Comm mpicomm,
                               int qdata_t_sizeof)
    : m_stg_mgr(stg_mgr), m_mpicomm(mpicomm) {

  P4EST_GLOBAL_PRODUCTION("-- read simulation settings -- \n");

  m_model = stg_mgr->readv_string("simulation.model", "Unknown");
  m_tcurrent = m_stg_mgr->readv_double("simulation.tcurrent", 0.0);
  m_tmax = m_stg_mgr->readv_double("simulation.tmax", 1.0);
  m_cfl = m_stg_mgr->readv_double("simulation.cfl", 0.9);
  m_space_order = m_stg_mgr->readv_int("simulation.space_order", 1);
  m_time_order = m_stg_mgr->readv_int("simulation.time_order", 1);

  P4EST_GLOBAL_PRODUCTION("\n");

  // create managers
  m_io_mgr = new IoManager(m_stg_mgr, m_mpicomm);

  m_stat_mgr = new StatManager(m_stg_mgr, m_mpicomm);

  m_amr_mgr = new AmrManager(m_stg_mgr, m_mpicomm, qdata_t_sizeof);

  // set p4est and simu pointers in io/amr managers
  set_p4est_io_mgr();
  set_simu_amr_mgr();

  // initialize counters
  m_t = m_tcurrent;
  m_dt = m_tmax;
  m_iteration = 0;
  m_total_num_cell_update = 0;
}

//////////////////////////////////////////////////////////////////////////////
SimulationBase::~SimulationBase() {

  delete m_stat_mgr;

  delete m_io_mgr;

  delete m_amr_mgr;
};

//////////////////////////////////////////////////////////////////////////////
int SimulationBase::restarted() { return m_amr_mgr->m_restart_enabled; };

//////////////////////////////////////////////////////////////////////////////
void SimulationBase::initial_refinement() {

  P4EST_GLOBAL_INFO("initial refinement\n");

  int n = m_amr_mgr->m_max_refine - m_amr_mgr->m_min_refine;

  for (int i = 0; i < n + 1; ++i) {
    printf(" Simulation initial refinement - call %d\n", i);
    adapt(1);
  }

  write_output_file();
}

//////////////////////////////////////////////////////////////////////////////
int SimulationBase::finished() { return m_t >= (m_tmax - 1e-14); }

//////////////////////////////////////////////////////////////////////////////
void SimulationBase::compute_time_step() {
  p4est_t *p4est = get_p4est();
  p4est_ghost_t *ghost = get_ghost();
  MPI_Comm mpicomm = p4est->mpicomm;
  double max_v_over_dx = -1;

  // initialize the min / max current levels
  m_amr_mgr->m_minlevel = P4EST_QMAXLEVEL;
  m_amr_mgr->m_maxlevel = 0;

  // compute the max of the velocities
  // update the min / max current levels
  p4est_iterate(p4est, ghost,
                &max_v_over_dx,                          // user data
                iv_factory.callback_byname("time_step"), // volume_fn
                NULL,                                    // face_fn
#ifdef P4_TO_P8
                NULL, // edge_fn
#endif
                NULL // corner_fn
  );

  // get the global maximum and minimum level
  MPI_Allreduce(MPI_IN_PLACE, &(m_amr_mgr->m_minlevel), 1, MPI_INT, MPI_MIN,
                mpicomm);
  MPI_Allreduce(MPI_IN_PLACE, &(m_amr_mgr->m_maxlevel), 1, MPI_INT, MPI_MAX,
                mpicomm);

  // update the min dt using the given courant number
  if (max_v_over_dx < 0) {
    m_dt = m_tmax;
  } else if (!ISFUZZYNULL(max_v_over_dx)) {
    m_dt = 1.0 / max_v_over_dx * m_cfl;
    m_dt = fmin(m_dt, fabs(m_tmax - m_t));
  } else {
    SC_ABORT("max_v_over_dx is 0\n");
  }

  // take the min over all the processes
  MPI_Allreduce(MPI_IN_PLACE, &(m_dt), 1, MPI_DOUBLE, MPI_MIN, mpicomm);
}

//////////////////////////////////////////////////////////////////////////////
void SimulationBase::next_iteration() {
  p4est_t *p4est = get_p4est();
  p4est_ghost_t *ghost = get_ghost();
  clock_t t_start = 0;
  double time = 0;

  // set size of ghost and fill
  set_ghost_qdata_size(ghost->ghosts.elem_count);
  p4est_ghost_exchange_data(p4est, ghost, get_ghost_qdata_ptr());

  // update the total number of cell visited/update during the run
  m_total_num_cell_update += p4est->global_num_quadrants;

  t_start = (double)clock();

  // compute the cfl condition and time step at the smallest level
  compute_time_step();

  SC_GLOBAL_INFO("Compute gradients (delta) \n");
  p4est_iterate(p4est, ghost,
                NULL,                                        // user data
                NULL,                                        // volume_fn
                if_factory.callback_byname("compute_delta"), // face_fn
#ifdef P4_TO_P8
                NULL, // edge_fn
#endif
                NULL // corner_fn
  );

  SC_GLOBAL_INFO("Compute primitive values at faces (wm, wp)\n");
  p4est_iterate(p4est, ghost,
                NULL,                                        // user data
                iv_factory.callback_byname("compute_wm_wp"), // volume_fn
                NULL,                                        // face_fn
#ifdef P4_TO_P8
                NULL, // edge_fn
#endif
                NULL // corner_fn
  );

  // exchange the data again to get all the face values
  p4est_ghost_exchange_data(p4est, ghost, get_ghost_qdata_ptr());

  SC_GLOBAL_INFO("Perform flux update\n");
  p4est_iterate(p4est, ghost,
                NULL,                                      // user data
                NULL,                                      // volume_fn
                if_factory.callback_byname("update_flux"), // face_fn
#ifdef P4_TO_P8
                NULL, // edge_fn
#endif
                NULL // corner_fn
  );

  // update the values in all the cell with gravity source term
  SC_GLOBAL_INFO("Perform source update\n");
  p4est_iterate(p4est, ghost,
                NULL,                                        // user data
                iv_factory.callback_byname("update_source"), // volume_fn
                NULL,                                        // face_fn
#ifdef P4_TO_P8
                NULL, // edge_fn
#endif
                NULL // corner_fn
  );

  SC_GLOBAL_INFO("Copy new into old\n");
  p4est_iterate(p4est, ghost,
                NULL,                               // user data
                iv_factory.callback_byname("copy"), // volume_fn
                NULL,                               // face_fn
#ifdef P4_TO_P8
                NULL, // edge_fn
#endif
                NULL // corner_fn
  );

  // update statistics
  time = (double)(clock() - t_start) / CLOCKS_PER_SEC;
  if (m_stat_mgr->get_stat()) {
    m_stat_mgr->statistics_timing_scheme(time);
    m_stat_mgr->statistics_dt(m_dt);
    m_stat_mgr->statistics_level(m_amr_mgr->m_minlevel, m_amr_mgr->m_maxlevel);
  }

  // incremenent
  ++m_iteration;
  m_t += m_dt;

  P4EST_GLOBAL_ESSENTIAL(
      "#######################################################\n");
  P4EST_GLOBAL_ESSENTIALF("[%d] t = %-10g\t dt = %.8g\t %s\n", m_iteration, m_t,
                          m_dt, (this->should_write() ? "***" : ""));
  print_current_date();
  P4EST_GLOBAL_ESSENTIAL(
      "#######################################################\n");
}

//////////////////////////////////////////////////////////////////////////////
int SimulationBase::should_adapt() {

  return m_amr_mgr->m_min_refine != m_amr_mgr->m_max_refine;
}

//////////////////////////////////////////////////////////////////////////////
void SimulationBase::adapt(int do_initial_refine) {
  clock_t t_total_start = clock();
  double time = 0.0;
  clock_t t_start = 0;
  double stat_time[5] = {0, 0, 0, 0, 0};

  p4est_t *p4est = get_p4est();
  p4est_ghost_t *ghost = get_ghost();
  p4est_locidx_t lnq = 0;
  int changed = 0;

  set_ghost_qdata_size(ghost->ghosts.elem_count);
  p4est_ghost_exchange_data(p4est, ghost, get_ghost_qdata_ptr());

  // iterate over cells and mark them for refine or coarsen
  t_start = (double)clock();

  if (do_initial_refine) {

    SC_GLOBAL_INFO("Perform initial condition\n");
    p4est_iterate(p4est, ghost,
                  NULL,                               // user data
                  iv_factory.callback_byname("init"), // volume_fn
                  NULL,                               // face_fn
#ifdef P4_TO_P8
                  NULL, // edge_fn
#endif
                  NULL // corner_fn
    );

    set_ghost_qdata_size(ghost->ghosts.elem_count);
    p4est_ghost_exchange_data(p4est, ghost, get_ghost_qdata_ptr());
  }

  SC_GLOBAL_INFO("Mark cells that should be refined/coarsened\n");
  p4est_iterate(p4est, ghost,
                NULL,                                     // user data
                iv_factory.callback_byname("mark_adapt"), // volume_fn
                NULL,                                     // face_fn
#ifdef P4_TO_P8
                NULL, // edge_fn
#endif
                NULL // corner_fn
  );

  // update statistics
  time = (double)(clock() - t_start) / CLOCKS_PER_SEC;
  if (m_stat_mgr->get_stat())
    m_stat_mgr->statistics_timing_adapt_mark(time);

  t_start = (double)clock();

  // adapt the forest
  changed = m_amr_mgr->adapt();

  time = (double)(clock() - t_start) / CLOCKS_PER_SEC;
  if (m_stat_mgr->get_stat())
    m_stat_mgr->statistics_timing_adapt_balance(time, stat_time);

  if (changed) {
    t_start = (double)clock();

    // partition the forest
    changed = m_amr_mgr->partition();

    time = (double)(clock() - t_start) / CLOCKS_PER_SEC;
    if (m_stat_mgr->get_stat())
      m_stat_mgr->statistics_timing_adapt_partition(time);
  }

  // complete the cycle
  if (changed) {
    m_amr_mgr->complete();
  }

  lnq = p4est->local_num_quadrants;

  // update statistics
  time = (double)(clock() - t_total_start) / CLOCKS_PER_SEC;
  if (m_stat_mgr->get_stat()) {
    m_stat_mgr->statistics_timing_adapt(time);
    m_stat_mgr->statistics_num_quadrants(lnq);
  }
}

//////////////////////////////////////////////////////////////////////////////
int SimulationBase::should_write() {
  double interval = m_tmax / m_io_mgr->m_save_count;

  if (m_io_mgr->m_save_count < 0) {
    return 1;
  }

  if ((m_t - (m_io_mgr->m_times_saved - 1) * interval) > interval) {
    return 1;
  }

  /* always write the last time step */
  if (ISFUZZYNULL(m_t - m_tmax)) {
    return 1;
  }

  return 0;
}

//////////////////////////////////////////////////////////////////////////////
void SimulationBase::write_restart_file() { m_io_mgr->write_p4est_file(); }

//////////////////////////////////////////////////////////////////////////////
void SimulationBase::write_statistics() {

  p4est_t *p4est = get_p4est();
  p4est_ghost_t *ghost = get_ghost();
  std::string filename;

  // don't do anything if statistics collecting is disabled
  if (!m_stat_mgr->get_stat()) {
    return;
  }

  // initialize some variables before the loop
  m_amr_mgr->m_minlevel = P4EST_QMAXLEVEL;
  m_amr_mgr->m_maxlevel = 0;

  if (m_stat_mgr->get_stat()) {
    m_stat_mgr->m_norml1 = 0;
    m_stat_mgr->m_norml2 = 0;
  }

  /*
   * This will:
   *  * compute an L^1 norm
   *  * compute an L^2 norm
   *  * update level min / max
   */
  p4est_iterate(p4est, ghost,
                NULL,                                   // user data
                iv_factory.callback_byname("get_stat"), // volume_fn
                NULL,                                   // face_fn
#ifdef P4_TO_P8
                NULL, // edge_fn
#endif
                NULL // corner_fn
  );

  if (p4est->mpirank == 0) {
    std::ostringstream ss;
    ss << m_io_mgr->m_output_prefix << "_";
#ifdef P4_TO_P8
    ss << "stats3d.txt";
#else
    ss << "stats2d.txt";
#endif
    filename = ss.str();
  }

  // update statistics
  if (m_stat_mgr->get_stat()) {
    m_stat_mgr->m_end = std::time(nullptr);
    m_stat_mgr->m_t = m_t;
    m_stat_mgr->m_iterations = m_iteration;
    m_stat_mgr->m_timers["t_total"] =
        ((double)clock() - m_stat_mgr->m_timers["t_total"]) / CLOCKS_PER_SEC;
    m_stat_mgr->statistics_level(m_amr_mgr->m_minlevel, m_amr_mgr->m_maxlevel);

    // write statistics
    m_stat_mgr->statistics_write(filename);
  }
}

//////////////////////////////////////////////////////////////////////////////
/// Factory storing simulation constructors
//////////////////////////////////////////////////////////////////////////////
template <> Factory<void, simu_callback_t>::Factory() {

  // Register available simulation types
#ifdef USE_BIFLUID5EQ
  register_factory("bifluid5eq", &Simulation<bifluid5eq::cons_t>::create);
#endif

#ifdef USE_BIFLUID7EQ
  register_factory("bifluid7eq", &Simulation<bifluid7eq::cons_t>::create);
#endif

#ifdef USE_MONOFLUID
  register_factory("monofluid", &Simulation<monofluid::cons_t>::create);
#endif
}
