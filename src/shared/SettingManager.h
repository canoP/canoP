//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef SETTING_MANAGER_H_
#define SETTING_MANAGER_H_

#include "utils.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief Get the log verbosity by name.
///
/// The log verbosity is defined in \a sc.h and are of the form SC_LP_NAME.
///
/// @param[in] name   The input name
/// @returns The ID of the requested log level for libsc.
//////////////////////////////////////////////////////////////////////////////
int setting_log_verbosity(const char *name);

//////////////////////////////////////////////////////////////////////////////
/// \brief Setting manager to handle input setting file.
///
/// The setting file can contain variables that define a configuration
/// option. For example, if we set up the Lua file to contain:
///
///  settings = {
///      name = "Leroy Jenkins",
///      age = 66
/// }
///
/// we can then read those values by calling:
/// name = stg_mgr->read_string("settings.name")
/// or stg_mgr->read_int("settings.age", &age)
/// or stg_mgr->readv_int("settings.age", &age) for a verbose mode to terminal
//////////////////////////////////////////////////////////////////////////////
class SettingManager {

public:
  //////////////////////////////////////////////////////////////////////////////
  /// \param [in] filename   The setting input file.
  //////////////////////////////////////////////////////////////////////////////
  SettingManager(const char *filename);
  ~SettingManager();

  lua_State *L;       //!< The Lua state
  sc_list_t *strings; //!< store all the strings that have been allocated

  //////////////////////////////////////////////////////////////////////////////
  /// @name read methods
  /// \brief Read from the setting input file. readv/read -> verbose=T/F
  ///
  /// @param[in] key    The name of the field.
  /// @param[in] preset The default to use in case the key does not exist.
  /// @returns Return the read value or the preset value if not read.
  //////////////////////////////////////////////////////////////////////////////
  /// @{
  int read_int(const char *key, int preset);
  int readv_int(const char *key, int preset);
  double read_double(const char *key, double preset);
  double readv_double(const char *key, double preset);
  std::string read_string(const char *key, const char *preset);
  std::string readv_string(const char *key, const char *preset);
  const char *read_char(const char *key, const char *preset);
  const char *readv_char(const char *key, const char *preset);
  /// @}
};

#endif
