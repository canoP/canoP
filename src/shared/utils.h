//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef UTILS_H_
#define UTILS_H_

//////////////////////////////////////////////////////////////////////////////
#include <array>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <variant>
#include <vector>

//////////////////////////////////////////////////////////////////////////////
#include <hdf5.h>
#include <hdf5_hl.h>
#include <mpi.h>

//////////////////////////////////////////////////////////////////////////////
#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_base.h>
#include <p4est_bits.h>
#include <p4est_connectivity.h>
#include <p4est_extended.h>
#include <p4est_geometry.h>
#include <p4est_ghost.h>
#include <p4est_iterate.h>
#include <p4est_lnodes.h>
#include <p4est_mesh.h>
#include <p4est_nodes.h>
#include <p4est_wrap.h>
const static std::string P4EST_FOREST_SUFFIX("p4est");
#define FACE_NEGATIVE_OFFSET 8
#else
#include <p4est_base.h>
#include <p8est_bits.h>
#include <p8est_connectivity.h>
#include <p8est_extended.h>
#include <p8est_geometry.h>
#include <p8est_ghost.h>
#include <p8est_iterate.h>
#include <p8est_lnodes.h>
#include <p8est_mesh.h>
#include <p8est_nodes.h>
#include <p8est_wrap.h>
const static std::string P4EST_FOREST_SUFFIX("p8est");
#define FACE_NEGATIVE_OFFSET 24
#endif

//////////////////////////////////////////////////////////////////////////////
extern "C" {
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
}

//////////////////////////////////////////////////////////////////////////////
/// make the compiler ignore an unused variable
//////////////////////////////////////////////////////////////////////////////
#ifndef UNUSED
#define UNUSED(x) ((void)(x))
#endif

//////////////////////////////////////////////////////////////////////////////
/// make the compiler ignore an unused function
//////////////////////////////////////////////////////////////////////////////
#ifdef __GNUC__
#define UNUSED_FUNCTION __attribute__((unused))
#else
#define UNUSED_FUNCTION
#endif

#define THRESHOLD 1e-12
#define ISFUZZYNULL(a) (fabs(a) < THRESHOLD)
#define FUZZYCOMPARE(a, b)                                                     \
  ((ISFUZZYNULL(a) && ISFUZZYNULL(b)) ||                                       \
   (fabs((a) - (b)) * 1000000000000. <= SC_MIN(fabs(a), fabs(b))))
#define FUZZYLIMITS(x, a, b)                                                   \
  (((x) > ((a)-THRESHOLD)) && ((x) < ((b) + THRESHOLD)))

//////////////////////////////////////////////////////////////////////////////
/// put date into a string
///
/// @param[inout] s output string
/// @param[in]    t time
//////////////////////////////////////////////////////////////////////////////
void format_date(std::string &s, std::time_t t);

//////////////////////////////////////////////////////////////////////////////
/// print date to terminal
//////////////////////////////////////////////////////////////////////////////
void print_current_date();

//////////////////////////////////////////////////////////////////////////////
/// Utility function used in several refine callback's.
///
/// @param[in] qi quantity_i
/// @param[in] qj quantity_j
/// @returns gradient normalized between 0 and 1
//////////////////////////////////////////////////////////////////////////////
double scalar_gradient(double qi, double qj);

/// enum cells
enum cell_t { CELL_CURRENT = 0, CELL_NEIGHBOR = 1 };

/// enum directions
enum dir_t { IX = 0, IY = 1, IZ = 2 };

#endif
