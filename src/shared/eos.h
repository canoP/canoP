//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef EOS_H_
#define EOS_H_

//////////////////////////////////////////////////////////////////////////////
/// \brief Type containing parameters for a Noble-Abel
/// Stiffened-Gas Equation of State
//////////////////////////////////////////////////////////////////////////////
struct eos_t {

  double gamma; //!< adiabatic index (default 1.666)
  double q;     //!< heat bond (default 0.0)
  double b;     //!< covolume (default 0.0)
  double pinf;  //!< parameter for attractive interactions (default 0.0)
  double Cv;    //!< specific heat capacity a constant volume (default 1.0)
  double qp;    //!< parameter to compute specific entropy (default 0.0)
  double mu;    //!< shear viscosity (default 0.0)
  double eta;   //!< bulk viscosity (default 0.0)
  double kappa; //!< thermal conductivity (default 0.0)

  inline eos_t()
      : gamma(5.0 / 3.0), q(0.0), b(0.0), pinf(0.0), Cv(1.0), qp(0.0), mu(0.0),
        eta(0.0), kappa(0.0) {}
};

//////////////////////////////////////////////////////////////////////////////
/// NASG Equation of state for a single phase.
/// Compute pressure p
///
/// @param[in]  rho  density
/// @param[in]  eint internal energy per mass unit
/// @param[in]  eos_t EOS parameters
//////////////////////////////////////////////////////////////////////////////
inline double get_pressure(double rho, double eint, const eos_t &eosFluid) {
  return (eosFluid.gamma - 1.0) * (eint - eosFluid.q) /
             (1.0 / rho - eosFluid.b) -
         eosFluid.gamma * eosFluid.pinf;
}

//////////////////////////////////////////////////////////////////////////////
/// NASG Equation of state for a single phase.
/// Compute temperature T
///
/// @param[in]  rho  density
/// @param[in]  eint internal energy per mass unit
/// @param[in]  eosFluid EOS parameters
//////////////////////////////////////////////////////////////////////////////
inline double get_temperature(double rho, double eint, const eos_t &eosFluid) {
  return ((eint - eosFluid.q) - (1.0 / rho - eosFluid.b) * eosFluid.pinf) /
         eosFluid.Cv;
}

//////////////////////////////////////////////////////////////////////////////
/// NASG Equation of state for a single phase.
/// Compute speed of sound c
///
/// @param[in]  rho  density
/// @param[in]  p pressure
/// @param[in]  eosFluid EOS parameters
//////////////////////////////////////////////////////////////////////////////
inline double get_soundspeed(double rho, double p, const eos_t &eosFluid) {
  return sqrt(eosFluid.gamma / (rho * rho) * (p + eosFluid.pinf) /
              (1.0 / rho - eosFluid.b));
}

//////////////////////////////////////////////////////////////////////////////
/// NASG Equation of state for a single phase.
/// Compute internal energy per unit mass
///
/// @param[in]  rho  density
/// @param[in]  p pressure
/// @param[in]  eosFluid EOS parameters
//////////////////////////////////////////////////////////////////////////////
inline double get_eint(double rho, double p, const eos_t &eosFluid) {
  return (p + eosFluid.gamma * eosFluid.pinf) / (eosFluid.gamma - 1.0) *
             (1.0 / rho - eosFluid.b) +
         eosFluid.q;
}

//////////////////////////////////////////////////////////////////////////////
/// Isobaric pressure from the equilibrium between two phases
/// Compute pressure p
///
/// @param[in]  eint internal energy per mass unit
/// @param[in]  ag gas volume fraction
/// @param[in]  mg gas mass per unit volume
/// @param[in]  ml liquid mass per unit volume
/// @param[in]  eos_g EOS parameters of the gas phase
/// @param[in]  eos_l EOS parameters of the liquid phase
//////////////////////////////////////////////////////////////////////////////
inline double get_isobaric_pressure(double eint, double ag, double mg,
                                    double ml, const eos_t &eos_g,
                                    const eos_t &eos_l) {

  double a0xsi0 = (ag - eos_g.b * mg) / (eos_g.gamma - 1.0);
  double a1xsi1 = (1.0 - ag - eos_l.b * ml) / (eos_l.gamma - 1.0);

  double xsi = a0xsi0 + a1xsi1;
  double rhoq = mg * eos_g.q + ml * eos_l.q;
  double xsiGammaPinf =
      a0xsi0 * eos_g.gamma * eos_g.pinf + a1xsi1 * eos_l.gamma * eos_l.pinf;

  return (((mg + ml) * eint - rhoq) - xsiGammaPinf) / xsi;
}

//////////////////////////////////////////////////////////////////////////////
/// Isobaric sound speed from the equilibrium between two phases
/// Compute speed of sound c
///
/// @param[in]  p pressure
/// @param[in]  ag gas volume fraction
/// @param[in]  mg gas mass per unit volume
/// @param[in]  ml liquid mass per unit volume
/// @param[in]  eos_g EOS parameters of the gas phase
/// @param[in]  eos_l EOS parameters of the liquid phase
//////////////////////////////////////////////////////////////////////////////
inline double get_isobaric_soundspeed(double p, double ag, double mg, double ml,
                                      const eos_t &eos_g, const eos_t &eos_l) {

  double a0xsi0 = (ag - eos_g.b * mg) / (eos_g.gamma - 1.0);
  double a1xsi1 = (1.0 - ag - eos_l.b * ml) / (eos_l.gamma - 1.0);

  double xsi0rho0cs20 = eos_g.gamma * (p + eos_g.pinf) / (eos_g.gamma - 1.0);
  double xsi1rho1cs21 = eos_l.gamma * (p + eos_l.pinf) / (eos_l.gamma - 1.0);

  double xsi = a0xsi0 + a1xsi1;
  double cs2 =
      (ag * xsi0rho0cs20 + (1.0 - ag) * xsi1rho1cs21) / ((mg + ml) * xsi);

  return sqrt(cs2);
}

//////////////////////////////////////////////////////////////////////////////
/// Isobaric internal energy from the equilibrium between two phases
/// Compute internal energy per unit mass
///
/// @param[in]  p pressure
/// @param[in]  ag gas volume fraction
/// @param[in]  mg gas mass per unit volume
/// @param[in]  ml liquid mass per unit volume
/// @param[in]  eos_g EOS parameters of the gas phase
/// @param[in]  eos_l EOS parameters of the liquid phase
//////////////////////////////////////////////////////////////////////////////
inline double get_isobaric_eint(double p, double ag, double mg, double ml,
                                const eos_t &eos_g, const eos_t &eos_l) {

  double a0xsi0 = (ag - eos_g.b * mg) / (eos_g.gamma - 1.0);
  double a1xsi1 = (1.0 - ag - eos_l.b * ml) / (eos_l.gamma - 1.0);

  double xsi = a0xsi0 + a1xsi1;
  double rhoq = mg * eos_g.q + ml * eos_l.q;
  double xsiGammaPinf =
      a0xsi0 * eos_g.gamma * eos_g.pinf + a1xsi1 * eos_l.gamma * eos_l.pinf;

  return (xsi * p + xsiGammaPinf + rhoq) / (mg + ml);
}

//////////////////////////////////////////////////////////////////////////////
/// Isothermic temperature assuming T1=T2 between two phases
/// Compute temperature T
///
/// @param[in]  eint internal energy per mass unit
/// @param[in]  ag gas gas volume fraction
/// @param[in]  mg gas mass per unit volume
/// @param[in]  ml liquid mass per unit volume
/// @param[in]  eos_g EOS parameters of the gas phase
/// @param[in]  eos_l EOS parameters of the liquid phase
//////////////////////////////////////////////////////////////////////////////
inline double get_isothermic_temperature(double eint, double ag, double mg,
                                         double ml, const eos_t &eos_g,
                                         const eos_t &eos_l) {

  double y = mg / (mg + ml);

  double Cv = y * eos_g.Cv + (1.0 - y) * eos_l.Cv;
  double q = y * eos_g.q + (1.0 - y) * eos_l.q;
  double pinf = ag * eos_g.pinf + (1.0 - ag) * eos_l.pinf;
  double bpinf = y * eos_g.b * eos_g.pinf + (1.0 - y) * eos_l.b * eos_l.pinf;

  return (eint - q - pinf / (mg + ml) + bpinf) / Cv;
}

#endif
