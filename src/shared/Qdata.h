//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef QDATA_H_
#define QDATA_H_

#include "utils.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief Generic type containing an ensemble of conservative traits
//////////////////////////////////////////////////////////////////////////////
template <class... ConsTraits> struct generic_data_t : public ConsTraits... {
  static constexpr int nvar = (0 + ... + ConsTraits::nvar);
};

/// Primitive type deduction from a given conservative type
template <class cons> struct to_prim;

/// Primitive type deduction from a given conservative type
template <class cons> using to_prim_t = typename to_prim<cons>::type;

//////////////////////////////////////////////////////////////////////////////
/// \brief Generic primitive type deduction from an ensemble of
/// conservative traits
//////////////////////////////////////////////////////////////////////////////
template <class... ConsTraits> struct to_prim<generic_data_t<ConsTraits...>> {
  using type = generic_data_t<to_prim_t<ConsTraits>...>;
};

/// Parameter type deduction from a given conservative type
template <class cons> struct to_param;

/// Parameter type deduction from a given conservative type
template <class cons> using to_param_t = typename to_param<cons>::type;

//////////////////////////////////////////////////////////////////////////////
/// \brief Generic parameter type deduction from an ensemble of
/// conservative traits
//////////////////////////////////////////////////////////////////////////////
template <class... ConsTraits> struct to_param<generic_data_t<ConsTraits...>> {
  using type = generic_data_t<to_param_t<ConsTraits>...>;
};

/// Type for a 2D/3D array of a given type
template <class T> using array = std::array<T, P4EST_DIM>;

//////////////////////////////////////////////////////////////////////////////
/// \brief Generic qdata type from a generic conservative type
/// containing an ensemble of conservative traits
//////////////////////////////////////////////////////////////////////////////
template <class cons_t> struct to_qdata_t {

  using prim_t = to_prim_t<cons_t>;

  cons_t w;
  cons_t wnext;
  array<prim_t> delta;
  array<prim_t> wm;
  array<prim_t> wp;
};

#endif
