//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef SIMULATION_H_
#define SIMULATION_H_

#include "AmrManager.h"
#include "Factory.h"
#include "IoManager.h"
#include "QdataManager.h"
#include "SettingManager.h"
#include "StatManager.h"
#include "utils.h"

#ifdef USE_BIFLUID5EQ
#include "bifluid5eq/QdataBifluid5eq.h"
#endif

#ifdef USE_MONOFLUID
#include "monofluid/QdataMonofluid.h"
#endif

#ifdef USE_BIFLUID7EQ
#include "bifluid7eq/QdataBifluid7eq.h"
#endif

//////////////////////////////////////////////////////////////////////////////
/// \brief Base class for the creation of a canoP simulation
///
/// Contains the time stepping integration procedure and the p4est_iterate
/// loops to perform the numerical scheme independently of the quadrant
/// data (qdata) in each leaf of the AMR trees.
/// Collect performance (timing) and monitoring (statistics) information.
/// A real simulation should derived from this class, to define the qdata
/// and adapt the time loop and p4est_iterate loops if needed.
//////////////////////////////////////////////////////////////////////////////
class SimulationBase {

public:
  // Constructor and destructor
  //////////////////////////////////////////////////////////////////////////////
  /// We need to pass here qdata size, because p4est is initialized in amr_mgr
  /// constructor the actual qdata_t is a template parameter only known in
  /// derived concrete simulation class.
  ///
  /// @param[in] stg_mgr a SettingManager object (to parse input setting file)
  /// @param[in] mpicomm MPI communicator
  /// @param[in] qdata_t_sizeof size of qdata_t hold by each p4est quadrant
  //////////////////////////////////////////////////////////////////////////////
  SimulationBase(SettingManager *stg_mgr, MPI_Comm mpicomm, int qdata_t_sizeof);
  virtual ~SimulationBase();

  MPI_Comm m_mpicomm; //!< MPI communicator

  // Managers
  SettingManager *m_stg_mgr; //!< setting manager to handle input setting files
  IoManager *m_io_mgr;       //!< io manager to handle data input/output
  AmrManager *m_amr_mgr;     //!< amr manager to handle amr cycle operations
  StatManager *m_stat_mgr;   //!< stat manager to gather performance statistics

  /// @defgroup simulation-settings
  /// read from the simulation group of the setting file
  /// @{
  std::string m_model; //!< model (default unknown, options monofluid,
                       //!< bifluid5eq or bifluid7eq)

  double m_tcurrent; //!< user defined start time (default 0.0)
  double m_tmax;     //!< user defined maximum time (default 1.0)
  double m_cfl; //!< user defined Courant–Friedrichs–Lewy number (default 0.8)

  int m_space_order; //!< space order of the scheme (MUSCL, default 1, options 1
                     //!< or 2)
  int m_time_order; //!< time order of the scheme (Hancock, default 1, options 1
                    //!< or 2)
  /// @}

  // iteration info
  double m_t;                             //!< the time at the current iteration
  double m_dt;                            //! the time step at the current level
  int m_iteration;                        //!< the current iteration
  p4est_gloidx_t m_total_num_cell_update; //!< nb of cell update

  /// Factory containing callback operations for p4est_iterate on cell volumes
  Factory<void, iv_callback_t> iv_factory;

  /// Factory containing callback operations for p4est_iterate on cell faces
  Factory<void, if_callback_t> if_factory;

  /// @name getters
  /// getters to get needed stg/p4est objects and ghost_qdata
  /// @{
  SettingManager *get_stg_mgr() { return m_stg_mgr; };
  AmrManager *get_amr_mgr() { return m_amr_mgr; };
  p4est_t *get_p4est() { return m_amr_mgr->m_wrap->p4est; };
  p4est_ghost_t *get_ghost() { return m_amr_mgr->m_wrap->ghost; };
  p4est_mesh_t *get_mesh() { return m_amr_mgr->m_wrap->mesh; };
  p4est_wrap_t *get_wrap() { return m_amr_mgr->m_wrap; };
  virtual void *get_ghost_qdata_ptr() = 0;
  /// @}

  /// @name setters
  /// setters to set pointers to p4est/simu ans size of ghost_qdata
  /// @{
  void set_p4est_io_mgr() { m_io_mgr->m_p4est = m_amr_mgr->m_wrap->p4est; }
  void set_simu_amr_mgr() { m_amr_mgr->m_wrap->user_pointer = (void *)this; }
  virtual void set_ghost_qdata_size(int size) = 0;
  /// @}

  /// @name Time loop operations
  /// They may be overriden in a derived simulation class
  int restarted();           //!< Check if the simulation has been restarted
  void initial_refinement(); //!< Perform initial refinement
  int finished();            //!< Check if current time is larger than end time
  void compute_time_step();  //!< Compute the allowed time step
  void next_iteration();     //!< Perform the scheme with p4est_iterate loops
  int should_adapt();        //!< Provides a strategy to decide when to adapt
  void adapt(int initial_refine); //!< Perform amr cycle operations
  int should_write();        //!< Decides if the simulation should dump data
  void write_restart_file(); //!< Write restart file using internal p4est format
  void write_statistics();   //!< collect and write statistics if enabled
  virtual void write_output_file() = 0; //!< dump solution to file (need qdata)
  /// @}
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Prototype derived class for the creation of a canoP simulation.
///
/// This derived class is a prototype for a set of conservative variables.
/// Primitive variables prim_t, the quadrant data qdata_t and the parameters
/// param_t are deduced from cons_t (see Qdata.h). This class contains
/// operations that requires the existence of a qdata. The template parameter
/// cons_t will be specialized as a function of the simulation type.
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t> class Simulation : public SimulationBase {

  using prim_t = to_prim_t<cons_t>;
  using qdata_t = to_qdata_t<cons_t>;
  using param_t = to_param_t<cons_t>;

  using ic_callback_t = to_ic_callback_t<qdata_t>;
  using bc_callback_t = to_bc_callback_t<qdata_t>;
  using rc_callback_t = to_rc_callback_t<qdata_t>;

  using hf_callback_t = to_hf_callback_t<cons_t>;
  using st_callback_t = to_st_callback_t<cons_t>;
  using df_callback_t = to_df_callback_t<cons_t>;

public:
  //////////////////////////////////////////////////////////////////////////////
  /// The constructor checks if cons_t contains only double to allow for loops
  /// on variables inside cons_t.
  ///
  /// @tparam cons_t a type with a set of conservative variables
  /// @param[in] stg_mgr a SettingManager object (to parse input setting file)
  /// @param[in] mpicomm MPI communicator
  //////////////////////////////////////////////////////////////////////////////
  Simulation(SettingManager *stg_mgr, MPI_Comm mpicomm);
  ~Simulation();

  QdataManager<cons_t> *m_qdata_mgr; //!< qdata manager to handle operations \
				     on data stored in a p4est quadrant

  std::vector<qdata_t> m_ghost_qdata; //!< ghost qdata for MPI partition

  /// @name factories
  /// factories storing the different callbackes
  /// @{
  Factory<qdata_t, hf_callback_t> m_hf_factory;
  Factory<qdata_t, df_callback_t> m_df_factory;
  Factory<qdata_t, st_callback_t> m_st_factory;

  Factory<qdata_t, ic_callback_t> m_ic_factory;
  Factory<qdata_t, bc_callback_t> m_bc_factory;
  Factory<qdata_t, rc_callback_t> m_rc_factory;
  Factory<void, sl_callback_t> m_sl_factory;
  /// @}

  /// @defgroup callback-settings
  /// read from the callback group of the setting file
  /// @{
  std::string
      m_initial_condition; //!< initial condition callback (default none)
  std::string
      m_boundary_condition; //!< boundary condition callback (default none)
  std::string m_refine_condition; //!< refine condition callback (default none)
  std::string m_scalar_limiter;   //!< scalar limiter callback (default minmod)

  std::string m_hyperbolic_flux; //!< user-defined hyperbolic flux callback
                                 //!< (default none)
  std::string
      m_diffusion_flux; //!< user-defined diffusion flux callback (default none)
  std::string
      m_source_term; //!< user-defined source term callback (default none)

  std::string
      m_hydrodynamics_flux; //!< hydrodynamics flux callback (default none)
  std::string
      m_conductivity_flux;      //!< conductivity flux callback (default none)
  std::string m_viscosity_flux; //!< viscosity flux callback (default none)
  std::string m_gravity_term;   //!< gravity source term callback (default none)
  std::string m_pressure_relaxation_term; //!< pressure relaxation callback (for
                                          //!< bifluid7eq, default none)
  std::string m_velocity_relaxation_term; //!< velocity relaxation callback (for
                                          //!< bifluid7eq, default none)
  /// @}

  /// @name callback-functions
  /// callback functions that are chosen from settings inside factories
  /// @{
  ic_callback_t m_ic_fn; //!< initial condition callback
  bc_callback_t m_bc_fn; //!< boundary condition callback
  rc_callback_t m_rc_fn; //!< refine condition callback
  sl_callback_t m_sl_fn; //!< scalar limiter callback

  hf_callback_t m_hf_fn; //!< user hyperbolic flux callback
  df_callback_t m_df_fn; //!< user diffusion flux callback
  st_callback_t m_st_fn; //!< user source term callback

  hf_callback_t m_hydro_fn;  //!< hydrodynamics flux callback
  df_callback_t m_cond_fn;   //!< conductivity flux callback
  df_callback_t m_visc_fn;   //!< viscosity flux callback
  st_callback_t m_grav_fn;   //!< gravity source term callback
  st_callback_t m_prelax_fn; //!< pressure relaxation callback (for bifluid7eq)
  st_callback_t m_vrelax_fn; //!< velocity relaxation callback (for bifluid7eq)
  /// @}

  /// @name getters
  /// getters to get needed objects from qdata manager/ghost
  /// @{
  QdataManager<cons_t> *get_qdata_mgr() { return m_qdata_mgr; };
  void *get_ghost_qdata_ptr() { return &(m_ghost_qdata[0]); };
  qdata_t *get_ghost_qdata() { return &(m_ghost_qdata[0]); };
  /// @}

  /// @name setters
  /// setters to set replace_fn, callbacks and iterators
  /// @{
  void set_replace_fn_amr_mgr() {
    m_amr_mgr->m_wrap->replace_fn = m_qdata_mgr->quad_replace_fn;
  };
  void set_ghost_qdata_size(int size) { m_ghost_qdata.resize(size); };
  void set_iterator_volume();
  void set_iterator_face();
  void set_callback();
  /// @}

  void write_output_file();

  void register_user_callback(){}; //!< user callbacks defined in a	\
				   specific app

  //////////////////////////////////////////////////////////////////////////////
  /// Static wrapper around the constructor to provide a factory with the
  /// different simulation type constructors
  ///
  /// @tparam cons_t a type with a set of conservative variables
  /// @param[in] stg_mgr a SettingManager object (to parse input setting file)
  /// @param[in] mpicomm MPI communicator
  //////////////////////////////////////////////////////////////////////////////
  static SimulationBase *create(SettingManager *stg_mgr, MPI_Comm mpicomm) {
    Simulation<cons_t> *simu = new Simulation<cons_t>(stg_mgr, mpicomm);

    return simu;
  };
};

//////////////////////////////////////////////////////////////////////////////
template <typename cons_t>
Simulation<cons_t>::Simulation(SettingManager *stg_mgr, MPI_Comm mpicomm)
    : SimulationBase(stg_mgr, mpicomm, sizeof(to_qdata_t<cons_t>)) {

  // check that sizeof(cons_t)=cons_t::nvar*sizeof(double) to allow loops on
  // individual fields inside cons_t
  if (!(sizeof(cons_t) == sizeof(double) * cons_t::nvar)) {

    P4EST_GLOBAL_LERROR("#### ERROR ####\n");
    P4EST_GLOBAL_LERROR(
        "sizeof(cons_t)/sizeof(double) is not equal to cons_t::nvar \n");
    P4EST_GLOBAL_LERROR(
        "structs composing cons_t should only contain double \n");
    P4EST_GLOBAL_LERROR(
        "the number of doubles should be given in a static constexpr nvar \n");
    P4EST_GLOBAL_LERRORF("cons_t::nvar is %d \n", cons_t::nvar);
    P4EST_GLOBAL_LERRORF("sizeof(cons_t)/sizeof(double) is %lu \n",
                         sizeof(cons_t) / sizeof(double));
    P4EST_GLOBAL_LERROR("#### ERROR ####\n");
    SC_ABORT("Error in Factory.h\n");
  }

  // check that sizeof(prim_t)=prim_t::nvar*sizeof(double) to allow loops on
  // individual fields inside prim_t
  if (!(sizeof(prim_t) == sizeof(double) * prim_t::nvar)) {

    P4EST_GLOBAL_LERROR("#### ERROR ####\n");
    P4EST_GLOBAL_LERROR(
        "sizeof(prim_t)/sizeof(double) is not equal to prim_t::nvar \n");
    P4EST_GLOBAL_LERROR(
        "structs composing prim_t should only contain double \n");
    P4EST_GLOBAL_LERROR(
        "the number of doubles should be given in a static constexpr nvar \n");
    P4EST_GLOBAL_LERRORF("prim_t::nvar is %d \n", prim_t::nvar);
    P4EST_GLOBAL_LERRORF("sizeof(prim_t)/sizeof(double) is %lu \n",
                         sizeof(prim_t) / sizeof(double));
    P4EST_GLOBAL_LERROR("#### ERROR ####\n");
    SC_ABORT("Error in Factory.h\n");
  }

  m_qdata_mgr = new QdataManager<cons_t>(m_stg_mgr);

  set_replace_fn_amr_mgr();
  set_iterator_volume();
  set_iterator_face();
  set_callback();
};

//////////////////////////////////////////////////////////////////////////////
template <typename cons_t> Simulation<cons_t>::~Simulation() {

  delete m_qdata_mgr;
};

//////////////////////////////////////////////////////////////////////////////
template <typename cons_t> void Simulation<cons_t>::set_callback() {

  register_user_callback();

  P4EST_GLOBAL_PRODUCTION("-- read callback settings -- \n");

  m_initial_condition =
      m_stg_mgr->readv_string("callback.initial_condition", "none");
  m_boundary_condition =
      m_stg_mgr->readv_string("callback.boundary_condition", "none");
  m_refine_condition =
      m_stg_mgr->readv_string("callback.refine_condition", "none");
  m_scalar_limiter =
      m_stg_mgr->readv_string("callback.scalar_limiter", "minmod");

  m_hydrodynamics_flux =
      m_stg_mgr->readv_string("callback.hydrodynamics_flux", "none");
  m_conductivity_flux =
      m_stg_mgr->readv_string("callback.conductivity_flux", "none");
  m_viscosity_flux = m_stg_mgr->readv_string("callback.viscosity_flux", "none");
  m_gravity_term = m_stg_mgr->readv_string("callback.gravity_term", "none");
  m_pressure_relaxation_term =
      m_stg_mgr->readv_string("callback.pressure_relaxation_term", "none");
  m_velocity_relaxation_term =
      m_stg_mgr->readv_string("callback.velocity_relaxation_term", "none");

  m_hyperbolic_flux =
      m_stg_mgr->readv_string("callback.hyperbolic_flux", "none");
  m_diffusion_flux = m_stg_mgr->readv_string("callback.diffusion_flux", "none");
  m_source_term = m_stg_mgr->readv_string("callback.source_term", "none");

  P4EST_GLOBAL_PRODUCTION("\n");

  // select and store the callback functions
  m_ic_fn = m_ic_factory.callback_byname(m_initial_condition);
  m_bc_fn = m_bc_factory.callback_byname(m_boundary_condition);
  m_rc_fn = m_rc_factory.callback_byname(m_refine_condition);
  m_sl_fn = m_sl_factory.callback_byname(m_scalar_limiter);

  m_hydro_fn = m_hf_factory.callback_byname(m_hydrodynamics_flux);
  m_cond_fn = m_df_factory.callback_byname(m_conductivity_flux);
  m_visc_fn = m_df_factory.callback_byname(m_viscosity_flux);
  m_grav_fn = m_st_factory.callback_byname(m_gravity_term);
  m_prelax_fn = m_st_factory.callback_byname(m_pressure_relaxation_term);
  m_vrelax_fn = m_st_factory.callback_byname(m_velocity_relaxation_term);

  m_hf_fn = m_hf_factory.callback_byname(m_hyperbolic_flux);
  m_df_fn = m_df_factory.callback_byname(m_diffusion_flux);
  m_st_fn = m_st_factory.callback_byname(m_source_term);
};

//////////////////////////////////////////////////////////////////////////////
template <typename cons_t> void Simulation<cons_t>::write_output_file() {
  clock_t t_start = clock();
  double time = 0;

  m_io_mgr->write_h5_file<cons_t>(m_t, m_qdata_mgr);

  // update the statistics
  time = (double)(clock() - t_start) / CLOCKS_PER_SEC;
  if (m_stat_mgr->get_stat())
    m_stat_mgr->statistics_timing_io(time);

  // increment output file number
  ++m_io_mgr->m_times_saved;
}

//////////////////////////////////////////////////////////////////////////////
/// Static constructor signature for simulation factory
//////////////////////////////////////////////////////////////////////////////
using simu_callback_t = SimulationBase *(*)(SettingManager *stg_mgr,
                                            MPI_Comm mpicomm);

#ifndef SKIPPED_BY_DOXYGEN

//////////////////////////////////////////////////////////////////////////////
/// Factory storing simulation constructors
//////////////////////////////////////////////////////////////////////////////
template <> Factory<void, simu_callback_t>::Factory();

#endif

//////////////////////////////////////////////////////////////////////////////
/// Generic getter to obtain a pointer to the simulation from p4est
///
/// @param[in] p4est
//////////////////////////////////////////////////////////////////////////////
template <typename cons_t> Simulation<cons_t> *get_simu(p4est_t *p4est) {
  p4est_wrap_t *pp = (p4est_wrap_t *)p4est->user_pointer;

  return static_cast<Simulation<cons_t> *>(pp->user_pointer);
};

#endif
