//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Simulation.h"

int main(int argc, char **argv) {
  MPI_Comm mpicomm;
  int mpisize, mpirank, mpiret;
  int log_verbosity;
  SimulationBase *simu;
  SettingManager *stg_mgr;
  clock_t t_start = 0;

  // initialize MPI
  mpiret = MPI_Init(&argc, &argv);
  SC_CHECK_MPI(mpiret);
  mpicomm = MPI_COMM_WORLD;
  mpiret = MPI_Comm_size(mpicomm, &mpisize);
  SC_CHECK_MPI(mpiret);
  mpiret = MPI_Comm_rank(mpicomm, &mpirank);
  SC_CHECK_MPI(mpiret);

  // exit if no settings are provided
  if (argc < 2) {
    if (mpirank == 0) {
      printf("usage: %s SETTING_FILE LOG_VERBOSITY\n", argv[0]);
    }
    MPI_Finalize();
    exit(0);
  }

  // get the verbosity for the logs e.g. argv[2]=DEBUG, check SettingManager.cpp
  log_verbosity = setting_log_verbosity(argv[2]);

  int SC_CATCH_SIGNALS = 1;
  int SC_PRINT_BACKTRACE = 1;

  // init p4est and libsc
  sc_init(mpicomm, SC_CATCH_SIGNALS, SC_PRINT_BACKTRACE, NULL, log_verbosity);
  p4est_init(NULL, log_verbosity);

  // init setting manager
  stg_mgr = new SettingManager(argv[1]);

  // retrieve the simulation type name from settings
  P4EST_GLOBAL_PRODUCTION("\n");
  const std::string simu_model =
      stg_mgr->read_string("simulation.model", "unknown");
  P4EST_GLOBAL_PRODUCTION("\n");

  // create simulation from the simulation factory
  Factory<void, simu_callback_t> simu_factory;
  simu_callback_t create_simulation = simu_factory.callback_byname(simu_model);

  simu = create_simulation(stg_mgr, mpicomm);

  t_start = (double)clock();

  // initial refinement on the initial condition
  if (!simu->restarted()) {
    simu->initial_refinement();
  }

  // perform the time loop
  while (!simu->finished()) {
    simu->next_iteration();

    if (simu->should_adapt()) {
      simu->adapt(0);
    }

    if (simu->should_write()) {
      simu->write_output_file();
    }
  }

  // Display some minimalist statistics on screen
  double total_time = (double)(clock() - t_start) / CLOCKS_PER_SEC;
  double nb_cell_update_per_second =
      1.0 * simu->m_total_num_cell_update / total_time;
  P4EST_GLOBAL_ESSENTIAL(
      "#######################################################\n");
  P4EST_GLOBAL_ESSENTIALF(
      "Number of cell-updates per seconds: %f Mupdate-cell/s\n",
      nb_cell_update_per_second / 1e6);

  // End of simu run, write a restart file
  simu->write_restart_file();

  // Write detailed statistics to file
  simu->write_statistics();

  // destroy our data structures
  delete simu;
  delete stg_mgr;

  // clean up and exit
  sc_finalize();

  mpiret = MPI_Finalize();
  SC_CHECK_MPI(mpiret);

  return 0;
}
