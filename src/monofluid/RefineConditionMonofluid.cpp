//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataMonofluid.h"

using cons_t = monofluid::cons_t;
using prim_t = monofluid::prim_t;
using param_t = monofluid::param_t;
using qdata_t = monofluid::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// \brief m gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_monofluid_m_gradient(qdata_t *cella, qdata_t *cellb) {

  double m[2] = {cella->w.m, cellb->w.m};

  return scalar_gradient(m[0], m[1]);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief mV gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_monofluid_mV_gradient(qdata_t *cella, qdata_t *cellb) {

  double mVx[2] = {cella->w.mV[IX], cellb->w.mV[IX]};

  double mVx_grad = scalar_gradient(mVx[0], mVx[1]);

  double mVy[2] = {cella->w.mV[IY], cellb->w.mV[IY]};

  double mVy_grad = scalar_gradient(mVy[0], mVy[1]);

  double grad = fmax(mVx_grad, mVy_grad);

#ifdef USE_3D
  double mVz[2] = {cella->w.mV[IZ], cellb->w.mV[IZ]};

  double mVz_grad = scalar_gradient(mVz[0], mVz[1]);

  grad = fmax(grad, mVz_grad);
#endif

  return grad;
}

//////////////////////////////////////////////////////////////////////////////
/// \brief mE gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_monofluid_mE_gradient(qdata_t *cella, qdata_t *cellb) {

  double mE[2] = {cella->w.mE, cellb->w.mE};

  return scalar_gradient(mE[0], mE[1]);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief any gradient refine condition
//////////////////////////////////////////////////////////////////////////////
double rc_monofluid_any_gradient(qdata_t *cella, qdata_t *cellb) {

  double mVx[2] = {cella->w.mV[IX], cellb->w.mV[IX]};

  double mVx_grad = scalar_gradient(mVx[0], mVx[1]);

  double mVy[2] = {cella->w.mV[IY], cellb->w.mV[IY]};

  double mVy_grad = scalar_gradient(mVy[0], mVy[1]);

  double grad = fmax(mVx_grad, mVy_grad);

#ifdef USE_3D
  double mVz[2] = {cella->w.mV[IZ], cellb->w.mV[IZ]};

  double mVz_grad = scalar_gradient(mVz[0], mVz[1]);

  grad = fmax(grad, mVz_grad);
#endif

  double mE[2] = {cella->w.mE, cellb->w.mE};

  double mE_grad = scalar_gradient(mE[0], mE[1]);

  grad = fmax(grad, mE_grad);

  double m[2] = {cella->w.m, cellb->w.m};

  double m_grad = scalar_gradient(m[0], m[1]);

  grad = fmax(grad, m_grad);

  return grad;
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing refine conditions for monofluid
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_rc_callback_t<qdata_t>>::Factory() {

  // register the above callback's
  register_factory("m_gradient", rc_monofluid_m_gradient);
  register_factory("mV_gradient", rc_monofluid_mV_gradient);
  register_factory("mE_gradient", rc_monofluid_mE_gradient);
  register_factory("any_gradient", rc_monofluid_any_gradient);
}
