//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "QdataManager.h"
#include "QdataMonofluid.h"

using cons_t = monofluid::cons_t;

/// @name specialization for monofluid
/// @{
//////////////////////////////////////////////////////////////////////////////
template <> QdataManager<cons_t>::QdataManager(SettingManager *stg_mgr) {

  P4EST_GLOBAL_PRODUCTION("-- read param settings -- \n");

  // Initialize gravity parameters.
  m_param.gravity_x = stg_mgr->readv_double("param.gravity_x", 0.0);
  m_param.gravity_y = stg_mgr->readv_double("param.gravity_y", 0.0);
  m_param.gravity_z = stg_mgr->readv_double("param.gravity_z", 0.0);

  // Initialize eos parameters.
  m_param.eosFluid.gamma = stg_mgr->readv_double("param.eosFluid.gamma", 1.666);
  m_param.eosFluid.Cv = stg_mgr->readv_double("param.eosFluid.Cv", 1.0);
  m_param.eosFluid.q = stg_mgr->readv_double("param.eosFluid.q", 0.0);
  m_param.eosFluid.b = stg_mgr->readv_double("param.eosFluid.b", 0.0);
  m_param.eosFluid.qp = stg_mgr->readv_double("param.eosFluid.qp", 0.0);
  m_param.eosFluid.pinf = stg_mgr->readv_double("param.eosFluid.pinf", 0.0);
  m_param.eosFluid.mu = stg_mgr->readv_double("param.eosFluid.mu", 0.0);
  m_param.eosFluid.eta = stg_mgr->readv_double("param.eosFluid.eta", 0.0);
  m_param.eosFluid.kappa = stg_mgr->readv_double("param.eosFluid.kappa", 0.0);

  // for all-regime scheme
  m_param.lowmach_correction_enabled =
      stg_mgr->readv_int("param.lowmach_correction_enabled", 0);
  m_param.K = stg_mgr->readv_double("param.K", 1.1);

  P4EST_GLOBAL_PRODUCTION("\n");
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::cons_to_prim(const cons_t &w, prim_t &r) {

  // kinetic energy per mass unit
  double ekin = 0.0;

  r.m = w.m;
  for (int i = 0; i < P4EST_DIM; ++i) {
    r.V[i] = w.mV[i] / w.m;
    ekin += r.V[i] * r.V[i];
  }
  ekin *= 0.5;

  // internal energy = total energy - kinetic energy (per mass unit)
  double eint = w.mE / w.m - ekin;

  // use equation of state to compute pressure and local speed of sound
  r.P = get_pressure(r.m, eint, m_param.eosFluid);
  r.T = get_temperature(r.m, eint, m_param.eosFluid);
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::prim_to_cons(const prim_t &pdata, cons_t &cdata) {
  double ekin = 0.0;

  cdata.m = pdata.m;

  for (int i = 0; i < P4EST_DIM; ++i) {
    cdata.mV[i] = pdata.V[i] * pdata.m;
    ekin += 0.5 * pdata.V[i] * pdata.V[i];
  }

  double eint = get_eint(pdata.m, pdata.P, m_param.eosFluid);

  cdata.mE = pdata.m * (eint + ekin);
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::get_hydro_flux_predictor(int idir, const cons_t &cc,
                                                    const prim_t &pf,
                                                    cons_t &flux) {
  UNUSED(cc);

  cons_t cf;

  prim_to_cons(pf, cf);

  // advection term
  flux.m = cf.m * pf.V[idir];

  for (int i = 0; i < P4EST_DIM; ++i) {
    flux.mV[i] = cf.mV[i] * pf.V[idir];
  }
  flux.mE = cf.mE * pf.V[idir];

  // pressure term
  flux.mV[idir] += pf.P;
  flux.mE += pf.P * pf.V[idir];
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::get_grav_source_predictor(const prim_t &pdata,
                                                     cons_t &source) {

  source.mV[IX] = pdata.m * m_param.gravity_x;
  source.mV[IY] = pdata.m * m_param.gravity_y;

  source.mE = pdata.m * pdata.V[IX] * m_param.gravity_x;
  source.mE += pdata.m * pdata.V[IY] * m_param.gravity_y;

#if USE_3D
  source.mV[IZ] = pdata.m * m_param.gravity_z;

  source.mE += pdata.m * pdata.V[IZ] * m_param.gravity_z;
#endif
}

//////////////////////////////////////////////////////////////////////////////
template <>
std::vector<double>
QdataManager<cons_t>::quad_get_field(const std::string &varName,
                                     p4est_quadrant_t *q) {

  qdata_t *qdata;
  cons_t w;

  if (q != nullptr) {

    qdata = quad_get_qdata(q);
    w = qdata->w;
  }

  std::vector<double> field;

  if (!varName.compare("m")) {
    field.push_back(w.m);
  } else if (!varName.compare("mE")) {
    field.push_back(w.mE);
  } else if (!varName.compare("P")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    field.push_back(pdata.P);
  } else if (!varName.compare("T")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    field.push_back(pdata.T);
  } else if (!varName.compare("mV")) {
    for (int i = 0; i < P4EST_DIM; i++)
      field.push_back(w.mV[i]);
  } else if (!varName.compare("V")) {
    prim_t pdata;
    cons_to_prim(w, pdata);
    for (int i = 0; i < P4EST_DIM; i++)
      field.push_back(pdata.V[i]);
  } else {
    // print warning
    P4EST_GLOBAL_INFOF("Unrecognized field: \"%s\"\n", varName.c_str());
  }

  return field;
}

//////////////////////////////////////////////////////////////////////////////
template <>
void QdataManager<cons_t>::swap_dir(int idir, prim_t &pdata, cons_t &cdata,
                                    array<prim_t> &delta) {

  std::swap(pdata.V[IX], pdata.V[idir]);
  std::swap(cdata.mV[IX], cdata.mV[idir]);

  for (int d = 0; d < P4EST_DIM; ++d) {
    std::swap(delta[d].V[IX], delta[d].V[idir]);
  }

  prim_swap(delta[IX], delta[idir]);
}

//////////////////////////////////////////////////////////////////////////////
template <> void QdataManager<cons_t>::swap_dir(int idir, cons_t &cdata) {

  std::swap(cdata.mV[IX], cdata.mV[idir]);
}

//////////////////////////////////////////////////////////////////////////////
template <>
double QdataManager<cons_t>::get_hydro_invdt(const prim_t &pdata,
                                             const double dx) {
  double V = 0.0;
  double c = 0.0;

  c = get_soundspeed(pdata.m, pdata.P, m_param.eosFluid);

  for (int d = 0; d < P4EST_DIM; ++d) {
    V += c + fabs(pdata.V[d]);
  }

  return V / dx;
}

//////////////////////////////////////////////////////////////////////////////
template <>
double QdataManager<cons_t>::get_cond_invdt(const prim_t &pdata,
                                            const double dx) {
  return m_param.eosFluid.kappa / (pdata.m * m_param.eosFluid.Cv * dx * dx);
}

//////////////////////////////////////////////////////////////////////////////
template <>
double QdataManager<cons_t>::get_visc_invdt(const prim_t &pdata,
                                            const double dx) {
  return m_param.eosFluid.mu / (pdata.m * dx * dx);
}
/// @}
