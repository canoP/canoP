//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataMonofluid.h"

using cons_t = monofluid::cons_t;
using prim_t = monofluid::prim_t;
using param_t = monofluid::param_t;
using qdata_t = monofluid::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// HLLC Conservative flux
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void hyperbolic_flux_hllc(prim_t &ql, prim_t &qr, cons_t &ucl, cons_t &ucr,
                          cons_t *fluxl, cons_t *fluxr,
                          QdataManager<cons_t> *qdata_mgr, double *extra) {

  UNUSED(ucl), UNUSED(ucr), UNUSED(extra);

  param_t *param = qdata_mgr->get_param();

  // Left variables
  double rl = ql.m;
  double pl = ql.P;
  double ul = ql.V[IX];

  double ecinl = 0.5 * rl * ul * ul;
  ecinl += 0.5 * rl * ql.V[IY] * ql.V[IY];
#ifdef USE_3D
  ecinl += 0.5 * rl * ql.V[IZ] * ql.V[IZ];
#endif

  double etotl = rl * get_eint(rl, pl, param->eosFluid) + ecinl;
  double ptotl = pl;

  // Right variables
  double rr = qr.m;
  double pr = qr.P;
  double ur = qr.V[IX];

  double ecinr = 0.5 * rr * ur * ur;
  ecinr += 0.5 * rr * qr.V[IY] * qr.V[IY];
#ifdef USE_3D
  ecinr += 0.5 * rr * qr.V[IZ] * qr.V[IZ];
#endif

  double etotr = rr * get_eint(rr, pr, param->eosFluid) + ecinr;
  double ptotr = pr;

  // Find the largest eigenvalues in the normal direction to the interface
  double cfastl = get_soundspeed(rl, pl, param->eosFluid);
  double cfastr = get_soundspeed(rr, pr, param->eosFluid);

  // Compute HLL wave speed
  double SL = fmin(ul, ur) - fmax(cfastl, cfastr);
  double SR = fmax(ul, ur) + fmax(cfastl, cfastr);

  // Compute lagrangian sound speed
  double rcl = rl * (ul - SL);
  double rcr = rr * (SR - ur);

  // Compute acoustic star state
  double ustar = (rcr * ur + rcl * ul + (ptotl - ptotr)) / (rcr + rcl);
  double ptotstar =
      (rcr * ptotl + rcl * ptotr + rcl * rcr * (ul - ur)) / (rcr + rcl);

  // L star region variables
  double rstarl = rl * (SL - ul) / (SL - ustar);
  double etotstarl =
      ((SL - ul) * etotl - ptotl * ul + ptotstar * ustar) / (SL - ustar);

  // R star region variables
  double rstarr = rr * (SR - ur) / (SR - ustar);
  double etotstarr =
      ((SR - ur) * etotr - ptotr * ur + ptotstar * ustar) / (SR - ustar);

  // Sample the solution at x/t=0
  double ro, uo, ptoto, etoto;
  if (SL > 0.0) {
    ro = rl;
    uo = ul;
    ptoto = ptotl;
    etoto = etotl;
  } else if (ustar > 0.0) {
    ro = rstarl;
    uo = ustar;
    ptoto = ptotstar;
    etoto = etotstarl;
  } else if (SR > 0.0) {
    ro = rstarr;
    uo = ustar;
    ptoto = ptotstar;
    etoto = etotstarr;
  } else {
    ro = rr;
    uo = ur;
    ptoto = ptotr;
    etoto = etotr;
  }

  // Compute the Godunov flux (in conservative variables)
  // conservative flux fluxl=fluxr
  fluxl->m += ro * uo;
  fluxr->m += ro * uo;

  fluxl->mV[IX] += ro * uo * uo + ptoto;
  fluxr->mV[IX] += ro * uo * uo + ptoto;

  fluxl->mE += (etoto + ptoto) * uo;
  fluxr->mE += (etoto + ptoto) * uo;

  if (uo > 0.0) {
    fluxl->mV[IY] += ro * uo * ql.V[IY];
    fluxr->mV[IY] += ro * uo * ql.V[IY];
  } else {
    fluxl->mV[IY] += ro * uo * qr.V[IY];
    fluxr->mV[IY] += ro * uo * qr.V[IY];
  }

#ifdef USE_3D
  if (uo > 0.0) {
    fluxl->mV[IZ] += ro * uo * ql.V[IZ];
    fluxr->mV[IZ] += ro * uo * ql.V[IZ];
  } else {
    fluxl->mV[IZ] += ro * uo * qr.V[IZ];
    fluxr->mV[IZ] += ro * uo * qr.V[IZ];
  }
#endif
}

//////////////////////////////////////////////////////////////////////////////
/// Allregime Conservative flux
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void hyperbolic_flux_allregime(prim_t &ql, prim_t &qr, cons_t &ucl, cons_t &ucr,
                               cons_t *fluxl, cons_t *fluxr,
                               QdataManager<cons_t> *qdata_mgr, double *extra) {
  UNUSED(ucl), UNUSED(ucr), UNUSED(extra);

  param_t *param = qdata_mgr->get_param();

  // Left variables
  double rl = ql.m;
  double pl = ql.P;
  double ul = ql.V[IX];
  double al = rl * get_soundspeed(rl, pl, param->eosFluid);

  // Right variables
  double rr = qr.m;
  double pr = qr.P;
  double ur = qr.V[IX];
  double ar = rr * get_soundspeed(rr, pr, param->eosFluid);

  // compute the acoustic impedance at interface
  double aface = param->K * fmax(al, ar);

  // Compute ustar
  double ustar = 0.5 * (ul + ur) - 0.5 * (pr - pl) / aface;

  // compute the asymptotic correction
  double theta = 1.;
  if (param->lowmach_correction_enabled) {
    theta = fmin(abs(ustar) / fmax(al / rl, ar / rr), 1.);
  }

  // compute pistar
  double pstar = 0.5 * (pl + pr) - 0.5 * (ur - ul) * aface * theta;

  cons_t Uuw;
  // upwind conservative variables
  if (ustar > 0) {
    Uuw.m = rl;
    Uuw.mV[IX] = rl * ql.V[IX];
    Uuw.mV[IY] = rl * ql.V[IY];

    double ekinl = 0.5 * rl * ul * ul;
    ekinl += 0.5 * rl * ql.V[IY] * ql.V[IY];
#ifdef USE_3D
    Uuw.mV[IZ] = rl * ql.V[IZ];
    ekinl += 0.5 * rl * ql.V[IZ] * ql.V[IZ];
#endif

    Uuw.mE = rl * get_eint(rl, pl, param->eosFluid) + ekinl;
  } else {
    Uuw.m = rr;
    Uuw.mV[IX] = rr * qr.V[IX];
    Uuw.mV[IY] = rr * qr.V[IY];

    double ekinr = 0.5 * rr * ur * ur;
    ekinr += 0.5 * rr * qr.V[IY] * qr.V[IY];
#ifdef USE_3D
    Uuw.mV[IZ] = rr * qr.V[IZ];
    ekinr += 0.5 * rr * qr.V[IZ] * qr.V[IZ];
#endif

    Uuw.mE = rr * get_eint(rr, pr, param->eosFluid) + ekinr;
  }

  fluxl->m += Uuw.m * ustar;
  fluxr->m += Uuw.m * ustar;

  fluxl->mV[IX] += Uuw.mV[IX] * ustar + pstar;
  fluxr->mV[IX] += Uuw.mV[IX] * ustar + pstar;

  fluxl->mV[IY] += Uuw.mV[IY] * ustar;
  fluxr->mV[IY] += Uuw.mV[IY] * ustar;
#ifdef USE_3D
  fluxl->mV[IZ] += Uuw.mV[IZ] * ustar;
  fluxr->mV[IZ] += Uuw.mV[IZ] * ustar;
#endif
  fluxl->mE += Uuw.mE * ustar + pstar * ustar;
  fluxr->mE += Uuw.mE * ustar + pstar * ustar;
}

//////////////////////////////////////////////////////////////////////////////
/// HLL Conservative flux
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void hyperbolic_flux_hll(prim_t &ql, prim_t &qr, cons_t &ucl, cons_t &ucr,
                         cons_t *fluxl, cons_t *fluxr,
                         QdataManager<cons_t> *qdata_mgr, double *extra) {
  UNUSED(ucl), UNUSED(ucr), UNUSED(extra);

  param_t *param = qdata_mgr->get_param();

  // 1D HLL Conservative flux

  // constants
  // const double smallp = param.smallc*param.smallc/param.eosFluid.gamma;
  const double entho = 1.0 / (param->eosFluid.gamma - 1.0);

  // Maximum wave speed
  double rl = ql.m;
  double pl = ql.P;
  double vl = ql.V[IX];

  double rr = qr.m;
  double pr = qr.P;
  double vr = qr.V[IX];

  double cl = sqrt(param->eosFluid.gamma * pl / rl);
  double cr = sqrt(param->eosFluid.gamma * pr / rr);

  // Compute HLL wave speed
  double SL = fmin(fmin(vl, vr) - fmax(cl, cr), 0.0);
  double SR = fmax(fmax(vl, vr) + fmax(cl, cr), 0.0);

  // Compute conservative variables
  prim_t ul, ur;
  ul.m = ql.m;
  ur.m = qr.m;
  ul.P = ql.P * entho + 0.5 * ql.m * ql.V[IX] * ql.V[IX];
  ur.P = qr.P * entho + 0.5 * qr.m * qr.V[IX] * qr.V[IX];
  ul.P += 0.5 * ql.m * ql.V[IY] * ql.V[IY];
  ur.P += 0.5 * qr.m * qr.V[IY] * qr.V[IY];
#ifdef USE_3D
  ul.P += 0.5 * ql.m * ql.V[IZ] * ql.V[IZ];
  ur.P += 0.5 * qr.m * qr.V[IZ] * qr.V[IZ];
#endif
  ul.V[IX] = ql.m * ql.V[IX];
  ur.V[IX] = qr.m * qr.V[IX];

  // Other advected quantities
  ul.V[IY] = ql.m * ql.V[IY];
  ur.V[IY] = qr.m * qr.V[IY];
#ifdef USE_3D
  ul.V[IZ] = ql.m * ql.V[IZ];
  ur.V[IZ] = qr.m * qr.V[IZ];
#endif

  // Compute l and r fluxes
  prim_t fl, fr;
  fl.m = ul.V[IX];
  fr.m = ur.V[IX];
  fl.P = ql.V[IX] * (ul.P + ql.P);
  fr.P = qr.V[IX] * (ur.P + qr.P);
  fl.V[IX] = ul.V[IX] * ql.V[IX] + ql.P;
  fr.V[IX] = ur.V[IX] * qr.V[IX] + qr.P;

  // Other advected quantities
  fl.V[IY] = fl.m * ql.V[IY];
  fr.V[IY] = fr.m * qr.V[IY];
#ifdef USE_3D
  fl.V[IZ] = fl.m * ql.V[IZ];
  fr.V[IZ] = fr.m * qr.V[IZ];
#endif

  // Compute HLL fluxes
  fluxl->m += (SR * fl.m - SL * fr.m + SR * SL * (ur.m - ul.m)) / (SR - SL);
  fluxr->m += (SR * fl.m - SL * fr.m + SR * SL * (ur.m - ul.m)) / (SR - SL);

  fluxl->mE += (SR * fl.P - SL * fr.P + SR * SL * (ur.P - ul.P)) / (SR - SL);
  fluxr->mE += (SR * fl.P - SL * fr.P + SR * SL * (ur.P - ul.P)) / (SR - SL);

  fluxl->mV[IX] +=
      (SR * fl.V[IX] - SL * fr.V[IX] + SR * SL * (ur.V[IX] - ul.V[IX])) /
      (SR - SL);
  fluxr->mV[IX] +=
      (SR * fl.V[IX] - SL * fr.V[IX] + SR * SL * (ur.V[IX] - ul.V[IX])) /
      (SR - SL);

  fluxl->mV[IY] +=
      (SR * fl.V[IY] - SL * fr.V[IY] + SR * SL * (ur.V[IY] - ul.V[IY])) /
      (SR - SL);
  fluxr->mV[IY] +=
      (SR * fl.V[IY] - SL * fr.V[IY] + SR * SL * (ur.V[IY] - ul.V[IY])) /
      (SR - SL);

#ifdef USE_3D
  fluxl->mV[IZ] +=
      (SR * fl.V[IZ] - SL * fr.V[IZ] + SR * SL * (ur.V[IZ] - ul.V[IZ])) /
      (SR - SL);
  fluxr->mV[IZ] +=
      (SR * fl.V[IZ] - SL * fr.V[IZ] + SR * SL * (ur.V[IZ] - ul.V[IZ])) /
      (SR - SL);
#endif
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing hyperbolic fluxes for monofluid
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_hf_callback_t<cons_t>>::Factory() {

  // register the above callback's
  register_factory("none", nullptr);
  register_factory("hll_enabled", hyperbolic_flux_hll);
  register_factory("hllc_enabled", hyperbolic_flux_hllc);
  register_factory("allregime_enabled", hyperbolic_flux_allregime);
}
