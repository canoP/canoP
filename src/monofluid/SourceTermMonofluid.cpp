//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataMonofluid.h"

using cons_t = monofluid::cons_t;
using prim_t = monofluid::prim_t;
using param_t = monofluid::param_t;
using qdata_t = monofluid::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// Gravity source term
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void source_term_gravity(prim_t &q, prim_t &qnext, cons_t *wnext, double dt,
                         QdataManager<cons_t> *qdata_mgr) {
  UNUSED(qnext);

  param_t *param = qdata_mgr->get_param();

  wnext->mV[IX] += param->gravity_x * q.m * dt;
  wnext->mV[IY] += param->gravity_y * q.m * dt;
#ifdef USE_3D
  wnext->mV[IZ] += param->gravity_z * q.m * dt;
#endif /* USE_3D */

  wnext->mE += param->gravity_x * q.m * q.V[IX] * dt;
  wnext->mE += param->gravity_y * q.m * q.V[IY] * dt;
#ifdef USE_3D
  wnext->mE += param->gravity_z * q.m * q.V[IZ] * dt;
#endif /* USE_3D */
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing source terms for monofluid
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_st_callback_t<cons_t>>::Factory() {

  // register the above callback's
  register_factory("none", nullptr);
  register_factory("gravity_enabled", source_term_gravity);
}
