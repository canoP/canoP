//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataMonofluid.h"

using cons_t = monofluid::cons_t;
using prim_t = monofluid::prim_t;
using param_t = monofluid::param_t;
using qdata_t = monofluid::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// Viscosity diffusion flux
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void diffusion_flux_viscosity(prim_t &ql, prim_t &qr, array<prim_t> &deltal,
                              array<prim_t> &deltar, cons_t *fluxl,
                              cons_t *fluxr, QdataManager<cons_t> *qdata_mgr) {

  param_t *param = qdata_mgr->get_param();

  double mu = param->eosFluid.mu;
  double eta = param->eosFluid.eta;

  double uface = 0.5 * (ql.V[IX] + qr.V[IX]);
  double vface = 0.5 * (ql.V[IY] + qr.V[IY]);

  double du_dx = 0.5 * (deltal[IX].V[IX] + deltar[IX].V[IX]);
  double dv_dy = 0.5 * (deltal[IY].V[IY] + deltar[IY].V[IY]);
  double du_dy = 0.5 * (deltal[IY].V[IX] + deltar[IY].V[IX]);
  double dv_dx = 0.5 * (deltal[IX].V[IY] + deltar[IX].V[IY]);

#ifdef USE_3D
  double wface = 0.5 * (ql.V[IZ] + qr.V[IZ]);

  double du_dz = 0.5 * (deltal[IZ].V[IX] + deltar[IZ].V[IX]);
  double dw_dx = 0.5 * (deltal[IX].V[IZ] + deltar[IX].V[IZ]);
  double dw_dz = 0.5 * (deltal[IZ].V[IZ] + deltar[IZ].V[IZ]);
#endif

  double tau_xx = 2. * mu * du_dx + eta * (du_dx + dv_dy);
  double tau_xy = mu * (du_dy + dv_dx);

#ifdef USE_3D
  tau_xx += eta * dw_dz;
  double tau_xz = mu * (du_dz + dw_dx);
#endif

  fluxl->mV[IX] -= tau_xx;
  fluxl->mV[IY] -= tau_xy;
  fluxl->mE -= tau_xx * uface + tau_xy * vface;

#ifdef USE_3D
  fluxl->mV[IZ] -= tau_xz;
  fluxl->mE -= tau_xz * wface;
#endif

  fluxr->mV[IX] -= tau_xx;
  fluxr->mV[IY] -= tau_xy;
  fluxr->mE -= tau_xx * uface + tau_xy * vface;

#ifdef USE_3D
  fluxr->mV[IZ] -= tau_xz;
  fluxr->mE -= tau_xz * wface;
#endif
}

//////////////////////////////////////////////////////////////////////////////
/// Conductivity diffusion flux
///
/// see Factory.h for the description of the callback signature
//////////////////////////////////////////////////////////////////////////////
void diffusion_flux_conductivity(prim_t &ql, prim_t &qr, array<prim_t> &deltal,
                                 array<prim_t> &deltar, cons_t *fluxl,
                                 cons_t *fluxr,
                                 QdataManager<cons_t> *qdata_mgr) {
  UNUSED(ql);
  UNUSED(qr);

  param_t *param = qdata_mgr->get_param();

  double kappa = param->eosFluid.kappa;
  double dT_dx = 0.5 * (deltal[IX].T + deltar[IX].T);

  fluxl->mE -= kappa * dT_dx;
  fluxr->mE -= kappa * dT_dx;
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing diffusion fluxes for monofluid
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_df_callback_t<cons_t>>::Factory() {

  // register the above callback's
  register_factory("none", nullptr);
  register_factory("viscosity_enabled", diffusion_flux_viscosity);
  register_factory("conductivity_enabled", diffusion_flux_conductivity);
}
