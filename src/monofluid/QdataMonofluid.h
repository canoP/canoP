//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#ifndef QDATA_MONOFLUID_H_
#define QDATA_MONOFLUID_H_

#include "Factory.h"
#include "Qdata.h"
#include "eos.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief Type containing conservative variables for a monofluid model
//////////////////////////////////////////////////////////////////////////////
struct monofluid_cons_t {
  static constexpr unsigned nvar = 2 + P4EST_DIM;

  double m;         //!< mass per unit volume
  double mE;        //!< total energy per unit volume
  array<double> mV; //!< 2D/3D momentum per unit volume

  inline monofluid_cons_t() : m(0.0), mE(0.0) { mV.fill(0.0); }
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Type containing primitive variables for a monofluid model
//////////////////////////////////////////////////////////////////////////////
struct monofluid_prim_t {
  static constexpr unsigned nvar = 3 + P4EST_DIM;

  double m;        //!< mass per unit volume
  double P;        //!< pressure
  double T;        //!< temperature
  array<double> V; //!< 2D/3D velocity

  inline monofluid_prim_t() : m(0.0), P(0.0), T(0.0) { V.fill(0.0); }
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Monofluid primitive type deduction from conservative type
//////////////////////////////////////////////////////////////////////////////
template <> struct to_prim<monofluid_cons_t> {
  using type = monofluid_prim_t;
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Type containing parameters for a monofluid model
/// (read from the param group in the setting file)
//////////////////////////////////////////////////////////////////////////////
struct monofluid_param_t {

  eos_t eosFluid; //!< parameters of the stiffened-gas eos (default gamma=1.666,
                  //!< Cv=1)

  double gravity_x; //!< gravitational force in the x direction (default 0.0)
  double gravity_y; //!< gravitational force in the y direction (default 0.0)
  double gravity_z; //!< gravitational force in the z direction (default 0.0)

  int lowmach_correction_enabled; //!< low-mach correction for the all-regime
                                  //!< solver (default 0, options 0 or 1)
  double K; //!< diffusion coefficient for the all-regime solver (default 1.1)
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Monofluid parameter type deduction from conservative type
//////////////////////////////////////////////////////////////////////////////
template <> struct to_param<monofluid_cons_t> {
  using type = monofluid_param_t;
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Namespace containing types for a monofluid simulation
/// with additional traits
//////////////////////////////////////////////////////////////////////////////
namespace monofluid {

using cons_t = generic_data_t<monofluid_cons_t>;
using prim_t = to_prim_t<cons_t>;
using param_t = to_param_t<cons_t>;
using qdata_t = to_qdata_t<cons_t>;

} // namespace monofluid

#ifndef SKIPPED_BY_DOXYGEN

/// Factory storing initiation conditions for monofluid
template <>
Factory<monofluid::qdata_t, to_ic_callback_t<monofluid::qdata_t>>::Factory();

/// Factory storing boundary conditions for monofluid
template <>
Factory<monofluid::qdata_t, to_bc_callback_t<monofluid::qdata_t>>::Factory();

/// Factory storing refine conditions for monofluid
template <>
Factory<monofluid::qdata_t, to_rc_callback_t<monofluid::qdata_t>>::Factory();

/// Factory storing hyberbolic fluxes for monofluid
template <>
Factory<monofluid::qdata_t, to_hf_callback_t<monofluid::cons_t>>::Factory();

/// Factory storing source terms for monofluid
template <>
Factory<monofluid::qdata_t, to_st_callback_t<monofluid::cons_t>>::Factory();

/// Factory storing diffusion fluxes for monofluid
template <>
Factory<monofluid::qdata_t, to_df_callback_t<monofluid::cons_t>>::Factory();

//////////////////////////////////////////////////////////////////////////////
/// @name specialization for monofluid
/// @{
//////////////////////////////////////////////////////////////////////////////
template <>
QdataManager<monofluid::cons_t>::QdataManager(SettingManager *stg_mgr);
template <>
void QdataManager<monofluid::cons_t>::cons_to_prim(const monofluid::cons_t &w,
                                                   monofluid::prim_t &r);
template <>
void QdataManager<monofluid::cons_t>::prim_to_cons(
    const monofluid::prim_t &pdata, monofluid::cons_t &cdata);
template <>
void QdataManager<monofluid::cons_t>::get_hydro_flux_predictor(
    int idir, const monofluid::cons_t &cc, const monofluid::prim_t &pf,
    monofluid::cons_t &flux);
template <>
void QdataManager<monofluid::cons_t>::get_grav_source_predictor(
    const monofluid::prim_t &pdata, monofluid::cons_t &source);
template <>
std::vector<double>
QdataManager<monofluid::cons_t>::quad_get_field(const std::string &field_name,
                                                p4est_quadrant_t *quad);
template <>
void QdataManager<monofluid::cons_t>::swap_dir(int idir,
                                               monofluid::prim_t &pdata,
                                               monofluid::cons_t &cdata,
                                               array<monofluid::prim_t> &delta);
template <>
double
QdataManager<monofluid::cons_t>::get_hydro_invdt(const monofluid::prim_t &pdata,
                                                 const double dx);
template <>
double
QdataManager<monofluid::cons_t>::get_cond_invdt(const monofluid::prim_t &pdata,
                                                const double dx);
template <>
double
QdataManager<monofluid::cons_t>::get_visc_invdt(const monofluid::prim_t &pdata,
                                                const double dx);
/// @}

#endif

#endif
