//////////////////////////////////////////////////////////////////////////////
// This file is part of canoP.
// CanoP is a library designed for solving computational fluid dynamics
// problems using a cell-based adaptive mesh refinement approach.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL:
// "http://www.cecill.info".
//////////////////////////////////////////////////////////////////////////////

#include "Factory.h"
#include "QdataMonofluid.h"
#include "Simulation.h"

using cons_t = monofluid::cons_t;
using prim_t = monofluid::prim_t;
using param_t = monofluid::param_t;
using qdata_t = monofluid::qdata_t;

//////////////////////////////////////////////////////////////////////////////
/// \brief dirichlet boundary condition
///
/// Imposed values
//////////////////////////////////////////////////////////////////////////////
void bc_monofluid_dirichlet(p4est_t *p4est, p4est_topidx_t which_tree,
                            p4est_quadrant_t *q, int face,
                            qdata_t *ghost_qdata) {
  UNUSED(q);
  UNUSED(which_tree);

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  SettingManager *stg_mgr = simu->m_stg_mgr;

  double m = 0., P = 0., Vx = 0., Vy = 0., Vz = 0.;

  if (face == 0) {
    m = stg_mgr->read_double("dirichlet.face0.m", 1.0);
    P = stg_mgr->read_double("dirichlet.face0.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face0.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face0.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face0.Vz", 0.0);
  } else if (face == 1) {
    m = stg_mgr->read_double("dirichlet.face1.m", 1.0);
    P = stg_mgr->read_double("dirichlet.face1.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face1.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face1.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face1.Vz", 0.0);
  } else if (face == 2) {
    m = stg_mgr->read_double("dirichlet.face2.m", 1.0);
    P = stg_mgr->read_double("dirichlet.face2.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face2.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face2.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face2.Vz", 0.0);
  } else if (face == 3) {
    m = stg_mgr->read_double("dirichlet.face3.m", 1.0);
    P = stg_mgr->read_double("dirichlet.face3.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face3.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face3.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face3.Vz", 0.0);
  } else if (face == 4) {
    m = stg_mgr->read_double("dirichlet.face4.m", 1.0);
    P = stg_mgr->read_double("dirichlet.face4.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face4.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face4.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face4.Vz", 0.0);
  } else if (face == 5) {
    m = stg_mgr->read_double("dirichlet.face5.m", 1.0);
    P = stg_mgr->read_double("dirichlet.face5.P", 1.0);
    Vx = stg_mgr->read_double("dirichlet.face5.Vx", 0.0);
    Vy = stg_mgr->read_double("dirichlet.face5.Vy", 0.0);
    Vz = stg_mgr->read_double("dirichlet.face5.Vz", 0.0);
  }
  UNUSED(Vz);

  prim_t w_prim;

  w_prim.m = m;
  w_prim.P = P;
  w_prim.V[IX] = Vx;
  w_prim.V[IY] = Vy;
#ifdef USE_3D
  w_prim.V[IX] = Vz;
#endif

  ghost_qdata->wm[IX] = w_prim;
  ghost_qdata->wp[IX] = w_prim;
  ghost_qdata->wm[IY] = w_prim;
  ghost_qdata->wp[IY] = w_prim;
#ifdef USE_3D
  ghost_qdata->wm[IZ] = w_prim;
  ghost_qdata->wp[IZ] = w_prim;
#endif

  /* set slopes to zero */
  qdata_mgr->prim_zero(ghost_qdata->delta[IX]);
  qdata_mgr->prim_zero(ghost_qdata->delta[IY]);
#ifdef USE_3D
  qdata_mgr->prim_zero(ghost_qdata->delta[IZ]);
#endif // USE_3D
}

//////////////////////////////////////////////////////////////////////////////
/// \brief neuman boundary condition
///
/// Imposed null gradient
//////////////////////////////////////////////////////////////////////////////
void bc_monofluid_neuman(p4est_t *p4est, p4est_topidx_t which_tree,
                         p4est_quadrant_t *q, int face, qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);
  UNUSED(face);

  // retrieve our Simu, UserDataManager and finaly our quadrant data
  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /*
   * copy the entire qdata is necessary for monofluid scheme
   * so that we have the slope and MUSCL-Hancock extrapoled states
   * and can perform the trace operation
   */
  qdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* wp / wm should also be initialized */
  {
    prim_t w_prim;

    /* get the primitive variables in the current quadrant */
    qdata_mgr->cons_to_prim(ghost_qdata->w, w_prim);

    ghost_qdata->wm[IX] = w_prim;
    ghost_qdata->wp[IX] = w_prim;
    ghost_qdata->wm[IY] = w_prim;
    ghost_qdata->wp[IY] = w_prim;
#ifdef USE_3D
    ghost_qdata->wm[IZ] = w_prim;
    ghost_qdata->wp[IZ] = w_prim;
#endif
  }

  /* set slopes to zero */
  qdata_mgr->prim_zero(ghost_qdata->delta[IX]);
  qdata_mgr->prim_zero(ghost_qdata->delta[IY]);
#ifdef USE_3D
  qdata_mgr->prim_zero(ghost_qdata->delta[IZ]);
#endif // USE_3D
}

//////////////////////////////////////////////////////////////////////////////
/// \brief reflective boundary condition
///
/// Copy values and switch the sign of normal velocity
//////////////////////////////////////////////////////////////////////////////
void bc_monofluid_reflective(p4est_t *p4est, p4est_topidx_t which_tree,
                             p4est_quadrant_t *q, int face,
                             qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);

  /* get face normal direction */
  int direction = face / 2;

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /* copy current into the ghost outside-boundary cell */
  qdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* reverse the normal V */
  ghost_qdata->w.mV[direction] = -ghost_qdata->w.mV[direction];
  ghost_qdata->wnext.mV[direction] = -ghost_qdata->wnext.mV[direction];

  // slopes in normal direction change sign
  qdata_mgr->prim_change_sign(ghost_qdata->delta[direction]);

  // swap wm and wp in normal direction:
  // so that qm on the right side is the same as qp on the left side
  qdata_mgr->prim_swap(ghost_qdata->wp[direction], ghost_qdata->wm[direction]);
  ghost_qdata->wp[direction].V[direction] =
      -ghost_qdata->wp[direction].V[direction];
  ghost_qdata->wm[direction].V[direction] =
      -ghost_qdata->wm[direction].V[direction];
}

//////////////////////////////////////////////////////////////////////////////
/// \brief no-slip boundary condition
///
/// change sign of velocity in transverse direction
//////////////////////////////////////////////////////////////////////////////
void bc_monofluid_noslip(p4est_t *p4est, p4est_topidx_t which_tree,
                         p4est_quadrant_t *q, int face, qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);

  /* get face normal direction */
  int direction = face / 2;

  /* get current cell data */
  // retrieve our Simu, UserDataManager and finaly our quadrant data
  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /* copy current into the ghost outside-boundary cell */
  qdata_mgr->qdata_copy(ghost_qdata, qdata);

  double dx = amr_mgr->quad_dx(q);

  /* reverse V */
  for (int i = 0; i < P4EST_DIM; i++) {
    ghost_qdata->w.mV[i] = -ghost_qdata->w.mV[i];
    ghost_qdata->wnext.mV[i] = -ghost_qdata->wnext.mV[i];

    for (int j = 0; j < P4EST_DIM; j++) {
      ghost_qdata->wp[j].V[i] = -ghost_qdata->wp[j].V[i];
      ghost_qdata->wm[j].V[i] = -ghost_qdata->wm[j].V[i];
    }
  }

  // slope change sign
  qdata_mgr->prim_change_sign(ghost_qdata->delta[direction]);

  // swap wm and wp:
  // so that qm on the right side is the same as qp on the left side
  qdata_mgr->prim_swap(ghost_qdata->wp[direction], ghost_qdata->wm[direction]);

  // change gradients in transverse direction
  for (int i = 0; i < P4EST_DIM; i++) {
    for (int j = 0; j < P4EST_DIM; j++) {

      if (!(i == direction) & !(j == direction)) {
        ghost_qdata->delta[j].V[i] = -ghost_qdata->delta[j].V[i];
      }
    }

    if (!(i == direction)) {

      if (face % 2 == 1) {
        ghost_qdata->delta[direction].V[i] =
            2 * ghost_qdata->w.mV[i] / (ghost_qdata->w.m * dx);
      } else {
        ghost_qdata->delta[direction].V[i] =
            -2 * ghost_qdata->w.mV[i] / (ghost_qdata->w.m * dx);
      }
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
/// \brief poiseuille boundary condition
///
/// Imposed pressure gradient in x direction, no-slip otherwise
//////////////////////////////////////////////////////////////////////////////
void bc_monofluid_poiseuille(p4est_t *p4est, p4est_topidx_t which_tree,
                             p4est_quadrant_t *q, int face,
                             qdata_t *ghost_qdata) {
  UNUSED(p4est);
  UNUSED(which_tree);
  UNUSED(face);

  // retrieve our Simu, UserDataManager and finaly our quadrant data
  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  /* setting parameter handle */
  param_t *param = qdata_mgr->get_param();

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  double dx = amr_mgr->quad_dx(q);

  // get the coordinates of the center of the quad
  double XYZ[3] = {0, 0, 0};
  double x, y, z;
  UNUSED(y);
  UNUSED(z);

  // get the physical coordinates of the center of the quad
  amr_mgr->quad_centre(which_tree, q, XYZ);

  x = XYZ[IX];
  y = XYZ[IY];
  z = XYZ[IZ];

  double gradP = simu->m_stg_mgr->read_double("poiseuille.gradP", 0.0);
  double P0 = simu->m_stg_mgr->read_double("poiseuille.P0", 1.0);

  if (face == 2 || face == 3 || face == 4 || face == 5) {

    bc_monofluid_noslip(p4est, which_tree, q, face, ghost_qdata);

  } else if (face == 0 || face == 1) {

    if (face == 0) {
      x -= dx;
    } else {
      x += dx;
    }

    /*
     * copy the entire qdata is necessary for monofluid scheme
     * so that we have the slope and MUSCL-Hancock extrapoled states
     * and can perform the trace operation
     */
    qdata_mgr->qdata_copy(ghost_qdata, qdata);

    prim_t w_prim;

    /* get the primitive variables in the current quadrant */
    qdata_mgr->cons_to_prim(ghost_qdata->w, w_prim);

    w_prim.P = P0 + gradP * x;

    double ekin = 0.5 *
                  (w_prim.V[IX] * w_prim.V[IX] + w_prim.V[IY] * w_prim.V[IY]) *
                  w_prim.m;
#ifdef USE_3D
    ekin += w_prim.V[IZ] * w_prim.V[IZ] * w_prim.m;
#endif // USE_3D

    ghost_qdata->w.mE =
        w_prim.m * get_eint(w_prim.m, w_prim.P, param->eosFluid) + ekin;
    ghost_qdata->wp[IX] = w_prim;
    ghost_qdata->wm[IX] = w_prim;
    ghost_qdata->delta[IX].P = gradP;
  }
}

//////////////////////////////////////////////////////////////////////////////
/// \brief Factory storing boundary conditions for monofluid
//////////////////////////////////////////////////////////////////////////////
template <> Factory<qdata_t, to_bc_callback_t<qdata_t>>::Factory() {

  // register the above callback's
  register_factory("dirichlet", bc_monofluid_dirichlet);
  register_factory("neuman", bc_monofluid_neuman);
  register_factory("reflective", bc_monofluid_reflective);
  register_factory("noslip", bc_monofluid_noslip);
  register_factory("poiseuille", bc_monofluid_poiseuille);
}
