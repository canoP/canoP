============
Dependencies
============

The code relies on the `p4est <https://p4est.github.io>`_ and `lua
<https://www.lua.org>`_ libraries that are integrated as git
submodules. The only other dependencies are `MPI <https://www.mpi-forum.org>`_,
`HDF5 <https://www.hdfgroup.org/solutions/hdf5/>`_, and `CMake <https://cmake.org>`_.

On MacOS  with `HomeBrew <https://brew.sh>`_:

.. code-block::

   brew install cmake
   brew install open-mpi
   brew install hdf5-mpi

With `Spack <https://spack.readthedocs.io/en/latest/index.html>`_:

.. code-block::

    git clone https://github.com/spack/spack.git
    cd spack; source share/spack/setup-env.sh
    spack install cmake
    spack install openmpi
    spack install hdf5+hl
    #load the corresponding modules

On Ruche mesocentre (Paris-Saclay):

.. code-block::

    module load gcc/9.2.0/gcc-4.8.5
    module load cmake/3.16.2/gcc-9.2.0
    module load openmpi/4.0.2/gcc-9.2.0
    module load hdf5/1.10.7/gcc-9.2.0-openmpi

The code is version controled with git on the
`Gitlab <https://gitlab.maisondelasimulation.fr>`_ of Maison de la Simulation.
To clone the project and its submodules:

.. code-block::

    git clone --recurse-submodules https://gitlab.maisondelasimulation.fr/tecna/canoP_Na.git



.. toctree::
   :maxdepth: 2

