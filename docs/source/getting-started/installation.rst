===============
Compile and run
===============

.. mdinclude:: ../../../README.md


Visualization
=============

Outputs are written in hdf5 files with xdmf headers. Open the main file with
`paraview <https://www.paraview.org>`_ and get for example for a blast
simulation:

.. image:: blast.png
  :width: 400


.. toctree::
   :maxdepth: 2

