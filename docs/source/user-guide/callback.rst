.. _callbacks:

===================
Available callbacks
===================
For each model, callbacks are stored in different factories (std::map storing
function pointers) for the initial,
boundary, and refine conditions; and for the hyperbolic, diffusion fluxes, and
source terms. Limiters for 2nd order spatial reconstructions are also stored in
a factory. All the available callbacks are listed below.


.. toctree::
   :maxdepth: 2


Monofluid model
===============

Initial conditions
******************

.. doxygenfile:: InitialConditionMonofluid.cpp
   :sections: func

Boundary conditions
*******************

.. doxygenfile:: BoundaryConditionMonofluid.cpp
   :sections: func

Refine conditions
*****************

.. doxygenfile:: RefineConditionMonofluid.cpp
   :sections: func

Hyperbolic fluxes
*****************

.. doxygenfile:: HyperbolicFluxMonofluid.cpp
   :sections: func

Diffusion fluxes
****************
.. doxygenfile:: DiffusionFluxMonofluid.cpp
   :sections: func

Source Terms
************
.. doxygenfile:: SourceTermMonofluid.cpp
   :sections: func

////////////////////////////////////////////////////

Bifluid5eq model
================

Initial conditions
******************

.. doxygenfile:: InitialConditionBifluid5eq.cpp
   :sections: func

Boundary conditions
*******************

.. doxygenfile:: BoundaryConditionBifluid5eq.cpp
   :sections: func

Refine conditions
*****************

.. doxygenfile:: RefineConditionBifluid5eq.cpp
   :sections: func

Hyperbolic fluxes
*****************

.. doxygenfile:: HyperbolicFluxBifluid5eq.cpp
   :sections: func

Diffusion fluxes
****************
.. doxygenfile:: DiffusionFluxBifluid5eq.cpp
   :sections: func

Source Terms
************
.. doxygenfile:: SourceTermBifluid5eq.cpp
   :sections: func





Bifluid7eq model
================

Initial conditions
******************

.. doxygenfile:: InitialConditionBifluid7eq.cpp
   :sections: func

Boundary conditions
*******************

.. doxygenfile:: BoundaryConditionBifluid7eq.cpp
   :sections: func

Refine conditions
*****************

.. doxygenfile:: RefineConditionBifluid7eq.cpp
   :sections: func

Hyperbolic fluxes
*****************

.. doxygenfile:: HyperbolicFluxBifluid7eq.cpp
   :sections: func

Diffusion fluxes
****************
.. doxygenfile:: DiffusionFluxBifluid7eq.cpp
   :sections: func

Source Terms
************
.. doxygenfile:: SourceTermBifluid7eq.cpp
   :sections: func



Limiters
========
.. doxygenfile:: ScalarLimiter.cpp
   :sections: func
