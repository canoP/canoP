======================
Writing a setting file
======================

A series of setting files are available `here
<https://gitlab.maisondelasimulation.fr/tecna/canoP_Na/-/tree/master/settings?ref_type=heads>`_. We
list below all the parameters that can be defined in the different group of the
setting file with their default values if they are not specified (remove the
prefix m_ in the setting file). The parameters
needed to define initial conditions are detailed in the initial condition
callbacks. See :ref:`callbacks` for a description of the different callbacks
that can be used.

simulation settings
===================
.. doxygengroup:: simulation-settings

amr settings
============
.. doxygengroup:: amr-settings

io settings
===========
.. doxygengroup:: io-settings

callback settings
=================
.. doxygengroup:: callback-settings

param settings (monofluid model)
================================
.. doxygenstruct:: monofluid_param_t
   :members-only:

param settings (bifluid5eq model)
=================================
.. doxygenstruct:: bifluid5eq_param_t
   :members-only:

param settings (bifluid7eq model)
=================================
.. doxygenstruct:: bifluid7eq_param_t
   :members-only:

EOS settings (Noble-Abel EOS)
=============================
.. doxygenstruct:: eos_t
   :members-only:

.. toctree::
   :maxdepth: 2

