============
Type details
============

.. toctree::
   :maxdepth: 2

Monofluid model
===============
.. doxygenstruct:: monofluid_cons_t
   :members:

.. doxygenstruct:: monofluid_prim_t
   :members:

Bifluid5eq model
===============

.. doxygenstruct:: bifluid5eq_cons_t
   :members:

.. doxygenstruct:: bifluid5eq_prim_t
   :members:

Bifluid7eq model
================

.. doxygenstruct:: bifluid7eq_cons_t
   :members:

.. doxygenstruct:: bifluid7eq_prim_t
   :members:


