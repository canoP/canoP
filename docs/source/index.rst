=================================
Welcome to canoP's documentation!
=================================

.. epigraph:: canoP is a library designed for solving computational fluid
	      dynamic problems using a cell-based adaptive mesh refinement
	      approach.

.. image:: dambreak.png
  :width: 400

`Drui (2017) <https://theses.hal.science/tel-01618320>`_


.. toctree::
   :maxdepth: 2
   :caption: Getting started

   getting-started/overview
   getting-started/installation

.. toctree::
   :maxdepth: 2
   :caption: User guide

   user-guide/setting
   user-guide/callback
   user-guide/app
   user-guide/simutype

.. toctree::
   :maxdepth: 2
   :caption: Developer guide

   developer-guide/simulation
   developer-guide/qdata
   developer-guide/iterator
   developer-guide/factory
   developer-guide/setting_manager
   developer-guide/amr_manager
   developer-guide/io_manager
   developer-guide/stat_manager
   developer-guide/qdata_manager



==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
