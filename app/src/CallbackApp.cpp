//////////////////////////////////////////////////////////////////////////////
// This file is part of the dummy application app.
// App is designed to illustrate how to use canoP as a library.
//
// This software is governed by whatever license you want to use.
//////////////////////////////////////////////////////////////////////////////

#include "CallbackApp.h"
#include "QdataBifluid5eq.h"


using cons_t = bifluid5eq::cons_t;
using prim_t = bifluid5eq::prim_t;
using param_t = bifluid5eq::param_t;
using qdata_t = bifluid5eq::qdata_t;

//////////////////////////////////////////////////////////////////////////////
void ic_bifluid5eq_myjet(p4est_t *p4est, p4est_topidx_t which_tree,
                         p4est_quadrant_t *quad) {

   UNUSED(which_tree);

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  SettingManager *stg_mgr = simu->m_stg_mgr;

  param_t *param = qdata_mgr->get_param();

  /* now start genuine initialization */
  cons_t w;
  memset(&w, 0, sizeof(cons_t));

  prim_t q;
  memset(&q, 0, sizeof(prim_t));

  q.mg = stg_mgr->read_double("jet.mg_out", 856.2);
  q.ml = stg_mgr->read_double("jet.ml_out", 0.0);
  q.ag = stg_mgr->read_double("jet.ag_out", 1.0);
  q.P = stg_mgr->read_double("jet.P_out", 1.0E5);

  qdata_mgr->prim_to_cons(q, w);

  // finally set quadrant's qdata
  qdata_mgr->quad_set_cons(quad, &w);
}

//////////////////////////////////////////////////////////////////////////////
void bc_bifluid5eq_myjet(p4est_t *p4est, p4est_topidx_t which_tree,
                         p4est_quadrant_t *q, int face, qdata_t *ghost_qdata) {

  UNUSED(which_tree);
  UNUSED(q);
  UNUSED(face);

  Simulation<cons_t> *simu = get_simu<cons_t>(p4est);

  QdataManager<cons_t> *qdata_mgr = simu->get_qdata_mgr();

  AmrManager *amr_mgr = simu->get_amr_mgr();

  param_t *param = qdata_mgr->get_param();

  SettingManager *stg_mgr = simu->m_stg_mgr;

  qdata_t *qdata = qdata_mgr->quad_get_qdata(q);

  /* one can specify the inflow face in the parameter file */
  int jetface = stg_mgr->read_int("jet.face", 0);
  double x_c = stg_mgr->read_double("jet.x_c", 0.0);
  double y_c = stg_mgr->read_double("jet.y_c", 0.5);
  double z_c = stg_mgr->read_double("jet.z_c", 0.5);
  double radius = stg_mgr->read_double("jet.radius", 0.02);
  double radius2 = radius * radius;

  UNUSED(z_c);

  if (face == jetface) { // inflow

    /* get face normal direction */
    int direction = jetface / 2;

    // reflective
    /* copy current cell data into the ghost outside-boundary cell */
    qdata_mgr->qdata_copy(ghost_qdata, qdata);

    /* reverse the normal V */
    ghost_qdata->w.mV[direction] = -ghost_qdata->w.mV[direction];
    ghost_qdata->wnext.mV[direction] = -ghost_qdata->wnext.mV[direction];

    // slopes in normal direction change sign
    qdata_mgr->prim_change_sign(ghost_qdata->delta[direction]);

    // swap wm and wp in normal direction:
    // so that qm on the right side is the same as qp on the left side
    qdata_mgr->prim_swap(ghost_qdata->wp[direction],
                         ghost_qdata->wm[direction]);
    ghost_qdata->wp[direction].V[direction] =
        -ghost_qdata->wp[direction].V[direction];
    ghost_qdata->wm[direction].V[direction] =
        -ghost_qdata->wm[direction].V[direction];

    // get the physical coordinates of the center of the quad
    double x, y, z;
    double dx;
    double XYZ[3] = {0, 0, 0};

    amr_mgr->quad_centre(which_tree, q, XYZ);
    dx = amr_mgr->quad_dx(q);

    x = XYZ[0];
    y = XYZ[1];
    z = XYZ[2];
    UNUSED(z);

    double d2 = SC_SQR(x - x_c) + SC_SQR(y - y_c);
#ifdef USE_3D
    d2 += SC_SQR(z - z_c);
#endif

    double ml_in = stg_mgr->read_double("jet.ml_in", 170.65);
    double mg_in = stg_mgr->read_double("jet.mg_in", 0.0);
    double ag_in = stg_mgr->read_double("jet.ag_in", 0.0);
    double P_in = stg_mgr->read_double("jet.P_in", 1.8E7);

    int centre_in_cell = 0;

#ifdef USE_3D
    if (((x_c >= x - 0.5 * dx) & (x_c <= x + 0.5 * dx)) &
        ((y_c >= y - 0.5 * dx) & (y_c <= y + 0.5 * dx)) &
        ((z_c >= z - 0.5 * dx) & (z_c <= z + 0.5 * dx))) {
      centre_in_cell = 1;
    }
#else
    if (((x_c >= x - 0.5 * dx) & (x_c <= x + 0.5 * dx)) &
        ((y_c >= y - 0.5 * dx) & (y_c <= y + 0.5 * dx))) {
      centre_in_cell = 1;
    }
#endif

    if (d2 < radius2 || centre_in_cell) {

      // reconstructed state (trace)
      prim_t wr;
      wr.ml = ml_in;
      wr.mg = mg_in;
      wr.ag = ag_in;
      wr.P = P_in;

      qdata_mgr->prim_to_cons(wr, ghost_qdata->w);
      qdata_mgr->prim_to_cons(wr, ghost_qdata->wnext);

      qdata_mgr->prim_copy(ghost_qdata->wm[IX], wr);
      qdata_mgr->prim_copy(ghost_qdata->wm[IY], wr);
      qdata_mgr->prim_copy(ghost_qdata->wp[IX], wr);
      qdata_mgr->prim_copy(ghost_qdata->wp[IY], wr);
#ifdef USE_3D
      qdata_mgr->prim_copy(ghost_qdata->wm[IZ], wr);
      qdata_mgr->prim_copy(ghost_qdata->wp[IZ], wr);
#endif

      // set slopes to zero
      qdata_mgr->prim_zero(ghost_qdata->delta[IX]);
      qdata_mgr->prim_zero(ghost_qdata->delta[IY]);
#ifdef USE_3D
      qdata_mgr->prim_zero(ghost_qdata->delta[IZ]);
#endif // USE_3D
    }

  } else { // reflective

   /* get face normal direction */
   int direction = jetface / 2;

   /* copy current into the ghost outside-boundary cell */
   qdata_mgr->qdata_copy(ghost_qdata, qdata);

   /* reverse the normal V */
   ghost_qdata->w.mV[direction] = -ghost_qdata->w.mV[direction];
   ghost_qdata->wnext.mV[direction] = -ghost_qdata->wnext.mV[direction];

   // slopes in normal direction change sign
   qdata_mgr->prim_change_sign(ghost_qdata->delta[direction]);

   // swap wm and wp in normal direction:
   // so that qm on the right side is the same as qp on the left side
   qdata_mgr->prim_swap(ghost_qdata->wp[direction], ghost_qdata->wm[direction]);
   ghost_qdata->wp[direction].V[direction] =
      -ghost_qdata->wp[direction].V[direction];
   ghost_qdata->wm[direction].V[direction] =
      -ghost_qdata->wm[direction].V[direction];

  }
}

//////////////////////////////////////////////////////////////////////////////
double rc_bifluid5eq_my_m_gradient(qdata_t *cella, qdata_t *cellb) {

  double mg[2] = {cella->w.mg, cellb->w.mg};

  double mg_grad = scalar_gradient(mg[0], mg[1]);

  double ml[2] = {cella->w.ml, cellb->w.ml};

  double ml_grad = scalar_gradient(ml[0], ml[1]);

  return fmax(mg_grad, ml_grad);

}

//////////////////////////////////////////////////////////////////////////////
double limiter_myminmod(double ul, double ur) {

  if ((ul * ur) < 0) {
    return 0;
  } else if (fabs(ul) < fabs(ur)) {
    return ul;
  } else {
    return ur;
  }
}

//////////////////////////////////////////////////////////////////////////////
void hf_bifluid5eq_myflux(prim_t &ql, prim_t &qr, cons_t &ucl, cons_t &ucr,
                          cons_t *fluxl, cons_t *fluxr,
                          QdataManager<cons_t> *qdata_mgr, double *extra) {

  app::param_t *param_app =
      static_cast<app::param_t *>(qdata_mgr->get_user_param());
};

//////////////////////////////////////////////////////////////////////////////
void df_bifluid5eq_myflux(prim_t &ql, prim_t &qr, array<prim_t> &deltal,
                          array<prim_t> &deltar, cons_t *fluxl, cons_t *fluxr,
                          QdataManager<cons_t> *qdata_mgr) {

  app::param_t *param_app =
      static_cast<app::param_t *>(qdata_mgr->get_user_param());
};

//////////////////////////////////////////////////////////////////////////////
void st_bifluid5eq_mysource(prim_t &q, prim_t &qnext, cons_t *wnext, double dt,
                            QdataManager<cons_t> *qdata_mgr) {

  app::param_t *param_app =
      static_cast<app::param_t *>(qdata_mgr->get_user_param());
};
