//////////////////////////////////////////////////////////////////////////////
// This file is part of the dummy application app.
// App is designed to illustrate how to use canoP as a library.
//
// This software is governed by whatever license you want to use.
//////////////////////////////////////////////////////////////////////////////

#include "CallbackApp.h"
#include "QdataBifluid5eq.h"
#include "Simulation.h"

//////////////////////////////////////////////////////////////////////////////
/// \brief Specialization to register the user defined callbacks
//////////////////////////////////////////////////////////////////////////////
template <> void Simulation<bifluid5eq::cons_t>::register_user_callback() {

  // register user callbacks
  m_ic_factory.register_factory("myjet", ic_bifluid5eq_myjet);
  m_bc_factory.register_factory("myjet", bc_bifluid5eq_myjet);
  m_rc_factory.register_factory("my_m_gradient", rc_bifluid5eq_my_m_gradient);
  m_sl_factory.register_factory("myminmod", limiter_myminmod);
  m_hf_factory.register_factory("my_hyperbolic_flux", hf_bifluid5eq_myflux);
  m_df_factory.register_factory("my_diffusion_flux", df_bifluid5eq_myflux);
  m_st_factory.register_factory("my_source_term", st_bifluid5eq_mysource);
};

int main(int argc, char **argv) {
  MPI_Comm mpicomm;
  int mpisize, mpirank, mpiret;
  int log_verbosity;
  SettingManager *stg_mgr;
  Simulation<bifluid5eq::cons_t> *simu;
  QdataManager<bifluid5eq::cons_t> *qdata_mgr;
  clock_t t_start = 0;

  // initialize MPI
  mpiret = MPI_Init(&argc, &argv);
  SC_CHECK_MPI(mpiret);
  mpicomm = MPI_COMM_WORLD;
  mpiret = MPI_Comm_size(mpicomm, &mpisize);
  SC_CHECK_MPI(mpiret);
  mpiret = MPI_Comm_rank(mpicomm, &mpirank);
  SC_CHECK_MPI(mpiret);

  // exit if no settings are provided
  if (argc < 2) {
    if (mpirank == 0) {
      printf("usage: %s SETTING_FILE LOG_VERBOSITY\n", argv[0]);
    }
    MPI_Finalize();
    exit(0);
  }

  // get the verbosity for the logs e.g. argv[2]=DEBUG, check SettingManager.cpp
  log_verbosity = setting_log_verbosity(argv[2]);

  int SC_CATCH_SIGNALS = 1;
  int SC_PRINT_BACKTRACE = 1;

  // init p4est and libsc
  sc_init(mpicomm, SC_CATCH_SIGNALS, SC_PRINT_BACKTRACE, NULL, log_verbosity);
  p4est_init(NULL, log_verbosity);

  // init setting manager
  stg_mgr = new SettingManager(argv[1]);

  // retrieve the simulation type name from settings
  P4EST_GLOBAL_PRODUCTION("\n");
  const std::string simu_name =
      stg_mgr->readv_string("simulation.type", "Unknown");
  P4EST_GLOBAL_PRODUCTION("\n");

  // create simulation from the derived simulation app
  simu = new Simulation<bifluid5eq::cons_t>(stg_mgr, mpicomm);

  // create app param and initialize them from settings
  app::param_t param_app;

  P4EST_GLOBAL_PRODUCTION("-- read app settings -- \n");

  param_app.myparam1 = stg_mgr->readv_double("param_app.myparam1", 1.0);
  param_app.myparam2 = stg_mgr->readv_double("param_app.myparam2", 2.0);

  P4EST_GLOBAL_PRODUCTION("\n");

  // set user_pointer of qdata_mgr to param_app
  qdata_mgr = simu->get_qdata_mgr();
  qdata_mgr->set_user_param(static_cast<void *>(&param_app));

  t_start = (double)clock();

  // initial refinement on the initial condition
  if (!simu->restarted()) {
    simu->initial_refinement();
  }

  // perform the time loop
  while (!simu->finished()) {
    simu->next_iteration();

    if (simu->should_adapt()) {
      simu->adapt(0);
    }

    if (simu->should_write()) {
      simu->write_output_file();
    }
  }

  // Display some minimalist statistics on screen
  double total_time = (double)(clock() - t_start) / CLOCKS_PER_SEC;
  double nb_cell_update_per_second =
      1.0 * simu->m_total_num_cell_update / total_time;
  P4EST_GLOBAL_ESSENTIAL(
      "#######################################################\n");
  P4EST_GLOBAL_ESSENTIALF(
      "Number of cell-updates per seconds: %f Mupdate-cell/s\n",
      nb_cell_update_per_second / 1e6);

  // End of simu run, write a restart file
  simu->write_restart_file();

  // Write detailed statistics to file
  simu->write_statistics();

  // destroy our data structures
  delete simu;
  delete stg_mgr;

  // clean up and exit
  sc_finalize();

  mpiret = MPI_Finalize();
  SC_CHECK_MPI(mpiret);

  return 0;
}
