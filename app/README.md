The [app directory](https://gitlab.maisondelasimulation.fr/canoP/canoP/-/tree/master/app?ref_type=heads)
is a dummy application using canoP as a library. This directory should be copied
outside of canoP in order to build an application on its own loading canoP as a git submodule.

Dependencies
============
Once copied outside canoP, inside the app directory:

    $ git init
    $ git submodule init
    $ git submodule add https://gitlab.maisondelasimulation.fr/canoP/canoP.git

User modifications
==================
Users have access to the default simulations available in canoP (monofluid, bifluid5eq and
bifluid7eq) and can register their own callbacks. A simulation of type Bifluid5eq is used
in this dummy example, and user defined parameters can be added thanks to a void* pointer
available in QdataManager (see ``main.cpp`` to add the pointer and ``CallbackApp.cpp`` to
retrieve the user defined parameters in the callbacks).

Users have access to all the default initial, boundary, and refine conditions, limiters,
fluxes and source terms available in canoP for the different simulations.

They can also add their custom initial conditions, boundary conditions, indicators,
limiters, fluxes and source terms by adding them to the file ``CallbackApp.cpp``
and register them in the different factories by specializing the function
``register_user_callback()`` in ``main.cpp``.

They can then access them in the settings, the dummy example in app defines:
* a ``myjet`` initial condition
* a ``myjet`` boundary condition
* a ``myrho_gradient`` indicator
* a ``myminmod`` limiter
* a ``my_hyperbolic_flux`` hyperbolic flux
* a ``my_diffusion_flux`` diffusion flux
* a ``my_source_term`` source term

Compilation
===========
The app uses CMake for the compilation. So these simple commands will do:

    $ mkdir build
    $ cd build
    $ cmake -DUSE_BIFLUID5EQ=ON ..
    $ make -j 4

This will produce an executable: ``app_2d``. Other simulation types (monofluid, and/or bifluid7eq)
can be activated by using -DUSE_xxx=ON. The 3d app can be produce by adding -DUSE_3D=ON and the
executable that is produced is then ``app_3d``.

Running
=======

After compilation the executable can be found in ``app/build/src/``. To
run the program, it is recommended to write a small configuration file with
the desired options. A full commented example is available in ``app/settings/``.

To run it, go:

    $ mpirun -np 4 ./src/app_2d settings_bifluid5eq_myjet.lua
